function out_str = print_hor_line(D, print_bool, sort_name)
    if ~strcmpi(sort_name,'bubsort')
        out_str = '_____';
    else
        out_str = '';
    end
    for i=1:length(D)
        out_str = [out_str '-----']; %#ok<AGROW>
    end
    
    if sort_name=='inssort'
        for i=1:14
            out_str = [out_str '_'];
        end
    elseif sort_name=='selsort'
        %do nothing
    elseif sort_name=='bubsort'
        for i=1:9
            out_str = [out_str '_'];
        end
    else
        error(['unknown sort_name(' sort_name ')']);
    end
    
    out_str = [out_str newline];
    if print_bool
        fprintf('%s',out_str)
    end
end