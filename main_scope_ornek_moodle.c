#include <stdio.h>
#include <stdlib.h>

int x = 8;//global x

void f01(int x, int y)
{
	printf("f01(%d)\n",x);//global x yazd�r�l�yor.
	x=y;//fonksiyon �a�r�l�rken set edilen y de�eri, lokal olan x'in i�eri�ine kopyalan�yor.
}

void f02(int y)
{
	//lokal ve ayn� zamanda static bir x tan�ml�yoruz
	static int x=5;//bu sadece main fonksiyonu f02 fonksiyonunu ILK defa �a��rd���nda �al��an bir sat�rd�r.
	//printf - static olan x'i yazd�r�r
	printf("f02_s(%d)\n",x);
	x=y;//burada 2 sat�r yukardaki static olarak tan�mlanm�� x'e f02 �a�r�l�rken g�nderilen y e�itleniyor.
}

void f03(int y)
{
	int x=y;//lokal olarak tan�mlanm�� bir x'e y de�eri atan�yor.
	printf("f03(%d)\n",x);//lokal x yazd�r�l�yor.
}

void f04(int y)
{
	printf("f04(%d)\n",x);//global x yazd�r�l�yor.
	x=y;//fonksiyon �a�r�l�rken set edilen y de�eri, global olan x'in i�eri�ine kopyalan�yor.
}

void f05(int y)
{
	printf("f05_g(%d)\n",x);//global x yazd�r�l�yor.
	x=y;//fonksiyon �a�r�l�rken set edilen y de�eri, global olan x'in i�eri�ine kopyalan�yor.
	int x = 2*y;//lokal bir x tan�mlan�yor ve y'nin 2 kat� i�ine set ediliyor.
	printf("f05_l(%d)\n",x);//lokal olan x yazd�r�l�yor
}


int main(int argc, char *argv[]) 
{
	f01(x,9);
	f03(7);
	f02(17);
	f04(21);
	printf("main_g(%d)\n",x);//global x
	int x = 37;
	f02(20);
	f01(x,32);
	printf("main_l(%d)\n",x);//lokal x
	f02(x-3);
	//scanf("%d",&x);
	return 0;
}

