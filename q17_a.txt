
Integer tipinde, adı Xarr olan 4 elemanlı bir dizi tanımlayın. 
	a. Dizinin birinci elemanını 334 olarak doldurun. Diziyi kutucuklarla gösterin.
	b. Bu diziye sırayla aşağıdaki işlemleri uygulayın ve her işlemden sonra dizinin kutularının şeklini çizin.
	1	 (1). push(351) --> [334,351,__,__]
	1	 (2). pop() <hangi sayı çıkar = 351> --> [334,__,__,__]
	1	 (3). push(391) --> [334,391,__,__]
	2	 (4). dequeue() <hangi sayı çıkar = 334> --> [__,391,__,__]
	2	 (5). enqueue(312) --> [__,391,312,__]
	1	 (6). pop() <hangi sayı çıkar = 312> --> [__,391,__,__]
	1	 (7). enqueue(386) --> [__,391,386,__]
	1	 (8). push(305) --> [__,391,386,305]
	1	 (9). push(329) --> [329,391,386,305]
	1	(10). enqueue(382) --> [329,391,386,305]
	1	(11). dequeue() <hangi sayı çıkar = 391> --> [329,__,386,305]
	1	(12). pop() <hangi sayı çıkar = 329> --> [__,__,386,305]
	1	(13). dequeue() <hangi sayı çıkar = 386> --> [__,__,__,305]
	1	(14). pop() <hangi sayı çıkar = 305> --> [__,__,__,__]
	2	(15). dequeue() <hangi sayı çıkar = --> --> [__,__,__,__]
	2	(16). pop() <hangi sayı çıkar = --> --> [__,__,__,__]
