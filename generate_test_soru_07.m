function [soru_07, correct_selection_id] = generate_test_soru_07(base_soru, ogrenci_str, selection_idx, verbose)
% % SoruTip007Begin
% \section{Soru-X:}
% Konu: Renk Uzayları \\
% Puan: SoruTip007Puan \\
% Soru:$[R,G,B]$ rengi için hangi kartuşlar kullanıl(ır-maz)?
% \begin{enumerate}[label=\alph*)]
%     \item Select-A
%     \item Select-B
%     \item Select-C
%     \item Select-D
%     \item Select-E
% \end{enumerate}
% % SoruTip007End
%   base_soru = soru_base{7}
    soru_id = 7;
    rng(selection_idx)
    [rgb_vec, cmyk_vec, color_name] = get_rgb_cmyk(selection_idx);
    soru_07 = strrep(base_soru, '{Soru-X:}',['{' ogrenci_str '-Soru-' num2str(soru_id, '%d')  '-Seed(' num2str(selection_idx)  '):}']);
    soru_07 = strrep(soru_07, '$[R,G,B]$', ['$[R=' num2str(rgb_vec(1),'%d') ',G=' num2str(rgb_vec(2),'%d') ',B=' num2str(rgb_vec(3),'%d') ']$ (' color_name ')']); 
    
    [answers_cell, correct_selection_id, str01] = latex_test_generate_answer_07(cmyk_vec, randi(2), randi(50));
    soru_07 = strrep(soru_07, 'kullanıl(ır-maz)?', str01); 
    
    
    for i=1:5
        old_str = ['Select-' char(('A')+i-1)];
        new_str = answers_cell{i};
        soru_07 = strrep(soru_07, old_str, new_str);
    end 
    if verbose>0
        disp(['soru_07 : \n' soru_07])
        disp(['cevap : ' char(('A')+correct_selection_id-1)])
        disp(['cevap : ' color_name ', rgb_vec(' mat2str(rgb_vec) '), cmyk_vec(' mat2str(cmyk_vec) ')' ])
    end
end

function [answers_cell, correct_selection_id, str01] = latex_test_generate_answer_07(cmyk_vec, use1_notuse2, seed_val)
    rng(seed_val)
    answers_cell = cell(1,5);
 

    correct_answer = 1+bin2dec(strrep(int2str(cmyk_vec), ' ', ''));
    possible_vec = 1:16;
    possible_vec(correct_answer) = [];
    possible_vec = possible_vec(randperm(15));
    possible_vec = [correct_answer possible_vec(1:4)];
    possible_vec = possible_vec(randperm(5));  
    correct_selection_id = find(possible_vec==correct_answer,1);

    cmyk_chars = 'CMYK';
    if use1_notuse2==1
        str01 = 'kullanılır';
        correct_answer = cmyk_chars(cmyk_vec==1);
    else
        str01 = 'kullanılmaz';
        correct_answer = cmyk_chars(cmyk_vec==0);
    end
    
    for i=1:5
        ans_ca = dec2bin(possible_vec(i)-1);
        while length(ans_ca)<4
            ans_ca = ['0' ans_ca];
        end
        ans_let = '';
        for j=1:4
            if ans_ca(j)=='1' && use1_notuse2==1
                ans_let = [ans_let cmyk_chars(j)];
            elseif ans_ca(j)=='0' && use1_notuse2==2
                ans_let = [ans_let cmyk_chars(j)];
            end
        end
        if strcmpi(ans_let, '')
            ans_let = 'Hiçbiri';
        end
        answers_cell{i} = ans_let;
    end 
    if isempty(correct_answer)
        correct_answer = 'Hiçbiri';
    end
    assert(strcmpi(answers_cell{correct_selection_id},correct_answer), 'problem here')
end

