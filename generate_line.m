function ret_struct = generate_line(minDif, maxDif, minPoint, seedval, verbose)
%generate_line Will generate a random line that has
%   start and end point
%   its incs for X and Y
%   find all points along the line
%   seedval 0(Y longer) and 2(X longer) generates 2 different lines
    if ~exist('seedval', 'var')
        seedval = 0;
    end
    if ~exist('verbose', 'var')
        verbose = 1;
    end
    rng(seedval)
    xBeg = minPoint + randi(3);
    yBeg = minPoint + randi(3);
    
    xDif = 0; yDif = 0;
    while xDif==yDif
        xDif = minDif + randi(maxDif);
        yDif = minDif + randi(maxDif);
    end
    
    xEnd = xBeg + xDif;
    yEnd = yBeg + yDif;
      
    longer_edge='X';
    if xDif<yDif,  longer_edge='Y'; end
    longer_dif = max(xDif, yDif);

    xInc = (xDif)/(longer_dif);
    yInc = (yDif)/(longer_dif);    
    
    xVec = round(xBeg + (0:longer_dif)*xInc);
    yVec = round(yBeg + (0:longer_dif)*yInc);
    
    if verbose>0
        disp(['x(', num2str(xBeg, '%d') ,', ', num2str(xDif, '%d') ,', ', num2str(xEnd, '%d'),', ', num2str(xInc, '%4.2f') ,')'])
        disp(['xVec: ' mat2str(xVec)])
        disp(['y(', num2str(yBeg, '%d') ,', ', num2str(yDif, '%d') ,', ', num2str(yEnd, '%d'),', ', num2str(yInc, '%4.2f') ,')'])
        disp(['yVec: ' mat2str(yVec)])
        disp(['longer - edge(' longer_edge  '), dif(' num2str(longer_dif,'%d') ')'])
    end    
    
    ret_struct.beg = [xBeg yBeg];
    ret_struct.end = [xEnd yEnd];
    ret_struct.dif = [xDif yDif];
    ret_struct.inc = [xInc yInc];
    ret_struct.longer_edge = longer_edge;
    ret_struct.longer_dif = longer_dif;
    ret_struct.xVec = xVec;
    ret_struct.yVec = yVec;    
    ret_struct.point_count = longer_dif+1;    
end

