
\begin{center}
     \Large{\textbf{Grafik Ara Sınavı 2021 - Ogr-Nu(8134)-AdSoyad(Abdullah FİDAN)}} \\
\end{center}
\begin{multicols}{3}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

%
% SoruTip001-cevap(E)
\section{OgrNu(8134)-Soru-1-Seed(45):}
Konu: Çizgi Çizmek \\
Puan: 10 \\
Soru:$[X=7,Y=6]$ noktasından $[X=14,Y=14]$ noktasına çizgi çizildiğinde
kaç adet piksel dolu olur?
\begin{enumerate}[label=\alph*)]
    \item 6
    \item 7
    \item 13
    \item 5
    \item 9
\end{enumerate}
% SoruTip001End
% SoruTip002-cevap(B)
\section{OgrNu(8134)-Soru-2-Seed(50):}
Konu: Çizgi Çizmek \\
Puan: 20 \\
Soru:$[X=8,Y=7]$ noktasından $[X=15,Y=13]$ noktasına çizgi çizildiğinde
aşağıdaki noktalardan hangisinden geçmez?
\begin{enumerate}[label=\alph*)]
    \item X=12,Y=10
    \item X=8,Y=9
    \item X=14,Y=12
    \item X=13,Y=11
    \item X=10,Y=9
\end{enumerate}
% SoruTip002End
% SoruTip003-cevap(B)
\section{OgrNu(8134)-Soru-3-Seed(34):}
Konu: Çizgi Çizmek \\
Puan: 10 \\
Soru:$[X=7,Y=6]$ noktasından $[X=16,Y=14]$ noktasına çizgi çizildiğinde
$X ve Y$ düzlemlerinde kullanılacak artış miktarları hangi seçenekle sırayla doğru olarak verilmiştir?
\begin{enumerate}[label=\alph*)]
    \item xInc=9.00,yInc=8.00
    \item xInc=1.00,yInc=0.89
    \item xInc=1.00,yInc=0.88
    \item xInc=0.89,yInc=1.00
    \item xInc=0.88,yInc=1.00
\end{enumerate}
\vfill\null\columnbreak
% SoruTip003End
% SoruTip004-cevap(B)
\section{OgrNu(8134)-Soru-4-Seed(17):}
Konu: Çizgi Çizmek \\
Puan: 10 \\
Soru:$[X=8,Y=6]$ noktasından $[X=13,Y=13]$ noktasına çizgi çizildiğinde
%X=5 nostası Y düzleminde veya Y=3 noktası X düzleminde
$Y=9$ noktası $X$ düzleminde 
hangi noktadan geçer?
\begin{enumerate}[label=\alph*)]
    \item 9
    \item 10
    \item 12
    \item 8
    \item 11
\end{enumerate}
% SoruTip004End
% SoruTip005-cevap(C)
\section{OgrNu(8134)-Soru-5-Seed(5):}
Konu: Renk Uzayları \\
Puan: 10 \\
Soru:$[R=0,G=0,B=255]$ (Mavi) rengi CMYK uzayında hangi renge eş gelir?
\begin{enumerate}[label=\alph*)]
    \item C(1),M(0),Y(0),K(1)
    \item C(0),M(1),Y(0),K(0)
    \item C(1),M(1),Y(0),K(0)
    \item C(1),M(1),Y(1),K(1)
    \item C(0),M(1),Y(1),K(1)
\end{enumerate}
% SoruTip005End
% SoruTip006-cevap(A)
\section{OgrNu(8134)-Soru-6-Seed(8):}
Konu: Renk Uzayları \\
Puan: 10 \\
Soru:$[C=0,M=1,Y=0,K=0]$ (Macenta) rengi RGB uzayında hangi renge eş gelir?
\begin{enumerate}[label=\alph*)]
    \item R(1),G(0),B(1)
    \item R(1),G(1),B(1)
    \item R(0),G(0),B(1)
    \item R(1),G(0),B(0)
    \item R(0),G(1),B(1)
\end{enumerate}
\vfill\null\columnbreak
% SoruTip006End
% SoruTip007-cevap(E)
\section{OgrNu(8134)-Soru-7-Seed(7):}
Konu: Renk Uzayları \\
Puan: 10 \\
Soru:$[R=0,G=255,B=255]$ (Camgöbeği) rengi için hangi kartuşlar kullanılır
\begin{enumerate}[label=\alph*)]
    \item CMY
    \item MY
    \item YK
    \item MYK
    \item C
\end{enumerate}
% SoruTip007End
% SoruTip008-cevap(E)
\section{OgrNu(8134)-Soru-8-Seed(3):}
Konu: Renk Uzayları \\
Puan: 10 \\
Soru:Mavi renginin 24 bitlik düzendeki yazılışı hangisidir?
\begin{enumerate}[label=\alph*)]
    \item 111111111111111111111111
    \item 000000000000000000000000
    \item 000000001111111100000000
    \item 111111110000000000000000
    \item 000000000000000011111111
\end{enumerate}
% SoruTip008End
% SoruTip009-cevap(D)
\section{OgrNu(8134)-Soru-9-Seed(2):}
Konu: Renk Uzayları \\
Puan: 10 \\
Soru:$[R,G,B]$ renginde her renk 5 bit ile ifade edilseydi kaç farklı tonda renk üretilebilirdi?
\begin{enumerate}[label=\alph*)]
    \item 486
    \item 64
    \item 10
    \item 96
    \item 30
\end{enumerate}
% SoruTip009End



\end{multicols}
\newpage

