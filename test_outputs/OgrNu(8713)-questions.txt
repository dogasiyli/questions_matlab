
\begin{center}
     \Large{\textbf{Grafik Ara Sınavı 2021 - Ogr-Nu(8713)-AdSoyad(Ahmet Furkan DUMLU)}} \\
\end{center}
\begin{multicols}{3}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

%
% SoruTip001-cevap(B)
\section{OgrNu(8713)-Soru-1-Seed(12):}
Konu: Çizgi Çizmek \\
Puan: 10 \\
Soru:$[X=8,Y=8]$ noktasından $[X=19,Y=17]$ noktasına çizgi çizildiğinde
kaç adet piksel dolu olur?
\begin{enumerate}[label=\alph*)]
    \item 8
    \item 12
    \item 14
    \item 17
    \item 9
\end{enumerate}
% SoruTip001End
% SoruTip002-cevap(C)
\section{OgrNu(8713)-Soru-2-Seed(31):}
Konu: Çizgi Çizmek \\
Puan: 20 \\
Soru:$[X=8,Y=6]$ noktasından $[X=13,Y=13]$ noktasına çizgi çizildiğinde
aşağıdaki noktalardan hangisinden geçmez?
\begin{enumerate}[label=\alph*)]
    \item X=11,Y=10
    \item X=12,Y=11
    \item X=8,Y=8
    \item X=9,Y=8
    \item X=10,Y=9
\end{enumerate}
% SoruTip002End
% SoruTip003-cevap(D)
\section{OgrNu(8713)-Soru-3-Seed(16):}
Konu: Çizgi Çizmek \\
Puan: 10 \\
Soru:$[X=6,Y=8]$ noktasından $[X=13,Y=17]$ noktasına çizgi çizildiğinde
$X ve Y$ düzlemlerinde kullanılacak artış miktarları hangi seçenekle sırayla doğru olarak verilmiştir?
\begin{enumerate}[label=\alph*)]
    \item xInc=0.75,yInc=1.00
    \item xInc=7.00,yInc=9.00
    \item xInc=1.00,yInc=0.75
    \item xInc=0.78,yInc=1.00
    \item xInc=1.00,yInc=0.78
\end{enumerate}
\vfill\null\columnbreak
% SoruTip003End
% SoruTip004-cevap(C)
\section{OgrNu(8713)-Soru-4-Seed(5):}
Konu: Çizgi Çizmek \\
Puan: 10 \\
Soru:$[X=6,Y=8]$ noktasından $[X=13,Y=17]$ noktasına çizgi çizildiğinde
%X=5 nostası Y düzleminde veya Y=3 noktası X düzleminde
$Y=11$ noktası $X$ düzleminde 
hangi noktadan geçer?
\begin{enumerate}[label=\alph*)]
    \item 6
    \item 9
    \item 8
    \item 10
    \item 7
\end{enumerate}
% SoruTip004End
% SoruTip005-cevap(E)
\section{OgrNu(8713)-Soru-5-Seed(3):}
Konu: Renk Uzayları \\
Puan: 10 \\
Soru:$[R=255,G=0,B=0]$ (Kırmızı) rengi CMYK uzayında hangi renge eş gelir?
\begin{enumerate}[label=\alph*)]
    \item C(0),M(0),Y(1),K(0)
    \item C(1),M(1),Y(1),K(0)
    \item C(0),M(1),Y(1),K(1)
    \item C(1),M(0),Y(1),K(1)
    \item C(0),M(1),Y(1),K(0)
\end{enumerate}
% SoruTip005End
% SoruTip006-cevap(D)
\section{OgrNu(8713)-Soru-6-Seed(1):}
Konu: Renk Uzayları \\
Puan: 10 \\
Soru:$[C=0,M=0,Y=1,K=0]$ (Siyah) rengi RGB uzayında hangi renge eş gelir?
\begin{enumerate}[label=\alph*)]
    \item R(1),G(1),B(0)
    \item R(1),G(0),B(0)
    \item R(0),G(0),B(1)
    \item R(0),G(0),B(0)
    \item R(1),G(0),B(1)
\end{enumerate}
\vfill\null\columnbreak
% SoruTip006End
% SoruTip007-cevap(E)
\section{OgrNu(8713)-Soru-7-Seed(6):}
Konu: Renk Uzayları \\
Puan: 10 \\
Soru:$[R=255,G=255,B=0]$ (Sarı) rengi için hangi kartuşlar kullanılmaz
\begin{enumerate}[label=\alph*)]
    \item CYK
    \item YK
    \item MY
    \item Hiçbiri
    \item CMK
\end{enumerate}
% SoruTip007End
% SoruTip008-cevap(B)
\section{OgrNu(8713)-Soru-8-Seed(15):}
Konu: Renk Uzayları \\
Puan: 10 \\
Soru:Camgöbeği renginin 24 bitlik düzendeki yazılışı hangisidir?
\begin{enumerate}[label=\alph*)]
    \item 111111111111111111111111
    \item 000000001111111111111111
    \item 000000000000000000000000
    \item 111111110000000000000000
    \item 000000001111111100000000
\end{enumerate}
% SoruTip008End
% SoruTip009-cevap(A)
\section{OgrNu(8713)-Soru-9-Seed(18):}
Konu: Renk Uzayları \\
Puan: 10 \\
Soru:$[R,G,B]$ renginde her renk 5 bit ile ifade edilseydi kaç farklı tonda renk üretilebilirdi?
\begin{enumerate}[label=\alph*)]
    \item 96
    \item 486
    \item 30
    \item 10
    \item 64
\end{enumerate}
% SoruTip009End



\end{multicols}
\newpage

