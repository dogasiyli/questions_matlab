function [soru_06, correct_selection_id] = generate_test_soru_06(base_soru, ogrenci_str, selection_idx, verbose)
% % SoruTip006Begin
% \section{Soru-X:}
% Konu: Renk Uzayları \\
% Puan: SoruTip006Puan \\
% Soru:$[C,M,Y,K]$ rengi RGB uzayında hangi renge eş gelir?
% \begin{enumerate}[label=\alph*)]
%     \item Select-A
%     \item Select-B
%     \item Select-C
%     \item Select-D
%     \item Select-E
% \end{enumerate}
% % SoruTip006End
%   base_soru = soru_base{6}
    soru_id = 6;
    rng(selection_idx)
    [rgb_vec, cmyk_vec, color_name] = get_rgb_cmyk(selection_idx);
    soru_06 = strrep(base_soru, '{Soru-X:}',['{' ogrenci_str '-Soru-' num2str(soru_id, '%d')  '-Seed(' num2str(selection_idx)  '):}']);
    soru_06 = strrep(soru_06, '$[C,M,Y,K]$', ['$[C=' num2str(cmyk_vec(1),'%d') ',M=' num2str(cmyk_vec(2),'%d') ',Y=' num2str(cmyk_vec(4),'%d') ',K=' num2str(cmyk_vec(3),'%d') ']$ (' color_name ')']); 
    
    [answers_cell, correct_selection_id] = latex_test_generate_answer_06(rgb_vec, randi(50));
    
    for i=1:5
        old_str = ['Select-' char(('A')+i-1)];
        new_str = answers_cell{i};
        soru_06 = strrep(soru_06, old_str, new_str);
    end 
    if verbose>0
        disp(['soru_06 : \n' soru_06])
        disp(['cevap : ' char(('A')+correct_selection_id-1)])
        disp(['cevap : ' color_name ', rgb_vec(' mat2str(rgb_vec) '), cmyk_vec(' mat2str(cmyk_vec) ')' ])
    end
end

function [answers_cell, correct_selection_id] = latex_test_generate_answer_06(rgb_vec, seed_val)
    rng(seed_val)
    answers_cell = cell(1,5);
    rgb_vec = rgb_vec/255;
    
    correct_answer = 1+bin2dec(strrep(int2str(rgb_vec), ' ', ''));
    possible_vec = 1:8;
    possible_vec(correct_answer) = [];
    possible_vec = possible_vec(randperm(7));
    possible_vec = [correct_answer possible_vec(1:4)];
    possible_vec = possible_vec(randperm(5));  
    correct_selection_id = find(possible_vec==correct_answer,1);

    for i=1:5
        ans_ca = dec2bin(possible_vec(i)-1);
        while length(ans_ca)<3
            ans_ca = ['0' ans_ca];
        end
        answers_cell{i} = ['R(' ans_ca(1) '),G(' ans_ca(2) '),B(' ans_ca(3) ')'];
    end 

    assert(sum(possible_vec(correct_selection_id)==correct_answer)~=0, 'problem here')
end

