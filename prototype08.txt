#include <iostream>
using namespace std;

void changeVal(int *X, int val, int inds) {
	X[inds] = val;
}

void printX(int *X, int r, int c)
{
	for(int ir = 0; ir<r ; ir++)
	{
		for(int jc=0; jc < c; jc++)
			printf("%%d,",X[ir* c + jc]);
		printf("\n");
	}
	printf("**\n");
}

int main()
{
	int i = 1;
	int X[][ARR_SIZE_2] = {ARR_BLOCKS};
	int indsArr[] = INDS_ARR;
	int valArr[] = VALS_ARR;
	for(int i = 0; i<5; i++)
	{
		changeVal(&X[0][0], valArr[i], indsArr[i]);
		printX(&X[0][0],ARR_SIZE_1,ARR_SIZE_2);
	}
}
