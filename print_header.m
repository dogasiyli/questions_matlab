function out_str =print_header(D, print_bool, sort_name)
    out_str = print_hor_line(D, false, sort_name);
    if ~strcmpi(sort_name,'bubsort')
        out_str = [out_str '  i |'];
    end    
    
    for i=1:length(D)
        out_str = [out_str ' ' num2str(i-1,'%02d') ' |'];
    end
    if sort_name=='inssort'
        out_str = [out_str '  k |ekle|D(k)' newline];
    elseif sort_name=='selsort'
        out_str = [out_str newline];
    elseif sort_name=='bubsort'
        out_str = [out_str ' mov|  k ' newline];
    end
    if print_bool
        fprintf('%s',out_str)
    end    
end