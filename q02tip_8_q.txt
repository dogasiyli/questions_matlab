int "X[K][L][M]" 3 boyutlu integer dizisi iÃ§in(K=10,L=6,M=7), X[a][b][c] == X[i] olarak dÃ¼ÅÃ¼nerek aÅaÄÄ±daki sorularÄ± cevaplayÄ±nÄ±z.
  a. "i = sub2ind(K,L,M,a,b,c)â fonksiyonunu, matematiksel veya program fonksiyonu olarak yazÄ±nÄ±z.
  b. "(a,b,c) = ind2sub(K,L,M,i)â fonksiyonunu, matematiksel veya program fonksiyonu olarak yazÄ±nÄ±z.
  c. buldugunuz fonksiyonlara gore asagidaki fonksiyonlari hesaplayiniz:
    i = sub2ind(10,6,7,9,4,6) --> i=?
    i = sub2ind(10,6,7,5,1,1) --> i=?
    i = sub2ind(10,6,7,7,5,2) --> i=?
    i = sub2ind(10,6,7,7,0,4) --> i=?
    [a,b,c] = ind2sub(10,6,7,197) --> a=___, b=___, c=___
    [a,b,c] = ind2sub(10,6,7,18) --> a=___, b=___, c=___
    [a,b,c] = ind2sub(10,6,7,60) --> a=___, b=___, c=___
    [a,b,c] = ind2sub(10,6,7,345) --> a=___, b=___, c=___
