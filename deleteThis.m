    str_ARR_BLOCKS = '';
    for r = 1:arrSize(2)
        c = (r-1)*arrSize(3)+1 : r*arrSize(3);
        str_arr_block_cur = strrep(strrep(strrep(mat2str(xArrAsVec(c)),' ',','),']','}'),'[','{');
        str_ARR_BLOCKS = [str_ARR_BLOCKS str_arr_block_cur ','];
    end
    str_ARR_BLOCKS = str_ARR_BLOCKS(1:end-1);