function [questSheet, answerSheet] = createQuestions(inputStruct)
    questSheet = '';
    answerSheet = '';
    tabCnt = 0;    
%Q03_stringsToDisp = {'KEDI','ARABA','MEYVE','SEPET','TEPELEME','SEKERLEME'};%[];%
%Q04_bitCount = [5 8];%[];
%repeatCounts = [10 10 length(Q03_stringsToDisp) length(Q04_bitCount) 5 5 4];    
%    repeatCounts = [0 0 length(Q03_stringsToDisp) length(Q04_bitCount) 0 0 4];    
%     feature('locale')
%     currentCharacterEncoding = slCharacterEncoding('ISO-8859-1');    
%     feature('locale')

    questionID = -1;
    repeatCounts = inputStruct.repeatCounts;
    optParamsStruct = [];
    for i=inputStruct.questionOrder
        if repeatCounts(i)>0
            optParamsStruct = struct;
            optParamsStruct.student_i = inputStruct.student_i;
            optParamsStruct.studentName = inputStruct.studentName;
            optParamsStruct.studentNumber = inputStruct.studentNumber;
            optParamsStruct.questionPuan = inputStruct.questPuanVec(i);
            switch i
                case 3
                    optParamsStruct.stringsToDisp = inputStruct.Q03_stringsToDisp;
                case 4
                    if (inputStruct.Q04_bitCount~=8)
                        optParamsStruct.bitCount = inputStruct.Q04_bitCount;
                    else
                        optParamsStruct.bitCount = 8 + randi(2,[1,repeatCounts(4)]);%sth between 9 and 10
                    end
            end
            eval(['[questSheetAdd, answerSheetAdd, questionID] = addQuestion' num2str(i,'%02d') '(questionID, repeatCounts(i), tabCnt, optParamsStruct);']);
            questSheet = [questSheet newLineFunc() questSheetAdd]; %#ok<AGROW>
            answerSheet = [answerSheet newLineFunc() answerSheetAdd];%#ok<AGROW>
        end
    end

    fileName_question = ['questions' filesep 'Ogrenci_' num2str(optParamsStruct.student_i,'%02d') '-' num2str(optParamsStruct.studentNumber) '-' optParamsStruct.studentName '_QuestionsAll.txt'];
    fileName_answer = ['questions' filesep 'Ogrenci_' num2str(optParamsStruct.student_i,'%02d') '-' num2str(optParamsStruct.studentNumber) '-' optParamsStruct.studentName '_AnswersAll.txt'];
    if exist(fileName_question,'file')
        delete(fileName_question);
    end
    if exist(fileName_answer,'file')
        delete(fileName_answer);
    end
    
    soruSayisi = sum(repeatCounts>0);%length(inputStruct.questionOrder);
    questSheetPuanInfo = ['Sinav ' num2str(soruSayisi) ' soru ve toplam ' num2str(sum(inputStruct.questPuanVec(repeatCounts>0))) ' puandan olusmakta olup, puan dagilimi su sekildedir:' newLineFunc()];
    questSheetPuanInfo = [questSheetPuanInfo '[SoruNu-Puan]-->'];  
    k = 0;
    for j=1:length(repeatCounts)
         if repeatCounts(j)>0
            k = k + 1;
            questSheetPuanInfo = [questSheetPuanInfo '[' num2str(k-1) '-' num2str(inputStruct.questPuanVec(inputStruct.questionOrder(k))) '],'];  
         end
    end
    questSheetPuanInfo = questSheetPuanInfo(1:end-1);
    
    questSheet = [questSheetPuanInfo newLineFunc() questSheet];
    answerSheet = [questSheetPuanInfo newLineFunc() answerSheet];
    
    
    fid = fopen(fileName_question,'w','n','UTF-8');
    fprintf(fid, questSheet);
    fclose(fid);
    
    fid = fopen(fileName_answer,'w','n','UTF-8');
    fprintf(fid, answerSheet);
    fclose(fid);
    
%     slCharacterEncoding(currentCharacterEncoding);

    % 1. for d�ng�s� ve break/continue/exit
    % 2. if (x<arr[0])...
    % 3. switch number case ... print arr[0]
    % 4. signed/unsigned int bit say?s?na g�re limitler
    % 5. 3D -> sub2ind, ind2sub
    % 6. fooRecursive() --> output
    % 7. 
    % 8. 2 boyutlu arr -> vekt�r indise g�re de?er de?i?tirme..
    % 9. global, local, static -> 5 farkl? fonksiyon ile sorgulama
    %10. 
end

function [questSheetAdd, answerSheetAdd, questionID] = addQuestion01(questionID, repeatCount, tabCnt, optParamsStruct)
    questionID = questionID+1;
    questNumStr = getQuestionNumberStr(questionID, optParamsStruct);
    
    if repeatCount>1
        questSentence = 'Asagidaki programlarin ciktisi ne olmali?';
    else
        questSentence = 'Asagidaki programin ciktisi ne olmali?';
    end
    weekAndSubject = 'Hafta 01, Konu : Donguler ve Cikis Komutlari';
    questSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    answerSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    for i=1:repeatCount
        if repeatCount>1
            questSheetAdd = [questSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
            answerSheetAdd = [answerSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
        end
        if mod(i,2)==0
            [q01, a01] = questType01(tabCnt+2, 0.2);
        else
            [q01, a01] = questType01(tabCnt+2, 0.7);
        end
        str = strcat(cellstr(repmat('.', 1, 2*(tabCnt+2))), a01{1,1});
        a01 = strjoin(str',newLineFunc());        

        questSheetAdd = [questSheetAdd newLineFunc() q01 newLineFunc()]; %#ok<AGROW>
        answerSheetAdd = [answerSheetAdd newLineFunc() a01 newLineFunc()]; %#ok<AGROW>
    end
    questSheetAdd = [questSheetAdd newLineFunc()]; 
    answerSheetAdd = [answerSheetAdd newLineFunc()];
end
function [questSheetAdd, answerSheetAdd, questionID] = addQuestion02(questionID, repeatCount, tabCnt, optParamsStruct)
    questionID = questionID+1;
    questNumStr = getQuestionNumberStr(questionID, optParamsStruct);
    a = round(rand(1));
    if repeatCount>1
        questSentence = 'foo_a ve foo_b fonksiyolari verilmistir. Buna gore asagidaki programlarin ciktisi ne olmalidir?';
    else
        if 0==a%either 1 or 0
            questSentence = 'foo_a fonksiyonu verilmistir. Buna gore asagidaki programin ciktisi ne olmalidir?';
        else
            questSentence = 'foo_b fonksiyonu verilmistir. Buna gore asagidaki programin ciktisi ne olmalidir?';
        end
    end
    weekAndSubject = 'Hafta 01, Konu : Donguler ve If-Else Yapilari.';
    questSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    answerSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);

    fileName_questPrototype = 'prototype01.txt';
    questPrototype = readFromFileWithBlanks(fileName_questPrototype);%uses ">="  

    questPrototype_a = strrep(questPrototype, 'void foo', 'void foo_a');
    questPrototype_b = strrep(questPrototype, 'void foo', 'void foo_b');
    questPrototype_b = strrep(questPrototype_b, '>=', '>');

    if repeatCount>1
        questSheetAdd = [questSheetAdd newLineFunc() questPrototype_a];
        questSheetAdd = [questSheetAdd newLineFunc() questPrototype_b];
    else
        if 0==a%either 1 or 0
            questSheetAdd = [questSheetAdd newLineFunc() questPrototype_a];
        else
            questSheetAdd = [questSheetAdd newLineFunc() questPrototype_b];
        end
    end

    for i=1:repeatCount
        if repeatCount>1
            questSheetAdd = [questSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
            answerSheetAdd = [answerSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
        end
        if mod(i,2)==a%either 1 or 0
            [q02, a02] = questType02(tabCnt+2, 0.2);
        else
            [q02, a02] = questType02(tabCnt+2, 0.7);
        end
        a02 = [repmat('.', 1, 2*(tabCnt+2)) a02];       %#ok<AGROW>

        questSheetAdd = [questSheetAdd newLineFunc() q02 newLineFunc()]; %#ok<AGROW>
        answerSheetAdd = [answerSheetAdd newLineFunc() a02 newLineFunc()]; %#ok<AGROW>
    end
    questSheetAdd = [questSheetAdd newLineFunc()];
    answerSheetAdd = [answerSheetAdd newLineFunc()]; 
end
function [questSheetAdd, answerSheetAdd, questionID] = addQuestion03(questionID, repeatCount, tabCnt, optParamsStruct)
    questionID = questionID+1;
    questNumStr = getQuestionNumberStr(questionID, optParamsStruct);
    
    if isstruct(optParamsStruct) && isfield(optParamsStruct, 'stringsToDisp')
        stringsToDisp = optParamsStruct.stringsToDisp;
    else
        stringsToDisp = {'KEDI','ARABA','MEYVE','SEPET','TEPELEME'};
    end
    repeatCount = length(stringsToDisp);
    
    if repeatCount>1
        questSentence = 'foo fonksiyonu verilmistir. Buna gore asagidaki programlarin ciktisi ne olmali?';
    else
        questSentence = 'foo fonksiyonu verilmistir. Buna gore asagidaki programin ciktisi ne olmali?';
    end
    weekAndSubject = 'Hafta 01, Konu : Donguler ve Switch-Case yapisi.';
    questSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    answerSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    
    fileName_questPrototype = 'prototype02.txt';
    questPrototype = readFromFileWithBlanks(fileName_questPrototype);%uses ">="  
    questSheetAdd = [questSheetAdd newLineFunc() questPrototype];
    
    for i=1:repeatCount
        if repeatCount>1
            questSheetAdd = [questSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
            answerSheetAdd = [answerSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
        end
        [q03, a03] = questType03(stringsToDisp{i}, tabCnt+2);
        a03 = [repmat('.', 1, 2*(tabCnt+2)) a03];       %#ok<AGROW>
        questSheetAdd = [questSheetAdd newLineFunc() q03 newLineFunc()]; %#ok<AGROW>
        answerSheetAdd = [answerSheetAdd newLineFunc() a03 newLineFunc()]; %#ok<AGROW>
    end
    questSheetAdd = [questSheetAdd newLineFunc()];
    answerSheetAdd = [answerSheetAdd newLineFunc()];
end
function [questSheetAdd, answerSheetAdd, questionID] = addQuestion04(questionID, repeatCount, tabCnt, optParamsStruct)
    questionID = questionID+1;
    questNumStr = getQuestionNumberStr(questionID, optParamsStruct);    
    if isstruct(optParamsStruct) && isfield(optParamsStruct, 'bitCount')
        bitCount = optParamsStruct.bitCount;
    else
        bitCount = 5;
    end
    repeatCount = length(bitCount);
    
    questSentence = 'Asagidaki sorularin cevaplarini yanlarina yaziniz.';
    weekAndSubject = 'Hafta 04, Konu : Degiskenlerin tanimli oldugu araliklar.';
    questSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    answerSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    for i=1:repeatCount
        if repeatCount>1
            questSheetAdd = [questSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
            answerSheetAdd = [answerSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
        end
        [q04, a04] = questType04(bitCount(i), tabCnt+1);      
        questSheetAdd = [questSheetAdd newLineFunc() q04 newLineFunc()]; %#ok<AGROW>
        answerSheetAdd = [answerSheetAdd newLineFunc() a04 newLineFunc()];%#ok<AGROW>
    end
    questSheetAdd = [questSheetAdd newLineFunc()];
    answerSheetAdd = [answerSheetAdd newLineFunc()];
end
function [questSheetAdd, answerSheetAdd, questionID] = addQuestion05(questionID, repeatCount, tabCnt, optParamsStruct)
    stringsToUse = {'Tramvay','Kertenkele','Suluk','Musluk','Miralay','Fare','Kemence','Bilgisayar','Sekonder','Palet'};
    stringIDsShuffle = randperm(length(stringsToUse));
    stringIDsShuffle(repeatCount+1:end) = [];
    
    questionID = questionID+1;
    questNumStr = getQuestionNumberStr(questionID, optParamsStruct);
    if repeatCount>1
        questSentence = 'Asagidaki programlarin hafizada kapladigi boyutu hesaplayiniz.';
    else
        questSentence = 'Asagidaki programin hafizada kapladigi boyutu hesaplayiniz.';
    end
    weekAndSubject = 'Hafta 03, Konu : Veri yapilari ve hafiza.';
    questSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    answerSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    
    fileName_questPrototype = 'prototype04.txt';
    questPrototype = readFromFileWithBlanks(fileName_questPrototype); 
    questSheetAdd = [questSheetAdd newLineFunc() questPrototype];
    
    for i=1:repeatCount
        if repeatCount>1
            questSheetAdd = [questSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
            answerSheetAdd = [answerSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
        end
        [q05, a05] = questType05(stringsToUse{stringIDsShuffle(i)}, tabCnt+2);     
        questSheetAdd = [questSheetAdd newLineFunc() q05 newLineFunc()]; %#ok<AGROW>
        answerSheetAdd = [answerSheetAdd newLineFunc() a05 newLineFunc()]; %#ok<AGROW>
    end      
    questSheetAdd = [questSheetAdd newLineFunc()];
    answerSheetAdd = [answerSheetAdd newLineFunc()];
end
function [questSheetAdd, answerSheetAdd, questionID] = addQuestion06(questionID, repeatCount, tabCnt, optParamsStruct)
    questionID = questionID+1;
    questNumStr = getQuestionNumberStr(questionID, optParamsStruct);
    if repeatCount>1
        questSentence = 'fooRecursive fonksiyonu verilmistir. Buna gore asagidaki programlarin ciktisi ne olmalidir?';
    else
        questSentence = 'fooRecursive fonksiyonu verilmistir. Buna gore asagidaki programin ciktisi ne olmalidir?';
    end
    weekAndSubject = 'Hafta 04, Konu : Recursion';
    questSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    answerSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    
    fileName_questPrototype = 'prototype05.txt';
    questPrototype = readFromFileWithBlanks(fileName_questPrototype);  
    questSheetAdd = [questSheetAdd newLineFunc() questPrototype];
    nums = 50+randperm(50);
    for i=1:repeatCount
        if repeatCount>1
            questSheetAdd = [questSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
            answerSheetAdd = [answerSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
        end
        [q06, a06] = questType06(nums(i), tabCnt);     
        questSheetAdd = [questSheetAdd newLineFunc() q06 newLineFunc() newLineFunc()]; %#ok<AGROW>
        answerSheetAdd = [answerSheetAdd newLineFunc() a06 newLineFunc() newLineFunc()]; %#ok<AGROW>
    end
end
function [questSheetAdd, answerSheetAdd, questionID] = addQuestion07(questionID, repeatCount, tabCnt, optParamsStruct)
    questionID = questionID+1;
    questNumStr = getQuestionNumberStr(questionID, optParamsStruct);
    if repeatCount>1
        questSentence = 'Asagida verilen dongu fonksiyonlarinin istenen diger dongu tipindeki eslenigini yaziniz.';
    else
        questSentence = 'Asagida verilen dongu fonksiyonunun istenen diger dongu tipindeki eslenigini yaziniz.';
    end
    weekAndSubject = 'Hafta 04, Konu : Recursion ve donguler';
    questSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    answerSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    
    nums = 50+randperm(50);
    %I will call [funcCells] = createLoop( loopTypes, i_lastValue, incVal, useExpVec, tabCnt) this function
    %loopTypes will always be [0 1 2 3]
    %i_lastValue and incVal as couple - i_lastValue will be 0 if incVal is -1, some value between [500-1000] if incVal is +1
    %useExpVec - will be 3 length vector with max value 6 in it
    %tabcnt is as known +2
    for i=1:repeatCount
        if repeatCount>1
            questSheetAdd = [questSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
            answerSheetAdd = [answerSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
        end
        if rand>0.5
            incVal = +1;
            i_lastValue = 500 + randi(500);
        else
            incVal = -1;
            i_lastValue = 0;
        end
        useExpVec = randperm(6,3);
        
        funcCells = createLoop([0 1 2 3], i_lastValue, incVal, useExpVec, tabCnt+2);
        %I need to select 0 and 1 from 1-2-3, then shuffle it 
        if rand>0.5
            q = 1;%question recursion
            a = 1 + randi(3); %answer sth else
           
        else
            q = 1 + randi(3);%question sth else
            a = 1;%answer recursion
        end
         qString = [getFuncName07(q) ' olarak verilen fonksiyonu ' getFuncName07(a) ' olacak sekilde yaziniz.'];
         q07 = [qString newLineFunc()];
         a07 = [qString newLineFunc()];
         q07 = [q07 newLineFunc() funcCells{q} newLineFunc()];
         a07 = [a07 newLineFunc() funcCells{a} newLineFunc()];
        
        %[q07, a07] = questType07_2(nums(i), tabCnt+2);     
        questSheetAdd = [questSheetAdd newLineFunc() q07 newLineFunc() newLineFunc()]; %#ok<AGROW>
        answerSheetAdd = [answerSheetAdd newLineFunc() a07 newLineFunc() newLineFunc()]; %#ok<AGROW>
    end
end
function [questSheetAdd, answerSheetAdd, questionID] = addQuestion08(questionID, repeatCount, tabCnt, optParamsStruct)
    questionID = questionID+1;
    questNumStr = getQuestionNumberStr(questionID, optParamsStruct);
    
    questSentence = 'Asagidaki programin ciktisi ne olmali?';

    weekAndSubject = 'Konu : ��aret�i ve diziler.';
    questSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    answerSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
       
    for i=1:repeatCount
        if repeatCount>1
            questSheetAdd = [questSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
            answerSheetAdd = [answerSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
        end
        arrSizeInds = randi(5);
        [q08, a08] = questType08(arrSizeInds, tabCnt);
        a08 = [repmat('.', 1, 2*(tabCnt+2)) a08];       %#ok<AGROW>
        questSheetAdd = [questSheetAdd newLineFunc() q08 newLineFunc()]; %#ok<AGROW>
        answerSheetAdd = [answerSheetAdd newLineFunc() a08 newLineFunc()]; %#ok<AGROW>
    end
    questSheetAdd = [questSheetAdd newLineFunc()];
    answerSheetAdd = [answerSheetAdd newLineFunc()];
end
function [questSheetAdd, answerSheetAdd, questionID] = addQuestion09(questionID, repeatCount, tabCnt, optParamsStruct)
    questionID = questionID+1;
    questNumStr = getQuestionNumberStr(questionID, optParamsStruct);
    
    questSentence = 'Asagidaki programin ciktisi ne olmali?';

    weekAndSubject = 'Konu : Scope.';
    questSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    answerSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
       

    [q09, a09] = questType09(tabCnt);
    a09 = [repmat('.', 1, 2*(tabCnt+2)) a09];
    questSheetAdd = [questSheetAdd newLineFunc() q09 newLineFunc()];
    answerSheetAdd = [answerSheetAdd newLineFunc() a09 newLineFunc()];

    questSheetAdd = [questSheetAdd newLineFunc()];
    answerSheetAdd = [answerSheetAdd newLineFunc()];
end
function [questSheetAdd, answerSheetAdd, questionID] = addQuestion10(questionID, repeatCount, tabCnt, optParamsStruct)
    questionID = questionID+1;
    questNumStr = getQuestionNumberStr(questionID, optParamsStruct);
    
    questSentence = 'Asagidaki programin ciktisi ne olmali?';

    weekAndSubject = 'Konu : Dizilerde indis hesaplama.';
    questSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    answerSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
       

    [q10, a10] = questType10(tabCnt);
    a10 = [repmat('.', 1, 2*(tabCnt+2)) a10];
    questSheetAdd = [questSheetAdd newLineFunc() q10 newLineFunc()];
    answerSheetAdd = [answerSheetAdd newLineFunc() a10 newLineFunc()];

    questSheetAdd = [questSheetAdd newLineFunc()];
    answerSheetAdd = [answerSheetAdd newLineFunc()];
end
function [questSheetAdd, answerSheetAdd, questionID] = addQuestion11(questionID, repeatCount, tabCnt, optParamsStruct)
    %bir structure tan?mlataca??m
    questionID = questionID+1;
    questNumStr = getQuestionNumberStr(questionID, optParamsStruct);
    
    if repeatCount>1
        questSentence = 'Asagida tanimlanan structure yapisini c programlama diline uygun olarak yaziniz.';
    else
        questSentence = 'Asagida tanimlanan structure yapisini c programlama diline uygun olarak yaziniz.';
    end
    weekAndSubject = 'Hafta 05, Konu : Structure Yapisi';
    questSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    answerSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    for i=1:repeatCount
        if repeatCount>1
            questSheetAdd = [questSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
            answerSheetAdd = [answerSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
        end
        
        [q11, a11] = questType11();

        questSheetAdd = [questSheetAdd newLineFunc() q11 newLineFunc()]; %#ok<AGROW>
        answerSheetAdd = [answerSheetAdd newLineFunc() a11 newLineFunc()]; %#ok<AGROW>
    end
    questSheetAdd = [questSheetAdd newLineFunc()]; 
    answerSheetAdd = [answerSheetAdd newLineFunc()];
end
function [questSheetAdd, answerSheetAdd, questionID] = addQuestion12(questionID, repeatCount, tabCnt, optParamsStruct)
    %bir structure tan?mlataca??m
    questionID = questionID+1;
    questNumStr = getQuestionNumberStr(questionID, optParamsStruct);
    
    if repeatCount>1
        questSentence = 'Asagida belirtilen ikili aga� islemlerini yapiniz.';
    else
        questSentence = 'Asagida belirtilen ikili aga� islemlerini yapiniz.';
    end
    weekAndSubject = 'Hafta 03, Konu : ikili agaclar';
    questSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    answerSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    for i=1:repeatCount
        if repeatCount>1
            questSheetAdd = [questSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
            answerSheetAdd = [answerSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
        end
        
        [q12, a12] = questType12();

        questSheetAdd = [questSheetAdd newLineFunc() q12 newLineFunc()]; %#ok<AGROW>
        answerSheetAdd = [answerSheetAdd newLineFunc() a12 newLineFunc()]; %#ok<AGROW>
    end
    questSheetAdd = [questSheetAdd newLineFunc()]; 
    answerSheetAdd = [answerSheetAdd newLineFunc()];
end
function [questSheetAdd, answerSheetAdd, questionID] = addQuestion13(questionID, repeatCount, tabCnt, optParamsStruct)
    %bir structure tan?mlataca??m
    questionID = questionID+1;
    questNumStr = getQuestionNumberStr(questionID, optParamsStruct);
    
    if repeatCount>1
        questSentence = 'Asagida belirtilen ikili aga� islemlerini yapiniz.';
    else
        questSentence = 'Asagida belirtilen ikili aga� islemlerini yapiniz.';
    end
    weekAndSubject = 'Hafta 03, Konu : ikili agaclar';
    questSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    answerSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    for i=1:repeatCount
        if repeatCount>1
            questSheetAdd = [questSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
            answerSheetAdd = [answerSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
        end
        
        [q13, a13] = questType13();

        questSheetAdd = [questSheetAdd newLineFunc() q13 newLineFunc()]; %#ok<AGROW>
        answerSheetAdd = [answerSheetAdd newLineFunc() a13 newLineFunc()]; %#ok<AGROW>
    end
    questSheetAdd = [questSheetAdd newLineFunc()]; 
    answerSheetAdd = [answerSheetAdd newLineFunc()];
end
function [questSheetAdd, answerSheetAdd, questionID] = addQuestion14(questionID, repeatCount, tabCnt, optParamsStruct)
    %bir structure tan?mlataca??m
    questionID = questionID+1;
    questNumStr = getQuestionNumberStr(questionID, optParamsStruct);
    
    if repeatCount>1
        questSentence = '-';
    else
        questSentence = '-';
    end
    weekAndSubject = 'Hafta 04, Konu : turetilmis veri yapilari';
    questSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    answerSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    for i=1:repeatCount
        if repeatCount>1
            questSheetAdd = [questSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
            answerSheetAdd = [answerSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
        end
        
        [q14, a14] = questType14();

        questSheetAdd = [questSheetAdd newLineFunc() q14 newLineFunc()]; %#ok<AGROW>
        answerSheetAdd = [answerSheetAdd newLineFunc() a14 newLineFunc()]; %#ok<AGROW>
    end
    questSheetAdd = [questSheetAdd newLineFunc()]; 
    answerSheetAdd = [answerSheetAdd newLineFunc()];
end
function [questSheetAdd, answerSheetAdd, questionID] = addQuestion15(questionID, repeatCount, tabCnt, optParamsStruct)
    %bir structure tan?mlataca??m
    questionID = questionID+1;
    questNumStr = getQuestionNumberStr(questionID, optParamsStruct);
    
    if repeatCount>1
        questSentence = '';
    else
        questSentence = '';
    end
    weekAndSubject = 'Konu : Genel Programlama Bilgisi';
    questSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    answerSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    for i=1:repeatCount
        if repeatCount>1
            questSheetAdd = [questSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
            answerSheetAdd = [answerSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
        end
        
        [q15, a15] = questType15();

        questSheetAdd = [questSheetAdd newLineFunc() q15 newLineFunc()]; %#ok<AGROW>
        answerSheetAdd = [answerSheetAdd newLineFunc() a15 newLineFunc()]; %#ok<AGROW>
    end
    questSheetAdd = [questSheetAdd newLineFunc()]; 
    answerSheetAdd = [answerSheetAdd newLineFunc()];
end
function [questSheetAdd, answerSheetAdd, questionID] = addQuestion16(questionID, repeatCount, tabCnt, optParamsStruct)
    %bir structure tan?mlataca??m
    questionID = questionID+1;
    questNumStr = getQuestionNumberStr(questionID, optParamsStruct);
    
    if repeatCount>1
        questSentence = '';
    else
        questSentence = '';
    end
    weekAndSubject = 'Konu : Genel Programlama Bilgisi';
    questSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    answerSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    for i=1:repeatCount
        if repeatCount>1
            questSheetAdd = [questSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
            answerSheetAdd = [answerSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
        end
        
        [q16, a16] = questType16();

        questSheetAdd = [questSheetAdd newLineFunc() q16 newLineFunc()]; %#ok<AGROW>
        answerSheetAdd = [answerSheetAdd newLineFunc() a16 newLineFunc()]; %#ok<AGROW>
    end
    questSheetAdd = [questSheetAdd newLineFunc()]; 
    answerSheetAdd = [answerSheetAdd newLineFunc()];
end
function [questSheetAdd, answerSheetAdd, questionID] = addQuestion17(questionID, repeatCount, tabCnt, optParamsStruct)
    %bir structure tan?mlataca??m
    questionID = questionID+1;
    questNumStr = getQuestionNumberStr(questionID, optParamsStruct);
    
    questSentence = 'Asagidaki kodun ekran ciktisi nedir?';
    
    weekAndSubject = 'Konu : Pointer';
    questSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    answerSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    
    [q17, a17] = questType17();

    questSheetAdd = [questSheetAdd newLineFunc() q17 newLineFunc()];
    answerSheetAdd = [answerSheetAdd newLineFunc() a17 newLineFunc()];
        
    questSheetAdd = [questSheetAdd newLineFunc()]; 
    answerSheetAdd = [answerSheetAdd newLineFunc()];
end
function [questSheetAdd, answerSheetAdd, questionID] = addQuestion18(questionID, repeatCount, tabCnt, optParamsStruct)
    %bir structure tan?mlataca??m
    questionID = questionID+1;
    questNumStr = getQuestionNumberStr(questionID, optParamsStruct);
    
    questSentence = 'Asagidaki push, pop, enqueue ve dequeue islemlerini uygulayiniz?';
    
    weekAndSubject = 'Konu : Yigin ve Listeler';
    questSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    answerSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    
    [q18, a18] = questType18();

    questSheetAdd = [questSheetAdd newLineFunc() q18 newLineFunc()];
    answerSheetAdd = [answerSheetAdd newLineFunc() a18 newLineFunc()];
        
    questSheetAdd = [questSheetAdd newLineFunc()]; 
    answerSheetAdd = [answerSheetAdd newLineFunc()];
end
function [questSheetAdd, answerSheetAdd, questionID] = addQuestion19(questionID, repeatCount, tabCnt, optParamsStruct)
    %bir structure tan?mlataca??m
    questionID = questionID+1;
    questNumStr = getQuestionNumberStr(questionID, optParamsStruct);
    
    questSentence = 'Soru seceneklerinde belirtilen fonksiyonlara bakarak her bir secenegi cozun.';
    
    weekAndSubject = 'Konu : Siralama algoritmalari';
    questSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    answerSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    
    [q19, a19] = questType19();

    questSheetAdd = [questSheetAdd newLineFunc() q19 newLineFunc()];
    answerSheetAdd = [answerSheetAdd newLineFunc() a19 newLineFunc()];
        
    questSheetAdd = [questSheetAdd newLineFunc()]; 
    answerSheetAdd = [answerSheetAdd newLineFunc()];
end


function questNumStr = getQuestionNumberStr(questionID, optParamsStruct)
    questNumStr = ['***Soru Nu[' num2str(questionID) ']' newLineFunc() 'Ogrenci[' num2str(optParamsStruct.student_i) '-' num2str(optParamsStruct.studentNumber) '-' optParamsStruct.studentName ']'  newLineFunc() 'Puan: ' num2str(optParamsStruct.questionPuan) newLineFunc() '--------->'  newLineFunc() ];
end

function s = getFuncName07(i)
    s = 'recursive';
    switch i
        case 2
            s = 'do-while';
        case 3
            s = 'while-do';
        case 4
            s = 'for-loop';
    end
end


function [questSheetAdd, answerSheetAdd, questionID] = addQuestionXX(questionID, repeatCount, tabCnt, optParamsStruct)
    questionID = questionID+1;
    questNumStr = [num2str(questionID) '. '];
    if repeatCount>1
        questSentence = 'Asagidaki programlarin ciktisi ne olmali?';
    else
        questSentence = 'Asagidaki programin ciktisi ne olmali?';
    end
    weekAndSubject = 'Hafta 01, Konu : Donguler ve Cikis Komutlari';
    questSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    answerSheetAdd = getStringWithTab([questNumStr  questSentence ' ' weekAndSubject], tabCnt);
    for i=1:repeatCount
        if repeatCount>1
            questSheetAdd = [questSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
            answerSheetAdd = [answerSheetAdd newLineFunc() getStringWithTab([char(96+i) '.' ], tabCnt+1)]; %#ok<AGROW>
        end
    end
end

function [questPrototype, answerPrototype] = questType01(tabCnt, a)
%  	  for (i=1;i<100;i++)
%     {
%         if (i>X01X)//X01X can be anything bigger than 70
%             break;
%         if (i>X02X)//X02X can be anything between [30 and X01X]
%             continue;
%         if (i>X05X)//X05X can be anything between [X02X and 120]
%             exit;
%         if (i%X03X==0)//X03X can be anything from [8 9 10 12]
%             continue;
%         if (i%X04X==0)//X04X can be anything from [2 3 4 5 6]
%             cout << i << endl;
%     }
%     cout << i << endl;
    questPrototype = '';
    evalStr = '';
    typeIdentStr = ['tip_' num2str(round(a))];
    questPrototype = [questPrototype getStringWithTab('int i=1;', tabCnt) newLineFunc()];
    if a>0.5
        questPrototype = [questPrototype getStringWithTab('for (i=1;i<100;i++)', tabCnt) newLineFunc()];
        questPrototype = [questPrototype getStringWithTab('{', tabCnt) newLineFunc()];
        evalStr = [evalStr 'for i=1:100, '];
        tabCnt=tabCnt+1;
    else
        questPrototype = [questPrototype getStringWithTab('while (i<100)', tabCnt) newLineFunc()];
        questPrototype = [questPrototype getStringWithTab('{', tabCnt) newLineFunc()];
        tabCnt=tabCnt+1;
        questPrototype = [questPrototype getStringWithTab('i++;', tabCnt) newLineFunc()];
        evalStr = [evalStr 'i=1;, while (i<100), i = i+1;,'];
    end
    
    X01X = 70 + randi(30);%sth between 70 and 100
    X02X = 30 + randi(X01X-30);%sth between [30 and X01X]
    %X05X = 30 + randi(69);%sth between [30 and 99]
    if rand>0.5
        %print out
        %make  make X05X bigger than both X01X and X02X
        X05X = min(max(X02X,X01X)+1,99);%sth between [X01X and X02X]
    else
        %dont print out
        %make  make X05X less than both X01X and X02X
        X05X = max(min(X02X,X01X)-1,29);%sth between [X01X and X02X]
    end
    X03X_set = [8 9 10 12];
    X04X_set = [4 5 6];
    X03X = X03X_set(randi(length(X03X_set)));
    X04X = X04X_set(randi(length(X04X_set)));
    
    questPrototype = [questPrototype getStringWithTab(['if (i>' num2str(X01X) ')'], tabCnt) newLineFunc()];
    questPrototype = [questPrototype getStringWithTab('break;', tabCnt+1) newLineFunc()];
    evalStr = [evalStr 'if (i>' num2str(X01X) '), break, end, '];%eval ended if_01
    
    questPrototype = [questPrototype getStringWithTab(['if (i>' num2str(X02X) ')'], tabCnt) newLineFunc()];
    questPrototype = [questPrototype getStringWithTab('continue;', tabCnt+1) newLineFunc()];
    evalStr = [evalStr 'if (i>' num2str(X02X) '), continue, end, '];%eval ended if_02
    
    questPrototype = [questPrototype getStringWithTab(['if (i>' num2str(X05X) ')'], tabCnt) newLineFunc()];
    questPrototype = [questPrototype getStringWithTab('exit(0);', tabCnt+1) newLineFunc()];
    evalStr = [evalStr 'if (i>' num2str(X05X) '), break, end, '];%eval ended if_03
    
    questPrototype = [questPrototype getStringWithTab(['if (i%%' num2str(X03X) '==0)'], tabCnt) newLineFunc()];
    questPrototype = [questPrototype getStringWithTab('continue;', tabCnt+1) newLineFunc()];
    evalStr = [evalStr 'if (mod(i,' num2str(X03X) ')==0), continue, end, '];%eval ended if_04
    
    questPrototype = [questPrototype getStringWithTab(['if (i%%' num2str(X04X) '==0)'], tabCnt) newLineFunc()];
    questPrototype = [questPrototype getStringWithTab('printf("%%d\\n",i);', tabCnt+1) newLineFunc()];
    evalStr = [evalStr 'if (mod(i,' num2str(X04X) ')==0), disp([num2str(i) newLineFunc()]), end, '];%eval ended if_05
    
    tabCnt=tabCnt-1;%we will get out of the for or while loop
    questPrototype = [questPrototype getStringWithTab('}', tabCnt) newLineFunc()];%end for or while loop
    
    questPrototype = [questPrototype getStringWithTab('printf("out-%%d\\n",i);', tabCnt) newLineFunc()];
    evalStr = [evalStr 'end, if (X01X<X05X && X05X>X02X), disp([''out-'' num2str(i) newLineFunc()]), end '];%the last disp i then eval ended for or while loop

    disp(questPrototype);
    
    fileName_question = ['q01' typeIdentStr '_q.txt'];
    fileName_answer = ['q01' typeIdentStr '_a.txt'];
    if exist(fileName_question,'file')
        delete(fileName_question);
    end
    if exist(fileName_answer,'file')
        delete(fileName_answer);
    end
    fid = fopen(fileName_question,'w');
    fprintf(fid, questPrototype);
    fclose(fid);

    diary(fileName_answer);
    eval(evalStr);
    diary('off');
    
    fid = fopen(fileName_answer,'r','n','ISO-8859-1');
    answerPrototype = textscan(fid,'%s');%fscanf(fid, '%s');
    fclose(fid);
end

function c = foo_ab(X, number, a)
	if(a>0.5 && number >= X(end)) || (a<=0.5 && number > X(end))
		c = 'A';
    elseif(a>0.5 && number >= X(end-1)) || (a<=0.5 && number > X(end-1))
        c = 'B';
    elseif(a>0.5 && number >= X(end-2)) || (a<=0.5 && number > X(end-2))
        c = 'C';
    elseif(a>0.5 && number >= X(end-3)) || (a<=0.5 && number > X(end-3))
        c = 'D';
    elseif(a>0.5 && number >= X(end-4)) || (a<=0.5 && number > X(end-4))
        c = 'E';
	else
        c = 'F';
	end
end
function [questPrototype, answerPrototype] = questType02(tabCnt, a)
% void foo(int X[], int number)
% {
% 	if(number >= X[5])
% 	{
% 		cout << "A";
% 	}
% 	else if(number >= X[4])
% 	{
% 		cout << "B";
% 	}
% 	else if(number >= X[3])
% 	{
% 		cout << "C";
% 	}
% 	else if(number >= X[2])
% 	{
% 		cout << "D";
% 	}
% 	else if(number >= X[1])
% 	{
% 		cout << "E";
% 	}
% 	else
% 	{
% 		cout << "F";
% 	}
% 	return;
% }
% void main()
% {
%    int X[] = {25, 45, 60, 70, 85, 95};
%    int nums[] = {32,56,76,132,45,23,54,23,7,9,4};
%    for (int i=0;i<10;i++)
%      foo(X, nums[i]);
%    return;
% }
    questPrototype = '';
    
    questPrototype = [questPrototype getStringWithTab('void main()', tabCnt) newLineFunc()];
    questPrototype = [questPrototype getStringWithTab('{', tabCnt) newLineFunc()];
    
    X = sort(randi(1000,1,5));
    Xstr = strrep(strrep(strrep(mat2str(X),' ',','),']','}'),'[','{');
    
    numCnt = 5 + randi(5);%select 5 to 10 random numbers
    numbers = [randi(1000,1,numCnt) X];%make sure to have the border values of each if condition 
    numbers = numbers(randperm(length(numbers)));
    numbers_str = strrep(strrep(strrep(mat2str(numbers),' ',','),']','}'),'[','{');
    
    questPrototype = [questPrototype getStringWithTab(['int X[] = ' Xstr ';'], tabCnt+1) newLineFunc()];
    questPrototype = [questPrototype getStringWithTab(['int nums[] = ' numbers_str ';'], tabCnt+1) newLineFunc()];
    questPrototype = [questPrototype getStringWithTab(['for (int i=0;i<' num2str(numCnt+5) ';i++)'], tabCnt+1) newLineFunc()];
    if (a>0.5)
        questPrototype = [questPrototype getStringWithTab('foo_a(X, nums[i]);', tabCnt+2) newLineFunc()];
    else
        questPrototype = [questPrototype getStringWithTab('foo_b(X, nums[i]);', tabCnt+2) newLineFunc()];
    end
    questPrototype = [questPrototype getStringWithTab('}', tabCnt) newLineFunc()];
    
    disp(questPrototype);
    
    typeIdentStr = ['tip_' num2str(round(a))];
    fileName_question = ['q02' typeIdentStr '_q.txt'];
    fileName_answer = ['q02' typeIdentStr '_a.txt'];
    if exist(fileName_question,'file')
        delete(fileName_question);
    end
    if exist(fileName_answer,'file')
        delete(fileName_answer);
    end
    fid = fopen(fileName_question,'w');
    fprintf(fid, questPrototype);
    fclose(fid);
    
    %now create the output here
    answerPrototype = '';
    for i=1:length(numbers)
        c = foo_ab(X, numbers(i), a);
        answerPrototype = [answerPrototype c];
    end
%     b = numbers-X';
%     if (a>0.5)
%         b(b>0)=-inf;
%     else
%         b(b>=0)=-inf;
%     end
%     [~,inds] = max(b,[],1);
%     charStr = 'ABCDEF';
    disp(['Output should be = ' answerPrototype]);
    
    fid = fopen(fileName_answer,'w');
    fprintf(fid, answerPrototype);
    fclose(fid);
end

function c = foo(letters, number)
    if (number>=1 && number<length(letters))
        c = letters(number);
    else
        c = letters(length(letters));
    end
end
function [questPrototype, answerPrototype] = questType03(strToDisp, tabCnt)
% void foo(char letters[], int number)
% {
%     switch(number)
%     {
%         case 0: 
%             cout << letters[0];
%             break;
%         case 1: 
%             cout << letters[1];
%             break;
%         case 2: 
%             cout << letters[2];
%             break;
%         case 3: 
%             cout << letters[3];
%             break;
%         case 4: 
%             cout << letters[4];
%             break;
%         default: 
%             cout << letters[5];
%     }
% 	return;
% } 
% void main()
% {
%    int X[] = {0,1,0,2,0};
%    char letters[]="ARBXY";
%    for (int i=0;i<5;i++)
%      foo(letters, X[i]);
% }
    questPrototype = '';
    
    questPrototype = [questPrototype getStringWithTab('void main()', tabCnt) newLineFunc()];
    questPrototype = [questPrototype getStringWithTab('{', tabCnt) newLineFunc()];
    
    if iscell(strToDisp)
        strToDisp = cell2mat(strToDisp);
    end
    
    letters = getUniqueLettersFromString(strToDisp, 6);
    %now what would be the integer sequence for the given string?
    X = getIndicesOfLettersFromSring(strToDisp, letters);
    
    if ~any(X>5)
        %pick any indice value to set to default
        slctedInd = X(randi(length(X)));
        letters([slctedInd 6]) = letters([6 slctedInd]);
        X = getIndicesOfLettersFromSring(strToDisp, letters);
    end
    
    if rand>0.5
        X(X==6) = 50 + randi(100);
    else
        X(X==6) = -1 - randi(100);
    end

    Xstr = strrep(strrep(strrep(mat2str(X-1),' ',','),']','}'),'[','{');

    questPrototype = [questPrototype getStringWithTab(['int X[] = ' Xstr ';'], tabCnt+1) newLineFunc()];
    questPrototype = [questPrototype getStringWithTab(['char letters[]="' letters '";'], tabCnt+1) newLineFunc()];
    questPrototype = [questPrototype getStringWithTab(['for (int i=0;i<' num2str(length(X)) ';i++)'], tabCnt+1) newLineFunc()];
    questPrototype = [questPrototype getStringWithTab('foo(letters, X[i]);', tabCnt+2) newLineFunc()];
    questPrototype = [questPrototype getStringWithTab('}', tabCnt) newLineFunc()];
    
    disp(questPrototype);
    
    fileName_question = 'q03_q.txt';
    fileName_answer = 'q03_a.txt';
    if exist(fileName_question,'file')
        delete(fileName_question);
    end
    if exist(fileName_answer,'file')
        delete(fileName_answer);
    end
    fid = fopen(fileName_question,'w');
    fprintf(fid, questPrototype);
    fclose(fid);
    
    %now create the output here
    answerPrototype = '';
    for i=1:length(X)
        c = foo(letters, X(i));
        answerPrototype = [answerPrototype c];
    end
    disp(['Output should be = ' answerPrototype]);
    
    fid = fopen(fileName_answer,'w');
    fprintf(fid, answerPrototype);
    fclose(fid);
end

function [questPrototype, answerPrototype] = questType04(bitCount, tabCnt)
% Eger integer bilgisayarda X001 bit ile ifade edilseydi
%   a. signed integer'in alabilecegi minimum deger ne olurdu?
%   b. signed integer'in alabilecegi maximum deger ne olurdu?
%   c. unsigned integer'in alabilecegi minimum deger ne olurdu?
%   d. unsigned integer'in alabilecegi maximum deger ne olurdu?
%   e. signed integer'in alabilecegi minimum degeri 2 bitlik duzende yaziniz:
%     X002
%   f. signed integer'in alabilecegi maximum degeri 2 bitlik duzende yaziniz:
%     X002
%   g. unsigned integer'in alabilecegi minimum degeri 2 bitlik duzende yaziniz:
%     X002
%   h. unsigned integer'in alabilecegi maximum degeri 2 bitlik duzende yaziniz:
%     X002

    fileName_questPrototype = 'prototype03.txt';
    questPrototype = readFromFileWithBlanks(fileName_questPrototype); 
    questPrototype = strrep(questPrototype, 'X001', num2str(bitCount));
    
    X002_str = repmat('_,',1,bitCount);
    X002_str = X002_str(1:end-1);
    questPrototype = strrep(questPrototype, 'X002', X002_str);
    
    disp(questPrototype);
    
    fileName_question = 'q03_q.txt';
    fileName_answer = 'q03_a.txt';
    if exist(fileName_question,'file')
        delete(fileName_question);
    end
    if exist(fileName_answer,'file')
        delete(fileName_answer);
    end
    [fid, errMsg] = fopen(fileName_question,'w');
    assert(fid>=0,['file(' fileName_question ') can not be opened. errMsg(' errMsg ')'])
    fprintf(fid, questPrototype);
    fclose(fid);
    
    %now create the output here
    answerPrototype = [getStringWithTab(['Eger integer bilgisayarda ' num2str(bitCount) ' bit ile ifade edilseydi'], tabCnt) newLineFunc()];
    tabCnt = tabCnt + 1;
    %   a. signed integer'in alabilecegi minimum deger ne olurdu?
    answerPrototype = [answerPrototype getStringWithTab(['a. -pow(2, ' num2str(bitCount-1) ')=' num2str(-1 * 2^(bitCount-1))], tabCnt) newLineFunc()];
    %   b. signed integer'in alabilecegi maximum deger ne olurdu?
    answerPrototype = [answerPrototype getStringWithTab(['b. pow(2, ' num2str(bitCount-1) ')-1=' num2str(2^(bitCount-1)-1)], tabCnt) newLineFunc()];
    %   c. unsigned integer'in alabilecegi minimum deger ne olurdu?
    answerPrototype = [answerPrototype getStringWithTab('c. 0', tabCnt) newLineFunc()];
    %   d. unsigned integer'in alabilecegi maximum deger ne olurdu?
    answerPrototype = [answerPrototype getStringWithTab(['d. pow(2, ' num2str(bitCount) ')=' num2str(2^(bitCount))], tabCnt) newLineFunc()];
    %   e. signed integer'in alabilecegi minimum degeri 2 bitlik duzende yaziniz:
    X002str = ['[1 ' repmat('0 ',1,bitCount-1)];
    answerPrototype = [answerPrototype getStringWithTab(['e. ' X002str(1:end-1) ']'], tabCnt) newLineFunc()];
    %   f. signed integer'in alabilecegi maximum degeri 2 bitlik duzende yaziniz:
    X002str = ['[0 ' repmat('1 ',1,bitCount-1)];
    answerPrototype = [answerPrototype getStringWithTab(['f. ' X002str(1:end-1) ']'], tabCnt) newLineFunc()];
    %   g. unsigned integer'in alabilecegi minimum degeri 2 bitlik duzende yaziniz:
    X002str = repmat('0 ',1,bitCount);
    answerPrototype = [answerPrototype getStringWithTab(['g. [' X002str(1:end-1) ']'], tabCnt) newLineFunc()];
    %   h. unsigned integer'in alabilecegi maximum degeri 2 bitlik duzende yaziniz:
    X002str = repmat('1 ',1,bitCount);
    answerPrototype = [answerPrototype getStringWithTab(['h. [' X002str(1:end-1) ']'], tabCnt) newLineFunc()];
    disp('answerPrototype:');
    disp(answerPrototype);
    
    fid = fopen(fileName_answer,'w');
    fprintf(fid, answerPrototype);
    fclose(fid);
end

function [questPrototype, answerPrototype] = questType05(strToEnter, tabCnt)
% void main()
% {
%    int A[] = {0,1,0,2,0};
%    char B[]="ARBXY";
%    float C[] = {0.3,1.2,0.2,2.2,0.8};
%    long long D[20];
% }
    questPrototype = '';
    
    questPrototype = [questPrototype getStringWithTab('void main()', tabCnt) newLineFunc()];
    questPrototype = [questPrototype getStringWithTab('{', tabCnt) newLineFunc()];
    
    numOfElements = [randi(10) length(strToEnter) randi(15) randi(100)];
    
    %now what would be the integer sequence for the given string?
    A = randi(500, 1, numOfElements(1));
    Astr = strrep(strrep(strrep(mat2str(A),' ',','),']','}'),'[','{');
    questPrototype = [questPrototype getStringWithTab(['int A[] = ' Astr ';'], tabCnt+1) newLineFunc()];
    
    questPrototype = [questPrototype getStringWithTab(['char B[]="' strToEnter '";'], tabCnt+1) newLineFunc()];
    
    C = 200*rand(1, numOfElements(3));
    Cstr = strrep(strrep(strrep(mat2str(C,5),' ',','),']','}'),'[','{');
    questPrototype = [questPrototype getStringWithTab(['float C[] = ' Cstr ';'], tabCnt+1) newLineFunc()];
    
    questPrototype = [questPrototype getStringWithTab(['long long D[' num2str(numOfElements(4)) '];'], tabCnt+1) newLineFunc()];
    questPrototype = [questPrototype getStringWithTab('}', tabCnt) newLineFunc()];
    
    disp(questPrototype);
    
    fileName_question = 'q04_q.txt';
    fileName_answer = 'q04_a.txt';
    if exist(fileName_question,'file')
        delete(fileName_question);
    end
    if exist(fileName_answer,'file')
        delete(fileName_answer);
    end
    fid = fopen(fileName_question,'w');
    fprintf(fid, questPrototype);
    fclose(fid);
    
    byteCnt = numOfElements(1)*2 + numOfElements(2)+1 + numOfElements(3)*2 + numOfElements(4)*4;
    %now create the output here
    a01 = [num2str(numOfElements(1)) ' integers --> ' num2str(numOfElements(1)) '*2 bytes = ' num2str(numOfElements(1)*2*8) ' bits.'];
    a02 = [num2str(numOfElements(2)+1) ' chars --> ' num2str(numOfElements(2)) ' + 1 bytes = ' num2str((numOfElements(2)+1)*8) ' bits.'];
    a03 = [num2str(numOfElements(3)) ' float --> ' num2str(numOfElements(3)) '*2 bytes = ' num2str(numOfElements(3)*2*8) ' bits.'];
    a04 = [num2str(numOfElements(4)) ' long --> ' num2str(numOfElements(4)) '*4 bytes = ' num2str(numOfElements(4)*4*8) ' bits.'];
    a05 = ['TOPLAM --> ' num2str(byteCnt) ' bytes = ' num2str(byteCnt*8) ' bits.'];
    answerPrototype = ['' getStringWithTab(a01, tabCnt+1) newLineFunc()];
    answerPrototype = [answerPrototype getStringWithTab(a02, tabCnt+1) newLineFunc()];
    answerPrototype = [answerPrototype getStringWithTab(a03, tabCnt+1) newLineFunc()];
    answerPrototype = [answerPrototype getStringWithTab(a04, tabCnt+1) newLineFunc()];
    answerPrototype = [answerPrototype getStringWithTab(a05, tabCnt+1) newLineFunc()];
    disp(['Output should be = ' newLineFunc() answerPrototype]);
    fid = fopen(fileName_answer,'w');
    fprintf(fid, answerPrototype);
    fclose(fid);
end

function s = fooRecursive(s, i)
	if (i==0)
		return;
	end
	if (mod(i,2)==0)
		s = [s num2str(i) newLineFunc()];%disp(num2str(i));
		s = fooRecursive(s, i/2);
    elseif (mod(i,3)==0)
		s = [s num2str(i) newLineFunc()];%disp(num2str(i));
		s = fooRecursive(s, i/3);
    else
		s = fooRecursive(s, i-1);
	end
end
function [questPrototype, answerPrototype] = questType06(num, tabCnt)
% void main()
% {
%    fooRecursive(28);
% }
    questPrototype = '';
    questPrototype = [questPrototype getStringWithTab('void main()', tabCnt) newLineFunc()];
    questPrototype = [questPrototype getStringWithTab('{', tabCnt) newLineFunc()];
    questPrototype = [questPrototype getStringWithTab(['fooRecursive(' num2str(num) ');'], tabCnt+1) newLineFunc()];
    questPrototype = [questPrototype getStringWithTab('}', tabCnt) newLineFunc()];
    disp(questPrototype);
    
    fileName_question = 'q06_q.txt';
    fileName_answer = 'q06_a.txt';
    if exist(fileName_question,'file')
        delete(fileName_question);
    end
    if exist(fileName_answer,'file')
        delete(fileName_answer);
    end
    fid = fopen(fileName_question,'w');
    fprintf(fid, questPrototype);
    fclose(fid);
    
    %now create the output here
    answerPrototype = fooRecursive('', num);
    disp(['Output should be = ' newLineFunc() answerPrototype]);
    fid = fopen(fileName_answer,'w');
    fprintf(fid, answerPrototype);
    fclose(fid);
end

function answerPrototype = questType07(num, tabCnt)
% void fooRecursive(int i)
% {
% 	if (i==0)
% 		return;
% 	if (i%2==0)
% 	{
% 		cout << i << endl;
% 		fooRecursive(i/2);
% 	}
% 	else if (i%3==0)
% 	{
% 		cout << i << endl;
% 		fooRecursive(i/3);
% 	}
% 	else
% 	{
% 		fooRecursive(i-1);
% 	}
% }
    answerPrototype = '';
    answerPrototype = [answerPrototype getStringWithTab('void fooRecursive(int i)', tabCnt) newLineFunc()];
    answerPrototype = [answerPrototype getStringWithTab('{', tabCnt) newLineFunc()];
    
    switch num
        case 1 %for
            answerPrototype = [answerPrototype getStringWithTab('for(;i>0;)', tabCnt+1) newLineFunc()];
        case 2 %while-do
            answerPrototype = [answerPrototype getStringWithTab('while(i>0)', tabCnt+1) newLineFunc()];            
        case 3 %do-while
            answerPrototype = [answerPrototype getStringWithTab('do', tabCnt+1) newLineFunc()];            
    end
    
    answerPrototype = [answerPrototype getStringWithTab('{', tabCnt+1) newLineFunc()];
        answerPrototype = [answerPrototype getStringWithTab('if(i%%2==0)', tabCnt+2) newLineFunc()];
        answerPrototype = [answerPrototype getStringWithTab('{', tabCnt+2) newLineFunc()];
            answerPrototype = [answerPrototype getStringWithTab('cout << i << endl;', tabCnt+3) newLineFunc()];
            answerPrototype = [answerPrototype getStringWithTab('i = i/2;', tabCnt+3) newLineFunc()];                
        answerPrototype = [answerPrototype getStringWithTab('}', tabCnt+2) newLineFunc()];
        answerPrototype = [answerPrototype getStringWithTab('else if(i%%3==0)', tabCnt+2) newLineFunc()];
        answerPrototype = [answerPrototype getStringWithTab('{', tabCnt+2) newLineFunc()];
            answerPrototype = [answerPrototype getStringWithTab('cout << i << endl;', tabCnt+3) newLineFunc()];
            answerPrototype = [answerPrototype getStringWithTab('i = i/3;', tabCnt+3) newLineFunc()];                
        answerPrototype = [answerPrototype getStringWithTab('}', tabCnt+2) newLineFunc()];
        answerPrototype = [answerPrototype getStringWithTab('else', tabCnt+2) newLineFunc()];
            answerPrototype = [answerPrototype getStringWithTab('i--;', tabCnt+3) newLineFunc()];

    switch num
        case 1 %for
            answerPrototype = [answerPrototype getStringWithTab('}', tabCnt+1) newLineFunc()];
        case 2 %while-do
            answerPrototype = [answerPrototype getStringWithTab('}', tabCnt+1) newLineFunc()];            
        case 3 %do-while
            answerPrototype = [answerPrototype getStringWithTab('}while(i>0);', tabCnt+1) newLineFunc()];            
    end
                    
    answerPrototype = [answerPrototype getStringWithTab('}', tabCnt) newLineFunc()];
    disp(answerPrototype);
    
    fileName_answer = 'q07_a.txt';
    if exist(fileName_answer,'file')
        delete(fileName_answer);
    end
    fid = fopen(fileName_answer,'w');
    fprintf(fid, fileName_answer);
    fclose(fid);

    disp(['Output should be = ' newLineFunc() answerPrototype]);
end

function strOut = printX(X, r, c)
    strOut = '';
    for ir = 1:r
        for jc = 1:c
            strOut = [strOut num2str(X(sub2ind([r,c],ir,jc))) ',' ]; %#ok<AGROW>
        end
        strOut = [strOut newLineFunc];%#ok<AGROW>
    end
    strOut = [strOut '**' newLineFunc];
end
function strOutAll = answType08(xArrAsVec, xArrSize, indsArr, valArr)
	%X[][ARR_SIZE_2] = {ARR_BLOCKS};
	%int indsArr[] = {1,0,2,4,3};
	%int valArr[] = {8, -12, -25, 2, 8};
    strOutAll = '';
    for i = 1:length(indsArr)
        xArrAsVec(indsArr(i)) = valArr(i);
        strOut = printX(xArrAsVec, xArrSize(2), xArrSize(3));%printX(&X[0][0],2,3);
        strOutAll = [strOutAll strOut];
    end
end
function [questPrototype, answerPrototype] = questType08(arrSizeInds, tabCnt)
    %xArrSize : 
    %1- 12 - 4 by 3
    %2- 12 - 3 by 4
    %3- 10 - 5 by 2
    %4- 15 - 5 by 3
    arrSizeSelections = [12 4 3;12 3 4;10 5 2;15 5 3];
    try
        arrSize = arrSizeSelections(arrSizeInds, :);
    catch
        arrSize = arrSizeSelections(randi(length(arrSizeSelections)), :);
    end
    
    xArrAsVec = randperm(1000,arrSize(1)+5);
    valArr = xArrAsVec(end:-1:end-4);
    xArrAsVec = xArrAsVec(1:end-5);
    indsArr = randperm(arrSize(1),5);
    
    
    
    str_ARR_BLOCKS = '';
    for r = 1:arrSize(2)
        c = (r-1)*arrSize(3)+1 : r*arrSize(3);
        str_arr_block_cur = strrep(strrep(strrep(mat2str(xArrAsVec(c)),' ',','),']','}'),'[','{');
        str_ARR_BLOCKS = [str_ARR_BLOCKS str_arr_block_cur ','];
    end
    str_ARR_BLOCKS = str_ARR_BLOCKS(1:end-1);
    
    str_indsArr = strrep(strrep(strrep(mat2str(indsArr),' ',','),']','}'),'[','{');
    str_valArr = strrep(strrep(strrep(mat2str(valArr),' ',','),']','}'),'[','{');

    fileName_questPrototype = 'prototype08.txt';
    questPrototype = readFromFileWithBlanks(fileName_questPrototype); 
    questPrototype = strrep(questPrototype, 'ARR_SIZE_2', num2str(arrSize(3)));
    questPrototype = strrep(questPrototype, 'ARR_SIZE_1', num2str(arrSize(2)));
    questPrototype = strrep(questPrototype, 'ARR_BLOCKS', str_ARR_BLOCKS);
    questPrototype = strrep(questPrototype, 'INDS_ARR', str_indsArr);
    questPrototype = strrep(questPrototype, 'VALS_ARR', str_valArr);
    %INDS_ARR, VALS_ARR
    
    disp(questPrototype);

    
    fileName_question = 'q08_q.txt';
    fileName_answer = 'q08_a.txt';
    if exist(fileName_question,'file')
        delete(fileName_question);
    end
    if exist(fileName_answer,'file')
        delete(fileName_answer);
    end
    fid = fopen(fileName_question,'w');
    fprintf(fid, questPrototype);
    fclose(fid);
    
    %now create the output here
    answerPrototype = answType08(xArrAsVec, arrSize, indsArr, valArr);
    disp(['Output should be = ' newLineFunc answerPrototype]);
    
    fid = fopen(fileName_answer,'w');
    fprintf(fid, answerPrototype);
    fclose(fid);
end


function strOut = f01(x, M0X)
	strOut = ['f01(' num2str(M0X(1)*x) ')' newLineFunc()];
end
function [strOut, X0X] = f02(y, X0X, M0X)
    strOut = ['f02_s(' num2str(M0X(2)*X0X(2)) ')' newLineFunc()];
    X0X(2) = y;
end
function strOut = f03(y, M0X)
	strOut = ['f03(' num2str(M0X(3)*y) ')' newLineFunc()];
end
function [strOut, X0X] = f04(y, X0X, M0X)
    strOut = ['f04(' num2str(M0X(4)*X0X(1)) ')' newLineFunc()];
    X0X(1) = y;
end
function [strOut, X0X] = f05(y, X0X, M0X)
    strOut = ['f05_g(' num2str(M0X(5)*X0X(1)) ')' newLineFunc()];
    X0X(1) = y;
    strOut = [strOut 'f05_l(' num2str(M0X(7)*M0X(6)*y) ')' newLineFunc()];
end
function answerPrototype = answType09(tabCnt, X0X, F_RAND_CALL_01_vals, F0X_1, F_RAND_CALL_02_vals, F0X_2, M0X, L0X)
    %F01_CALL_01
    ansLine01 = f01(X0X(1), M0X);
    
    %F_RAND_CALL_01
    ansLine02 = '';
    for i=1:3
        switch F_RAND_CALL_01_vals(i)
            case 2
                [curStr, X0X] = f02(F0X_1(i), X0X, M0X);
            case 3
                curStr = f03(F0X_1(i), M0X);
            case 4
                [curStr, X0X] = f04(F0X_1(i), X0X, M0X);
            otherwise
        end
        ansLine02 = [ansLine02 getStringWithTab(curStr, tabCnt)];
    end    
    
    %printf("main_g(%d)\n",x);//  g  -  l  -  s
    ansLine03 = ['main_g(' num2str(X0X(1)) ')' newLineFunc()];
    
    x = X0X(3);
    
    %F01_CALL_02
    ansLine04 = f01(x, M0X);
    
    %printf("main_l(%d)\n",x);//  g  -  l  -  s
    ansLine05 = ['main_l(' num2str(x) ')' newLineFunc()];
    
    %F_RAND_CALL_02
    ansLine06 = '';
    for i=1:3
        switch F_RAND_CALL_02_vals(i)
            case 2
                [curStr, X0X] = f02(F0X_2(i), X0X, M0X);
            case 3
                curStr = f03(F0X_2(i), M0X);
            case 4
                [curStr, X0X] = f04(F0X_2(i), X0X, M0X);
            otherwise
        end
        ansLine06 = [ansLine06 getStringWithTab(curStr, tabCnt)];
    end      
    
    [ansLine07, X0X] = f05(x-L0X(3), X0X, M0X);
    
    answerPrototype = [ansLine01 ansLine02 ansLine03 ansLine04 ansLine05 ansLine06 ansLine07];
end
function [questPrototype, answerPrototype] = questType09(tabCnt)
    X0X = 20 + randi(30, 1, 3);
    F0X_1 = 20 + randi(30, 1, 3);
    F0X_2 = 10 + randi(30, 1, 3);
    M0X = randi(3, 1, 7);
    L0X = 10 + randi(10, 1, 3);
    
    %newLineFunc()
    F01_CALL_01 = getStringWithTab(['f01(x,' num2str(L0X(1)) ');//  g  -  l  -  s'], tabCnt);
    F_RAND_CALL_01_vals = randperm(3) + 1;
    F_RAND_CALL_01_01 = getStringWithTab(['f' num2str(F_RAND_CALL_01_vals(1),'%02d') '(' num2str(F0X_1(1)) ');'], tabCnt);
    F_RAND_CALL_01_02 = getStringWithTab(['f' num2str(F_RAND_CALL_01_vals(2),'%02d') '(' num2str(F0X_1(2)) ');'], tabCnt);
    F_RAND_CALL_01_03 = getStringWithTab(['f' num2str(F_RAND_CALL_01_vals(3),'%02d') '(' num2str(F0X_1(3)) ');'], tabCnt);
    
    F01_CALL_02 = getStringWithTab(['f01(x,' num2str(L0X(2)) ');//  g  -  l  -  s'], tabCnt);
    
    F_RAND_CALL_02_vals = randperm(3) + 1;
    F_RAND_CALL_02_01 = getStringWithTab(['f' num2str(F_RAND_CALL_02_vals(1),'%02d') '(' num2str(F0X_2(1)) ');'], tabCnt);
    F_RAND_CALL_02_02 = getStringWithTab(['f' num2str(F_RAND_CALL_02_vals(2),'%02d') '(' num2str(F0X_2(2)) ');'], tabCnt);
    F_RAND_CALL_02_03 = getStringWithTab(['f' num2str(F_RAND_CALL_02_vals(3),'%02d') '(' num2str(F0X_2(3)) ');'], tabCnt);

    
    F05_Call = getStringWithTab(['f05(x-' num2str(L0X(3)) ');//  g  -  l  -  s'], tabCnt);
    
    fileName_questPrototype = 'prototype09.txt';
    questPrototype = readFromFileWithBlanks(fileName_questPrototype); 
    for i=1:3
        strToRep = ['X' num2str(i,'%02d')];
        questPrototype = strrep(questPrototype, strToRep, num2str(X0X(i)));
    end
    for i=1:2
        strToRep = ['L' num2str(i,'%02d')];
        questPrototype = strrep(questPrototype, strToRep, num2str(L0X(i)));
    end
    for i=1:7
        strToRep = ['M' num2str(i,'%02d')];
        questPrototype = strrep(questPrototype, strToRep, num2str(M0X(i)));
    end
    
    questPrototype = strrep(questPrototype, 'F01_CALL_01', F01_CALL_01);
    questPrototype = strrep(questPrototype, 'F_RAND_CALL_01_01', F_RAND_CALL_01_01);
    questPrototype = strrep(questPrototype, 'F_RAND_CALL_01_02', F_RAND_CALL_01_02);
    questPrototype = strrep(questPrototype, 'F_RAND_CALL_01_03', F_RAND_CALL_01_03);
    questPrototype = strrep(questPrototype, 'F01_CALL_02', F01_CALL_02);
    questPrototype = strrep(questPrototype, 'F_RAND_CALL_02_01', F_RAND_CALL_02_01);
    questPrototype = strrep(questPrototype, 'F_RAND_CALL_02_02', F_RAND_CALL_02_02);
    questPrototype = strrep(questPrototype, 'F_RAND_CALL_02_03', F_RAND_CALL_02_03);
    questPrototype = strrep(questPrototype, 'F05_Call', F05_Call);
    
    disp(questPrototype);
    
    %now create the output here
    answerPrototype = answType09(tabCnt, X0X, F_RAND_CALL_01_vals, F0X_1, F_RAND_CALL_02_vals, F0X_2, M0X, L0X);
    disp(['Output should be = ' newLineFunc answerPrototype]);
    
    saveQuestionAndAnswerToSeperateFiles(9, questPrototype, answerPrototype);
end

function [questPrototype, answerPrototype] = questType10(tabCnt)

    questPrototype = ['int "X[K][L][M]" 3 boyutlu integer dizisi için(K=X01,L=X02,M=X03), X[a][b][c] == X[i] olarak düşünerek aşağıdaki soruları cevaplayınız.' newLineFunc()];
    
    questPrototype = [questPrototype getStringWithTab('a. "i = sub2ind(K,L,M,a,b,c)“ fonksiyonunu, matematiksel veya program fonksiyonu olarak yazınız.', tabCnt+1) newLineFunc()];
    questPrototype = [questPrototype getStringWithTab('b. "(a,b,c) = ind2sub(K,L,M,i)“ fonksiyonunu, matematiksel veya program fonksiyonu olarak yazınız.', tabCnt+1) newLineFunc()];
    questPrototype = [questPrototype getStringWithTab('c. buldugunuz fonksiyonlara gore asagidaki fonksiyonlari hesaplayiniz:', tabCnt+1) newLineFunc()];
    
    KLM = randperm(5,3)+5;%numbers between 6 and 10
    for i=1:3
        strToRep = ['X' num2str(i,'%02d')];
        questPrototype = strrep(questPrototype, strToRep, num2str(KLM(i)));
    end
    
    ansVec_01 = NaN(4,4);
    for i=1:4
        a = randi(KLM(1))-1;
        b = randi(KLM(2))-1;
        c = randi(KLM(3))-1;
        ansVec_01(i,:) = [a,b,c,sub2ind_mine(KLM(2),KLM(3),a,b,c)];
        questPrototype = [questPrototype getStringWithTab(['i = sub2ind(' num2str(KLM(1)) ',' num2str(KLM(2)) ',' num2str(KLM(3)) ',' num2str(ansVec_01(i,1)) ',' num2str(ansVec_01(i,2)) ',' num2str(ansVec_01(i,3)) ') --> i=?'], tabCnt+2) newLineFunc()];
    end
    
    ansVec_02 = NaN(4,4);
    for i=1:4
        a = randi(KLM(1))-1;
        b = randi(KLM(2))-1;
        c = randi(KLM(3))-1;
        ansVec_02(i,:) = [a,b,c,sub2ind_mine(KLM(2),KLM(3),a,b,c)];
        questPrototype = [questPrototype getStringWithTab(['[a,b,c] = ind2sub(' num2str(KLM(1)) ',' num2str(KLM(2)) ',' num2str(KLM(3)) ',' num2str(ansVec_02(i,4)) ') --> a=___, b=___, c=___'], tabCnt+2) newLineFunc()];
    end
    
    disp(questPrototype);
    
    typeIdentStr = ['tip_' num2str(round(a))];
    fileName_question = ['q02' typeIdentStr '_q.txt'];
    fileName_answer = ['q02' typeIdentStr '_a.txt'];
    if exist(fileName_question,'file')
        delete(fileName_question);
    end
    if exist(fileName_answer,'file')
        delete(fileName_answer);
    end
    fid = fopen(fileName_question,'w');
    fprintf(fid, questPrototype);
    fclose(fid);
    
    %now create the output here
    answerPrototype = newLineFunc();
    answerPrototype = [answerPrototype getStringWithTab('a. i = axMxL + bxM + c.', tabCnt+1) newLineFunc()];
    answerPrototype = [answerPrototype getStringWithTab('b. c=i%%M, b = ((i-c)/M)%%L, a = floor(i/(L*M))', tabCnt+1) newLineFunc()];
    answerPrototype = [answerPrototype getStringWithTab('c. buldugunuz fonksiyonlara gore asagidaki fonksiyonlari hesaplayiniz:', tabCnt+1) newLineFunc()];
    
    for i=1:4
        answerPrototype = [answerPrototype getStringWithTab(['i(' num2str(ansVec_01(i,4)) ') = sub2ind(' num2str(KLM(1)) ',' num2str(KLM(2)) ',' num2str(KLM(3)) ',' num2str(ansVec_01(i,1)) ',' num2str(ansVec_01(i,2)) ',' num2str(ansVec_01(i,3)) ');'], tabCnt+2) newLineFunc()];
    end
    for i=1:4
        answerPrototype = [answerPrototype getStringWithTab(['(' num2str(ansVec_02(i,1)) ',' num2str(ansVec_02(i,2)) ',' num2str(ansVec_02(i,3)) ') = ind2sub(' num2str(KLM(1)) ',' num2str(KLM(2)) ',' num2str(KLM(3)) ',i(' num2str(ansVec_02(i,4)) ');'], tabCnt+2) newLineFunc()];
    end

    disp(['Output should be = ' answerPrototype]);
    
    fid = fopen(fileName_answer,'w');
    fprintf(fid, answerPrototype);
    fclose(fid);
end
function [questPrototype, answerPrototype] = questType11()
% create a struct with name ['dugum','node','yap?'],
% have ['an integer','a float','a char','a boolean'] with name YYYY
% have a pointer to same struct with name "prevElement"
% have a pointer to same struct with name "nextElement"
    structNameList = {'dugum','node','yapi'};
    elementBaseVarNameList = {'X','myX','Z','element'};
    elementBaseVarTypeList = {'integer','float','char','boolean'};
    elementBaseVarTypeAnswer = {'int','float','char','bool'};
    
    structNameSelected = randi(3);
    elementBaseVarNameSelected = randi(4);
    elementBaseVarTypeSelected = randi(4);

    fileName_questPrototype = 'prototype11.txt';
    questPrototype = readFromFileWithBlanks(fileName_questPrototype); 
    questPrototype = strrep(questPrototype, 'XXstructNameXX', structNameList{structNameSelected});
    questPrototype = strrep(questPrototype, 'XXelementNameXX', elementBaseVarNameList{elementBaseVarNameSelected});
    questPrototype = strrep(questPrototype, 'XXelementBaseVarTypeXX', elementBaseVarTypeList{elementBaseVarTypeSelected});

    fileName_answerPrototype = 'prototype11_a.txt';
    answerPrototype = readFromFileWithBlanks(fileName_answerPrototype); 
    answerPrototype = strrep(answerPrototype, 'XXstructNameXX', structNameList{structNameSelected});
    answerPrototype = strrep(answerPrototype, 'XXelementNameXX', elementBaseVarNameList{elementBaseVarNameSelected});
    answerPrototype = strrep(answerPrototype, 'XXelementBaseVarTypeXX', elementBaseVarTypeAnswer{elementBaseVarTypeSelected});

    disp(questPrototype);
    disp(['Output should be = ' newLineFunc() answerPrototype]);
    
    saveQuestionAndAnswerToSeperateFiles(11, questPrototype, answerPrototype);
end
function [questPrototype, answerPrototype] = questType12()
% 7 adet eleman verecegim sirayla.. bunlari agaca ekleyecekler
% ortaya cikan agaci kullanip 
    
    selected7 = 10 + randperm(89);
    selected7 = sort(selected7(1:7));
    
    preOrderedLists = [4 2 5 1 3 6 7;4 5 2 6 7 1 3;4 2 1 5 3 6 7;4 6 3 2 1 7 5; 4 3 6 7 5 2 1;4 3 2 6 5 7 1];
    selectedOrder = randi(6);
    ordered7 = selected7(preOrderedLists(selectedOrder,:));

    fileName_questPrototype = 'prototype12.txt';
    questPrototype = readFromFileWithBlanks(fileName_questPrototype); 
    questPrototype = strrep(questPrototype, 'XXXinsertElementsListXXX', mat2str(ordered7));

    fileName_answerPrototype = 'prototype12_a.txt';
    answerPrototype = readFromFileWithBlanks(fileName_answerPrototype); 
    answerPrototype = strrep(answerPrototype, 'XXXselectedOrderIDXXX', num2str(selectedOrder));
    answerPrototype = strrep(answerPrototype, 'XXXselectedOrderXXX', mat2str(preOrderedLists(selectedOrder,:)));
    
    
    XXX_OUT_A_XXX = [repmat('  ', 1, 3) num2str(selected7(4)) newLineFunc()];
    if (selectedOrder<=3)
        XXX_OUT_A_XXX = [XXX_OUT_A_XXX repmat('  ', 1, 1) num2str(selected7(2)) repmat('  ', 1, 2) num2str(selected7(5)) newLineFunc()];
        XXX_OUT_A_XXX = [XXX_OUT_A_XXX num2str(selected7(1)) repmat('  ', 1, 1) num2str(selected7(3)) repmat('  ', 1, 2) num2str(selected7(6)) newLineFunc()];
        XXX_OUT_A_XXX = [XXX_OUT_A_XXX repmat('  ', 1, 6) num2str(selected7(7))];
        
        XXX_OUT_B_XXX = selected7([4 2 1 3 5 6 7]);
        XXX_OUT_C_XXX = selected7([1 2 3 4 5 6 7]);
        XXX_OUT_D_XXX = selected7([1 3 2 7 6 5 4]);
    else
        XXX_OUT_A_XXX = [XXX_OUT_A_XXX repmat('  ', 1, 2) num2str(selected7(3)) repmat('  ', 1, 2) num2str(selected7(6)) newLineFunc()];
        XXX_OUT_A_XXX = [XXX_OUT_A_XXX repmat('  ', 1, 1) num2str(selected7(2)) repmat('  ', 1, 2) num2str(selected7(5)) repmat('  ', 1, 1) num2str(selected7(7)) newLineFunc()];
        XXX_OUT_A_XXX = [XXX_OUT_A_XXX num2str(selected7(1))];
        
        XXX_OUT_B_XXX = selected7([4 3 2 1 6 5 7]);
        XXX_OUT_C_XXX = selected7([1 2 3 4 5 6 7]);
        XXX_OUT_D_XXX = selected7([1 2 3 5 7 6 4]);
    end
    answerPrototype = strrep(answerPrototype, 'XXX_OUT_A_XXX', XXX_OUT_A_XXX);
    answerPrototype = strrep(answerPrototype, 'XXX_OUT_B_XXX', mat2str(XXX_OUT_B_XXX));
    answerPrototype = strrep(answerPrototype, 'XXX_OUT_C_XXX', mat2str(XXX_OUT_C_XXX));
    answerPrototype = strrep(answerPrototype, 'XXX_OUT_D_XXX', mat2str(XXX_OUT_D_XXX));
    
    disp(questPrototype);
    disp(['Output should be = ' newLineFunc() answerPrototype]);
    
    saveQuestionAndAnswerToSeperateFiles(12, questPrototype, answerPrototype);
end
function [questPrototype, answerPrototype] = questType13()
    fileName_questPrototype = 'prototype13.txt';
    questPrototype = readFromFileWithBlanks(fileName_questPrototype); 
    fileName_answerPrototype = 'prototype13_a.txt';
    answerPrototype = readFromFileWithBlanks(fileName_answerPrototype);     
    disp(questPrototype);
    disp(['Output should be = ' newLineFunc() answerPrototype]);
    saveQuestionAndAnswerToSeperateFiles(13, questPrototype, answerPrototype);
end
function [questPrototype, answerPrototype] = questType14()

    allNumbers8 = 10 + randperm(89);
    allNumbers8 = allNumbers8(1:8);
    
    fileName_questPrototype = 'prototype14.txt';
    questPrototype = readFromFileWithBlanks(fileName_questPrototype); 
    questPrototype = strrep(questPrototype, 'XXX_firstElement_XXX', num2str(allNumbers8(1)));
    questPrototype = strrep(questPrototype, 'XXX_01_XXX', num2str(allNumbers8(2)));
    questPrototype = strrep(questPrototype, 'XXX_02_XXX', num2str(allNumbers8(3)));
    questPrototype = strrep(questPrototype, 'XXX_03_XXX', num2str(allNumbers8(4)));
    questPrototype = strrep(questPrototype, 'XXX_04_XXX', num2str(allNumbers8(5)));
    questPrototype = strrep(questPrototype, 'XXX_05_XXX', num2str(allNumbers8(6)));
    questPrototype = strrep(questPrototype, 'XXX_06_XXX', num2str(allNumbers8(7)));
    questPrototype = strrep(questPrototype, 'XXX_07_XXX', num2str(allNumbers8(8)));

    fileName_answerPrototype = 'prototype14_a.txt';
    answerPrototype = readFromFileWithBlanks(fileName_answerPrototype); 
    answerPrototype = strrep(answerPrototype, 'XXX_firstElement_XXX', num2str(allNumbers8(1)));
    answerPrototype = strrep(answerPrototype, 'XXX_01_XXX', num2str(allNumbers8(2)));
    answerPrototype = strrep(answerPrototype, 'XXX_02_XXX', num2str(allNumbers8(3)));
    answerPrototype = strrep(answerPrototype, 'XXX_03_XXX', num2str(allNumbers8(4)));
    answerPrototype = strrep(answerPrototype, 'XXX_04_XXX', num2str(allNumbers8(5)));
    answerPrototype = strrep(answerPrototype, 'XXX_05_XXX', num2str(allNumbers8(6)));
    answerPrototype = strrep(answerPrototype, 'XXX_06_XXX', num2str(allNumbers8(7)));
    answerPrototype = strrep(answerPrototype, 'XXX_07_XXX', num2str(allNumbers8(8)));
    
    disp(questPrototype);
    disp(['Output should be = ' newLineFunc() answerPrototype]);
    
    saveQuestionAndAnswerToSeperateFiles(14, questPrototype, answerPrototype);
end
function [questPrototype, answerPrototype] = questType15()
    fileName_questPrototype = 'prototype15.txt';
    questPrototype = readFromFileWithBlanks(fileName_questPrototype); 
    fileName_answerPrototype = 'prototype15_a.txt';
    answerPrototype = readFromFileWithBlanks(fileName_answerPrototype);     
    disp(questPrototype);
    disp(['Output should be = ' newLineFunc() answerPrototype]);
    saveQuestionAndAnswerToSeperateFiles(15, questPrototype, answerPrototype);
end
function [questPrototype, answerPrototype] = questType16()
    fileName_questPrototype = 'prototype16.txt';
    questPrototype = readFromFileWithBlanks(fileName_questPrototype); 
    fileName_answerPrototype = 'prototype16_a.txt';
    answerPrototype = readFromFileWithBlanks(fileName_answerPrototype);     
    disp(questPrototype);
    disp(['Output should be = ' newLineFunc() answerPrototype]);
    saveQuestionAndAnswerToSeperateFiles(16, questPrototype, answerPrototype);
end
function [questPrototype, answerPrototype] = questType17()
    fileName_questPrototype = 'prototype17.txt';
    questPrototype = readFromFileWithBlanks(fileName_questPrototype); 
    fileName_answerPrototype = 'prototype17_a.txt';
    answerPrototype = readFromFileWithBlanks(fileName_answerPrototype);     
    
    XX_NUM = randperm(2000,3);
    questPrototype = strrep(questPrototype, 'XX_NUM_00', num2str(XX_NUM(1)));
    questPrototype = strrep(questPrototype, 'XX_NUM_01', num2str(XX_NUM(2)));
    questPrototype = strrep(questPrototype, 'XX_NUM_02', num2str(XX_NUM(3)));
    answerPrototype = strrep(answerPrototype, 'XX_NUM_00', num2str(XX_NUM(1)));
    answerPrototype = strrep(answerPrototype, 'XX_NUM_01', num2str(XX_NUM(2)));
    answerPrototype = strrep(answerPrototype, 'XX_NUM_02', num2str(XX_NUM(3)));
    
    chrInts = randperm(5,3);
    XX_CHR = [char('A' + chrInts(1)) char('a' + chrInts(2)) char('K' + chrInts(3))];
    questPrototype = strrep(questPrototype, 'XX_CHR_00', XX_CHR(1));
    questPrototype = strrep(questPrototype, 'XX_CHR_01', XX_CHR(2));
    questPrototype = strrep(questPrototype, 'XX_CHR_02', XX_CHR(3));
    answerPrototype = strrep(answerPrototype, 'XX_CHR_00', XX_CHR(1));
    answerPrototype = strrep(answerPrototype, 'XX_CHR_01', XX_CHR(2));
    answerPrototype = strrep(answerPrototype, 'XX_CHR_02', XX_CHR(3));
    
    XX_FLT = randperm(2000,3) + rand(1,3);
    questPrototype = strrep(questPrototype, 'XX_FLT_00', num2str(XX_FLT(1),'%4.2f'));
    questPrototype = strrep(questPrototype, 'XX_FLT_01', num2str(XX_FLT(2),'%4.2f'));
    questPrototype = strrep(questPrototype, 'XX_FLT_02', num2str(XX_FLT(3),'%4.2f'));
    answerPrototype = strrep(answerPrototype, 'XX_FLT_00', num2str(XX_FLT(1),'%4.2f'));
    answerPrototype = strrep(answerPrototype, 'XX_FLT_01', num2str(XX_FLT(2),'%4.2f'));
    answerPrototype = strrep(answerPrototype, 'XX_FLT_02', num2str(XX_FLT(3),'%4.2f'));

    XX_ADDRESSES = randperm(100,15)*4;
    for i=1:15
        questPrototype = strrep(questPrototype, ['XX_ADDRESSES(' num2str(i) ')'], num2str(XX_ADDRESSES(i)));
        answerPrototype = strrep(answerPrototype, ['XX_ADDRESSES(' num2str(i) ')'], num2str(XX_ADDRESSES(i)));
    end
    
    questPrototype = strrep(questPrototype, 'XX_ADDRESSES', mat2str(XX_ADDRESSES));
    
    disp(questPrototype);
    disp(['Output should be = ' newLineFunc() answerPrototype]);
    saveQuestionAndAnswerToSeperateFiles(17, questPrototype, answerPrototype);
end
function [questPrototype, answerPrototype] = questType18()
    fileName_questPrototype = 'prototype18.txt';
    questPrototype = readFromFileWithBlanks(fileName_questPrototype); 
    fileName_answerPrototype = 'prototype18_a.txt';
    answerPrototype = readFromFileWithBlanks(fileName_answerPrototype);     
    
    numOfNumbers = 10;
    
    XX_NUM = randi(4)*100 + randperm(100,numOfNumbers)-1;
    for i=1:numOfNumbers
        questPrototype = strrep(questPrototype, ['XX_NUM_' num2str(i-1,'%02d')], num2str(XX_NUM(i)));
        answerPrototype = strrep(answerPrototype, ['XX_NUM_' num2str(i-1,'%02d')], num2str(XX_NUM(i)));
    end
    
    disp(questPrototype);
    disp(['Output should be = ' newLineFunc() answerPrototype]);
    saveQuestionAndAnswerToSeperateFiles(17, questPrototype, answerPrototype);
end

function [astr, qstr] = get_sort_q_a(sort_name)
    sort_name = strrep(sort_name, '.txt', '');
    % this function will find a suitable array D=[x y z ...]
    % return the array for adding into the <qstr> - XXX_d?_XXX
    % also the sort answer string into <astr>
    min_el_cnt = 5;
    inc_el_cnt = 5;
    
    switch sort_name
        case 'sort_bubble'
            dArrElemCnt = min_el_cnt + randi(inc_el_cnt);
            rowcnt = 99;
            try_cnt = 0;
            while (rowcnt>10 || rowcnt<6)
                if try_cnt>0
                    disp([sort_name ' try(' num2str(try_cnt) ') - D=' mat2str(D) ' with rowcnt(' num2str(rowcnt) ') skipped' ])
                end
                D = randperm(99);
                D = D(1:dArrElemCnt);
                [out_str, rowcnt] = bubsort(D, false, false);
                try_cnt = try_cnt + 1;
                if mod(try_cnt,100)==0 && dArrElemCnt>min_el_cnt
                    dArrElemCnt = dArrElemCnt - 1;
                end
            end
        case 'sort_insertion'
            dArrElemCnt = min_el_cnt + randi(inc_el_cnt);
            rowcnt = 99;
            try_cnt = 0;
            while (rowcnt>10 || rowcnt<6)
                if try_cnt>0
                    disp([sort_name ' try(' num2str(try_cnt) ') - D=' mat2str(D) ' with rowcnt(' num2str(rowcnt) ') skipped' ])
                end
                D = randperm(99);
                D = D(1:dArrElemCnt);
                [out_str, rowcnt] = inssort(D, false, false);
                try_cnt = try_cnt + 1;
                if mod(try_cnt,100)==0 && dArrElemCnt>min_el_cnt
                    dArrElemCnt = dArrElemCnt - 1;
                end
            end          
        case 'sort_merge'
            dArrElemCnt = 8;
            D = randi(10) + randperm(8);
            out_str = strrep(strrep(mat2str(sort(D)), ']', ''), '[', '');
        case 'sort_selection'
            dArrElemCnt = min_el_cnt + inc_el_cnt;
            rowcnt = 99;
            try_cnt = 0;
            while (rowcnt>10 || rowcnt<6)
                if try_cnt>0
                    disp([sort_name ' try(' num2str(try_cnt) ') - D=' mat2str(D) ' with rowcnt(' num2str(rowcnt) ') skipped' ])
                end
                D = randperm(99);
                D = D(1:dArrElemCnt);
                [out_str, rowcnt] = selsort(D, false, false);
                try_cnt = try_cnt + 1;
                if mod(try_cnt,100)==0 && dArrElemCnt>min_el_cnt+2
                    dArrElemCnt = dArrElemCnt - 1;
                end
            end
                                   
        otherwise
    end
    astr = out_str; 
    qstr = strrep(strrep(mat2str(D), ']', ''), '[', '');
end
function [questPrototype, answerPrototype] = questType19()
    functionIDs = randperm(4);
    func_file_names = {'sort_bubble.txt'; 'sort_insertion.txt'; 'sort_merge.txt'; 'sort_selection.txt'};
    
    fileName_questPrototype = 'prototype19.txt';
    questPrototype = readFromFileWithBlanks(fileName_questPrototype); 
    fileName_answerPrototype = 'prototype19_a.txt';
    answerPrototype = readFromFileWithBlanks(fileName_answerPrototype); 
    
    for i=1:4
        func_file_name = func_file_names{functionIDs(i)};
        funcPrototype = readFromFileWithBlanks(func_file_name); 

        funcPrototypeQue = strrep(funcPrototype, 'XXXfuncnameXXX',['f' num2str(i)]);
        questPrototype = strrep(questPrototype, ['XXX_f' num2str(i) '_XXX'], funcPrototypeQue);
        
        funcPrototypeAns = strrep(funcPrototype, 'XXXfuncnameXXX',['f' num2str(i) '_' strrep(func_file_name, '.txt', '')]);
        
        [astr, qstr] = get_sort_q_a(func_file_name);
        questPrototype = strrep(questPrototype, ['XXX_d' num2str(i) '_XXX'], qstr);
        answerPrototype = strrep(answerPrototype, ['XXX_d' num2str(i) '_XXX'], qstr);
        answerPrototype = strrep(answerPrototype, ['XXX_f' num2str(i) '_XXX'], [funcPrototypeAns newline astr]);
    end
    
    disp(questPrototype);
    disp(['Output should be = ' newLineFunc() answerPrototype]);
    
    saveQuestionAndAnswerToSeperateFiles(19, questPrototype, answerPrototype);
end

function saveQuestionAndAnswerToSeperateFiles(questionID, questPrototype, answerPrototype)
    fileName_question = ['q' num2str(questionID,'%02d') '_q.txt'];
    fileName_answer = ['q' num2str(questionID,'%02d') '_a.txt'];
    if exist(fileName_question,'file')
        delete(fileName_question);
    end
    if exist(fileName_answer,'file')
        delete(fileName_answer);
    end
    
    questPrototype = strrep(questPrototype, '\n', '\\n');
    questPrototype = strrep(questPrototype, '%%', '%');
    questPrototype = strrep(questPrototype, '%', '%%');

    fid = fopen(fileName_question,'w','n','UTF-8');
    fprintf(fid, questPrototype);
    fclose(fid);
    fid = fopen(fileName_answer,'w');
    fprintf(fid, answerPrototype);
    fclose(fid);    
end
