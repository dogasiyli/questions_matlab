function cmyk_vec = convert_rgb_cmyk(rgb_vec)
    max_rgb_val = max(rgb_vec);
    if max_rgb_val > 1
        max_rgb_val = 255.0;
        rgb_vec = rgb_vec/max_rgb_val;
    end
    r = rgb_vec(1);
    g = rgb_vec(2);
    b = rgb_vec(3);
    
    cmyk_vec = zeros(1,4);
    k = 1-max(rgb_vec);
    cmyk_vec(1) = (1-r-k)/(1-k);%c
    cmyk_vec(2) = (1-g-k)/(1-k);%m
    cmyk_vec(3) = (1-b-k)/(1-k);%y
    cmyk_vec(4) = k;%k
end