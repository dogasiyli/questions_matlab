function [a,b,c] = ind2sub_mine(L,M,i)
    c = mod(i,M);
    b = mod(((i-c)/M),L);
    a = floor(i/(L*M));
end