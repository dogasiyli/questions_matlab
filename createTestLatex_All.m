function [soru_base, sorular, correct_selection] = createTestLatex_All( nameNumberCells, soruPuan, seed_val)
    %load('namesOfStudents_sample2.mat')
    %createTestLatex_All( nameNumberCells, [21], [1], [19]);
    
    disp(nameNumberCells)    
    display_question = true;
    page_begin = latex_test_get_page_begin();
    page_end = latex_test_get_page_end();
    questSheetAll = '';
    answerSheetAll = '';
    test_outputs_folder = 'test_outputs';
    questions_file_name = fullfile(test_outputs_folder, 'all-questions.txt');
    answers_file_name = fullfile(test_outputs_folder, 'all-answers.txt');
    soru_base = cell(9,1);
    for i=1:9
        soru_base{i}  = latex_test_get_soru_by_id( i, display_question);
    end
    str_ans_charray = 'ABCDE';
    for ogr_inds = 1:size(nameNumberCells,1)
        rng(seed_val + ogr_inds)
        correct_selection = zeros(9,1);
        verbose = 1;
        %soru-1 (ret_struct = generate_line(3, 6, 4, 2);)
        sorular = soru_base;
        questSheetStudent = '';
        ogrenci_str_1 = ['Ogr-Nu(' num2str(nameNumberCells{ogr_inds,2}) ')-AdSoyad(' nameNumberCells{ogr_inds,1} ')'];
        ogrenci_str = ['OgrNu(' num2str(nameNumberCells{ogr_inds,2}) ')'];
        ogrenci_file_name = fullfile(test_outputs_folder, [ogrenci_str '-questions.txt']);

        selection_idx_5_6_7 = randperm(8, 3);
        for i=1:9
            seed_val_2 = randi(50); %#ok<*NASGU>
            if i>=5 && i<=7
                seed_val_2 = selection_idx_5_6_7(i-4);
            end
            eval_str = ['generate_test_soru_' num2str(i,'%02d'), '(soru_base{' num2str(i), '}, ogrenci_str, seed_val_2, verbose);'];
            disp(eval_str)
            [sorular{i}, correct_selection(i)] = eval(eval_str);
            sorular{i} = strrep(sorular{i}, ['SoruTip' num2str(i, '%03d') 'Puan'], num2str(soruPuan(i)));    
            sorular{i} = strrep(sorular{i}, ['SoruTip' num2str(i, '%03d') 'Begin'], ['SoruTip' num2str(i, '%03d') '-cevap(' str_ans_charray(correct_selection(i)) ')']);    
            questSheetStudent = [questSheetStudent sorular{i} newLineFunc()]; 
        end

        questSheetStudent = [strrep(page_begin, 'OGRTANIMLAMASI', ogrenci_str_1) newLineFunc() questSheetStudent newLineFunc() page_end newLineFunc()]; 
        fid = fopen(ogrenci_file_name,'w','n','UTF-8');
        fprintf(fid, '%s', questSheetStudent);
        fclose(fid);
        
        questSheetAll = [questSheetAll questSheetStudent newLineFunc()];  %#ok<*AGROW>
        answerSheetAll = [answerSheetAll ogrenci_str_1 ': ' str_ans_charray(correct_selection) newLineFunc()];
    end
    fid = fopen(questions_file_name,'w','n','UTF-8');
    fprintf(fid, '%s', questSheetAll);
    fclose(fid);
    fid = fopen(answers_file_name,'w','n','UTF-8');
    fprintf(fid, '%s', answerSheetAll);
    fclose(fid);    
end

function soru_base = latex_test_get_soru_by_id( soru_id, display_question)
    if nargin<2 || ~exist('display_question', 'var')
        display_question = false;
    end
    quest_proto = readFromFileWithBlanks('grafik_test_base_2021.txt'); 
    soru_beg = strfind(quest_proto, ['% SoruTip'  sprintf('%03d',soru_id)   'Begin']);
    soru_end = strfind(quest_proto, ['% SoruTip'  sprintf('%03d',soru_id)   'End']);
    soru_base = quest_proto(soru_beg:soru_end+14);
    if display_question
        disp(soru_base)
    end
end

function page_begin = latex_test_get_page_begin()
    quest_proto = readFromFileWithBlanks('grafik_test_base_2021.txt'); 
    soru_beg = strfind(quest_proto, '% SoruTip001Begin');
    page_begin = quest_proto(1:soru_beg);
end

function page_end = latex_test_get_page_end()
    quest_proto = readFromFileWithBlanks('grafik_test_base_2021.txt'); 
    soru_end = strfind(quest_proto, '% SoruTip009End');
    page_end = quest_proto(soru_end+15:end);
end
