function combineStudentPapersIntoOne(nameNumberCells, baseFold, addStr)
    questSheetCombined = '';
    answerSheetCombined = '';
    for i=1:size(nameNumberCells,1)%3%
        studentName = nameNumberCells{i,1};
        studentNumber = nameNumberCells{i,2};     
        a_file = ['Ogrenci_' num2str(i-1,'%02d') '-' studentNumber '-' studentName '_AnswersAll.txt'];
        q_file = ['Ogrenci_' num2str(i-1,'%02d') '-' studentNumber '-' studentName '_QuestionsAll.txt'];
        a_str = readFromFileWithBlanks([baseFold filesep a_file]);
        q_str = readFromFileWithBlanks([baseFold filesep q_file]);
        questSheetCombined = [questSheetCombined newLineFunc() 'SINAV***SORU***' studentName '***SORU***SINAV' newLineFunc() q_str]; %#ok<AGROW>
        answerSheetCombined = [answerSheetCombined newLineFunc() 'SINAV***CEVAP***' studentName '***CEVAP***SINAV' newLineFunc() a_str];%#ok<AGROW>
    end  
    fileName_question = [baseFold filesep 'combQ' filesep addStr 'QuestionsAll.txt'];
    fileName_answer = [baseFold filesep 'combQ' filesep addStr 'AnswersAll.txt'];
    if exist(fileName_question,'file')
        delete(fileName_question);
    end
    if exist(fileName_answer,'file')
        delete(fileName_answer);
    end
    
    fid = fopen(fileName_question,'w','n','UTF-8');
    fprintf(fid, questSheetCombined);
    fclose(fid);
    
    fid = fopen(fileName_answer,'w','n','UTF-8');
    fprintf(fid, answerSheetCombined);
    fclose(fid);    
end


function strFile = readFromFileWithBlanks(fileName)
    strFile = '';
    fid = fopen(fileName,'r','n','UTF-8');
    tline = fgetl(fid);
    while ischar(tline)
        strFile = [strFile newLineFunc() tline];
        tline = fgetl(fid);
    end    
    strFile = [strFile newLineFunc()];
    fclose(fid);  
end