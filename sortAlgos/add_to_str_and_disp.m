function out_str = add_to_str_and_disp(out_str, new_str, print_bool)
    if print_bool
        disp(new_str);
    end
    out_str = [out_str new_str newline];
end