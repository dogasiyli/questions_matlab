function [out_str, rowcnt] = selsort(D, print_each_line_bool, print_out_str)
%selsort Takes an unsorted array D
%   Sorts it by printing out necessary table
    if nargin<2 || isempty(print_each_line_bool)
        print_each_line_bool = true;
    end
    if nargin<3 || isempty(print_out_str)
        print_out_str = false;
    end
    pel = print_each_line_bool;
    out_str = add_to_str_and_disp('', ['Baslangic D=', mat2str(D)], pel);

    N = length(D);
    header_str = print_header(D, pel, 'selsort');
    out_str = app2str(out_str, header_str);
    rowcnt=0;
    for i=1:N-1
        index = N;
        enkucuk = D(index);
        for j=i:N
            if D(j)<enkucuk
                enkucuk = D(j);
                index = j;
            end
        end
        D(index) = D(i);
        D(i) = enkucuk;
        row_str = print_D(D, i-1, pel, 'selsort', sort(unique([i index])), []);
        out_str = app2str(out_str, row_str);
        rowcnt=rowcnt+1;
    end
    row_str = print_hor_line(D, pel, 'selsort');
    out_str = app2str(out_str, row_str);
    
    if pel
        disp([num2str(rowcnt), ' islem yapildi.'])
        disp(['Son D=', mat2str(D)])    
    end
    
    out_str = app2str(out_str, [num2str(rowcnt) ' islem yapildi.' newline]);
    out_str = app2str(out_str, ['Son D=' mat2str(D) newline]);
    
    if print_out_str
        disp('out_str = ')
        fprintf('%s', out_str)  
    end
end
