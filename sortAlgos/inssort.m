function [out_str, rowcnt] = inssort(D, print_each_line_bool, print_out_str)
%inssort Takes an unsorted array D
%   Sorts it by printing out necessary table
    if nargin<2 || isempty(print_each_line_bool)
        print_each_line_bool = true;
    end
    if nargin<3 || isempty(print_out_str)
        print_out_str = false;
    end
    pel = print_each_line_bool;%;
    out_str = add_to_str_and_disp('', ['Baslangic D=', mat2str(D)], pel);

    N = length(D);
    header_str = print_header(D, pel, 'inssort');
    out_str = app2str(out_str, header_str);
    rowcnt=0;
    for i=2:N
        ekle = D(i);
        for k=i-1:-1:0
            if k>0 && ekle<=D(k)
                %disp([D, "--D01"])
                D(k+1) = D(k);
                %disp([D, "--D02"])
                row_str = print_D(D, i-1, pel, 'inssort', [k k+1], [k, ekle]);
                out_str = app2str(out_str, row_str);
                rowcnt=rowcnt+1;
            else
                break
            end
        end
        %if D(k+1) ~= ekle
            %disp([D, "--D11"])
            D(k+1) = ekle;
            %disp([D, "--D12"])
            row_str = print_D(D,i-1, pel, 'inssort', k+1, [k, ekle]);
            out_str = app2str(out_str, row_str);
            rowcnt=rowcnt+1;
        %end
    end
    row_str = print_hor_line(D, pel, 'inssort');
    out_str = app2str(out_str, row_str);
    
    if pel
        disp([num2str(rowcnt), ' islem yapildi.'])
        disp(['Son D=', mat2str(D)])    
    end

    out_str = app2str(out_str, [num2str(rowcnt) ' islem yapildi.' newline]);
    out_str = app2str(out_str, ['Son D=' mat2str(D) newline]);
    
    if print_out_str
        disp('out_str = ')
        fprintf('%s', out_str)  
    end
end
