function out_str = print_changed_D(D, changedIndiceVec, until_i, pel)
    %if pel - print along the way
    %else - only return the string to be displayed
    out_str = '';
    for i=1:length(D)
        if i<until_i
            out_str = [out_str 'xxxx|'];
            if pel
                fprintf('xxxx|')
            end
        elseif sum(i==changedIndiceVec)==0
            out_str = [out_str sprintf(' %2d |',D(i))];
            if pel
                fprintf('%s',sprintf(' %2d |',D(i)));
            end
        else
            out_str = [out_str sprintf('*%2d*|',D(i))];
            if pel
                cprintf(-[1,0.5,0],' %2d |',D(i));
            end
        end
    end
end