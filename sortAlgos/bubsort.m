function [out_str, rowcnt] = bubsort(D, print_each_line_bool, print_out_str)
%bubsort Takes an unsorted array D
%   Sorts it by printing out necessary table
    if nargin<2 || isempty(print_each_line_bool)
        print_each_line_bool = true;
    end
    if nargin<3 || isempty(print_out_str)
        print_out_str = false;
    end
    pel = print_each_line_bool;
    out_str = add_to_str_and_disp('', ['Baslangic D=', mat2str(D)], pel);
    
    N = length(D);
    header_str = print_header(D, pel, 'bubsort');
    out_str = app2str(out_str, header_str);
    rowcnt=0;
    for mov=1:N
        for k=1:N-mov
            if D(k)>D(k+1)
                %disp([D, "--D01"])
                D = swap_D(D,k,k+1);
                %disp([D, "--D02"])
                row_str = print_D(D, mov-1, pel, 'bubsort', [k k+1], [mov-1 k-1]);
                out_str = app2str(out_str, row_str);
                rowcnt=rowcnt+1;
            end
        end
    end
    row_str = print_hor_line(D,pel,'bubsort');
    out_str = app2str(out_str, row_str);
    
    if pel
        disp([num2str(rowcnt), ' islem yapildi.'])
        disp(['Son D=', mat2str(D)])    
    end

    out_str = app2str(out_str, [num2str(rowcnt) ' islem yapildi.' newline]);
    out_str = app2str(out_str, ['Son D=' mat2str(D) newline]);
    
    if print_out_str
        disp('out_str = ')
        fprintf('%s', out_str)  
    end
end

function D = swap_D(D,i,j)
    x = D(i);
    D(i) = D(j);
    D(j) = x;
end
