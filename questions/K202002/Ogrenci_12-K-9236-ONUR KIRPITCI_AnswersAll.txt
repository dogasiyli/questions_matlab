Sinav 5 soru ve toplam 100 puandan olusmakta olup, puan dagilimi su sekildedir:
[SoruNu-Puan]-->[0-10],[1-10],[2-10],[3-10],[4-60]

***Soru Nu[0]
Ogrenci[12-K-9236-ONUR KIRPITCI]
Puan: 10
--------->
Asagidaki sorularin cevaplarini yanlarina yaziniz. Hafta 04, Konu : Degiskenlerin tanimli oldugu araliklar.
  Eger integer bilgisayarda 7 bit ile ifade edilseydi
    a. -pow(2, 6)=-64
    b. pow(2, 6)-1=63
    c. 0
    d. pow(2, 7)=128
    e. [1 0 0 0 0 0 0]
    f. [0 1 1 1 1 1 1]
    g. [0 0 0 0 0 0 0]
    h. [1 1 1 1 1 1 1]



***Soru Nu[1]
Ogrenci[12-K-9236-ONUR KIRPITCI]
Puan: 10
--------->
Asagidaki programin hafizada kapladigi boyutu hesaplayiniz. Hafta 03, Konu : Veri yapilari ve hafiza.
      8 integers --> 8*2 bytes = 128 bits.
      11 chars --> 10 + 1 bytes = 88 bits.
      10 float --> 10*2 bytes = 160 bits.
      19 long --> 19*4 bytes = 608 bits.
      TOPLAM --> 123 bytes = 984 bits.



***Soru Nu[2]
Ogrenci[12-K-9236-ONUR KIRPITCI]
Puan: 10
--------->
 Konu : Genel Programlama Bilgisi

	BLOK_A - for öncesinde çalışır.
	BLOK_B - for bloğunun ilklendirici komutudur. Her zaman çalışır. Boş bırakılabilir.
	BLOK_C - for bloğunun sonlanacağı zamanı belirten koşul komutudur. Burasının ilk değeri "yanlış" ise döngü başlamaz. Boş bırakılması uygun değildir. Boş ise döngü içinde mutlaka break veya exit bulunmalıdır.
	BLOK_D - for bloğunun iterasyon komutudur. 
	BLOK_E - for bloğunun uygulama bloğudur. BLOK_E işlendikten sonra önce bu işlem gerçekleştirilir ardından BLOK_C'ye tekrar bakılır.
	BLOK_F - for sonrasında çalışır.
	
	sıralama - A,B,[C,E,D],F --> sırasıyla çalışır. D sonrası C'ye geri döner.



***Soru Nu[3]
Ogrenci[12-K-9236-ONUR KIRPITCI]
Puan: 10
--------->
- Hafta 04, Konu : turetilmis veri yapilari

	a. [68, ___, ___]
	b. Yığın - Stack
	c. 
		(1). push(91) - [68, 91, ___]
		(2). pop() [hangi sayı diziden çıkar = 91]
		(3). pop() [hangi sayı diziden çıkar = 68]
		(4). pop() [hangi sayı diziden çıkar = hiçbir sayı çıkmaz]
		(6). push(63) - [63, ___, ___]
		(7). push(88) - [63, 88, ___]
		(8). push(12) - [63, 88, 12]
	d. Queue
	e. 
		(1). dequeue() [hangi sayı diziden çıkar = 68]
		(2). dequeue() [hangi sayı diziden çıkar = hiçbir sayı çıkmaz]
		(3). enqueue(97) - [___, 97, ___]
		(4). enqueue(84) - [___, 97, 84]
		(5). enqueue(51) - [51, 97, 84]
		(6). dequeue() [hangi sayı diziden çıkar = 97]



***Soru Nu[4]
Ogrenci[12-K-9236-ONUR KIRPITCI]
Puan: 60
--------->
Soru seceneklerinde belirtilen fonksiyonlara bakarak her bir secenegi cozun. Konu : Siralama algoritmalari

Asagidaki seceneklerde belirtilen f1, f2, f3, f4 fonksiyonlara gore sorulari cozunuz.
	a. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[7 4 6 3 5 8 2 9] dizisini siralayiniz.

void f1_sort_merge(int arr[], int i, int n)  
{  
    if(n>1) 
    {
        j=n/2;
        ms(arr,i,j);
        ms(arr,j,n-j);
        m(D,i,j,n);
    }
}

2 3 4 5 6 7 8 9
	b. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[18 31 58 69 32 27 20 26 74 17] dizisini siralayiniz.

void f2_sort_selection(int arr[], int n)  
{  
    int i, j, k, m;
    for (i=0;i<n-1;i++) 
    {
        k=n-1;
        m=arr[k];
        for(j=i;j<n-1;j++)
            if(arr[j]<m)
            {
                k=j;
                m=arr[j];
            }
        arr[k]=arr[i];
        arr[i]=m;
    }
}

Baslangic D=[18 31 58 69 32 27 20 26 74 17]
_____--------------------------------------------------
  i | 00 | 01 | 02 | 03 | 04 | 05 | 06 | 07 | 08 | 09 |
_____--------------------------------------------------
 00 |*17*| 31 | 58 | 69 | 32 | 27 | 20 | 26 | 74 |*18*|
_____--------------------------------------------------
 01 | 17 |*18*| 58 | 69 | 32 | 27 | 20 | 26 | 74 |*31*|
_____--------------------------------------------------
 02 | 17 | 18 |*20*| 69 | 32 | 27 |*58*| 26 | 74 | 31 |
_____--------------------------------------------------
 03 | 17 | 18 | 20 |*26*| 32 | 27 | 58 |*69*| 74 | 31 |
_____--------------------------------------------------
 04 | 17 | 18 | 20 | 26 |*27*|*32*| 58 | 69 | 74 | 31 |
_____--------------------------------------------------
 05 | 17 | 18 | 20 | 26 | 27 |*31*| 58 | 69 | 74 |*32*|
_____--------------------------------------------------
 06 | 17 | 18 | 20 | 26 | 27 | 31 |*32*| 69 | 74 |*58*|
_____--------------------------------------------------
 07 | 17 | 18 | 20 | 26 | 27 | 31 | 32 |*58*| 74 |*69*|
_____--------------------------------------------------
 08 | 17 | 18 | 20 | 26 | 27 | 31 | 32 | 58 |*69*|*74*|
_____--------------------------------------------------
9 islem yapildi.
Son D=[17 18 20 26 27 31 32 58 69 74]

	c. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[10 23 27 9 66 88 83] dizisini siralayiniz.

void f3_sort_insertion(int arr[], int n)  
{  
    int i, j, k;  
    for (i=1;i<n;i++) 
    {  
        k = arr[i];  
        for(j=i-1;j>=0 && arr[j]>k;j--) 
            arr[j+1]=arr[j];  
        arr[j+1]=k;
    }
}

Baslangic D=[10 23 27 9 66 88 83]
_____-----------------------------------______________
  i | 00 | 01 | 02 | 03 | 04 | 05 | 06 |  k |ekle|D(k)
_____-----------------------------------______________
 01 | 10 |*23*| 27 |  9 | 66 | 88 | 83 |  0 | 23 | 10 
_____-----------------------------------______________
 02 | 10 | 23 |*27*|  9 | 66 | 88 | 83 |  1 | 27 | 23 
_____-----------------------------------______________
 03 | 10 | 23 |*27*|*27*| 66 | 88 | 83 |  2 |  9 | 27 
_____-----------------------------------______________
 03 | 10 |*23*|*23*| 27 | 66 | 88 | 83 |  1 |  9 | 23 
_____-----------------------------------______________
 03 |*10*|*10*| 23 | 27 | 66 | 88 | 83 |  0 |  9 | 10 
_____-----------------------------------______________
 03 |* 9*| 10 | 23 | 27 | 66 | 88 | 83 | -1 |  9 | NaN 
_____-----------------------------------______________
 04 |  9 | 10 | 23 | 27 |*66*| 88 | 83 |  3 | 66 | 27 
_____-----------------------------------______________
 05 |  9 | 10 | 23 | 27 | 66 |*88*| 83 |  4 | 88 | 66 
_____-----------------------------------______________
 06 |  9 | 10 | 23 | 27 | 66 |*88*|*88*|  5 | 83 | 88 
_____-----------------------------------______________
 06 |  9 | 10 | 23 | 27 | 66 |*83*| 88 |  4 | 83 | 66 
_____-----------------------------------______________
10 islem yapildi.
Son D=[9 10 23 27 66 83 88]

	d. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[61 22 4 6 28 93 55 77] dizisini siralayiniz.

void f4_sort_bubble(int arr[], int n)  
{  
    int i, j;
    for (i=0;i<n-1;i++) 
        for(j=0;j<n-1-i;j++) 
            if(arr[j]>arr[j+1]) 
	        swap(arr,j,j+1);
}

Baslangic D=[61 22 4 6 28 93 55 77]
----------------------------------------_________
 00 | 01 | 02 | 03 | 04 | 05 | 06 | 07 | mov|  k 
----------------------------------------_________
*22*|*61*|  4 |  6 | 28 | 93 | 55 | 77 |  0 |  0 
----------------------------------------_________
 22 |* 4*|*61*|  6 | 28 | 93 | 55 | 77 |  0 |  1 
----------------------------------------_________
 22 |  4 |* 6*|*61*| 28 | 93 | 55 | 77 |  0 |  2 
----------------------------------------_________
 22 |  4 |  6 |*28*|*61*| 93 | 55 | 77 |  0 |  3 
----------------------------------------_________
 22 |  4 |  6 | 28 | 61 |*55*|*93*| 77 |  0 |  5 
----------------------------------------_________
 22 |  4 |  6 | 28 | 61 | 55 |*77*|*93*|  0 |  6 
----------------------------------------_________
* 4*|*22*|  6 | 28 | 61 | 55 | 77 | 93 |  1 |  0 
----------------------------------------_________
  4 |* 6*|*22*| 28 | 61 | 55 | 77 | 93 |  1 |  1 
----------------------------------------_________
  4 |  6 | 22 | 28 |*55*|*61*| 77 | 93 |  1 |  4 
----------------------------------------_________
9 islem yapildi.
Son D=[4 6 22 28 55 61 77 93]




