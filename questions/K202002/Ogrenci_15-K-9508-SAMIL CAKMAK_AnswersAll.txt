Sinav 5 soru ve toplam 100 puandan olusmakta olup, puan dagilimi su sekildedir:
[SoruNu-Puan]-->[0-10],[1-10],[2-10],[3-10],[4-60]

***Soru Nu[0]
Ogrenci[15-K-9508-SAMIL CAKMAK]
Puan: 10
--------->
Asagidaki sorularin cevaplarini yanlarina yaziniz. Hafta 04, Konu : Degiskenlerin tanimli oldugu araliklar.
  Eger integer bilgisayarda 9 bit ile ifade edilseydi
    a. -pow(2, 8)=-256
    b. pow(2, 8)-1=255
    c. 0
    d. pow(2, 9)=512
    e. [1 0 0 0 0 0 0 0 0]
    f. [0 1 1 1 1 1 1 1 1]
    g. [0 0 0 0 0 0 0 0 0]
    h. [1 1 1 1 1 1 1 1 1]



***Soru Nu[1]
Ogrenci[15-K-9508-SAMIL CAKMAK]
Puan: 10
--------->
Asagidaki programin hafizada kapladigi boyutu hesaplayiniz. Hafta 03, Konu : Veri yapilari ve hafiza.
      6 integers --> 6*2 bytes = 96 bits.
      8 chars --> 7 + 1 bytes = 64 bits.
      5 float --> 5*2 bytes = 80 bits.
      47 long --> 47*4 bytes = 1504 bits.
      TOPLAM --> 218 bytes = 1744 bits.



***Soru Nu[2]
Ogrenci[15-K-9508-SAMIL CAKMAK]
Puan: 10
--------->
 Konu : Genel Programlama Bilgisi

	BLOK_A - for öncesinde çalışır.
	BLOK_B - for bloğunun ilklendirici komutudur. Her zaman çalışır. Boş bırakılabilir.
	BLOK_C - for bloğunun sonlanacağı zamanı belirten koşul komutudur. Burasının ilk değeri "yanlış" ise döngü başlamaz. Boş bırakılması uygun değildir. Boş ise döngü içinde mutlaka break veya exit bulunmalıdır.
	BLOK_D - for bloğunun iterasyon komutudur. 
	BLOK_E - for bloğunun uygulama bloğudur. BLOK_E işlendikten sonra önce bu işlem gerçekleştirilir ardından BLOK_C'ye tekrar bakılır.
	BLOK_F - for sonrasında çalışır.
	
	sıralama - A,B,[C,E,D],F --> sırasıyla çalışır. D sonrası C'ye geri döner.



***Soru Nu[3]
Ogrenci[15-K-9508-SAMIL CAKMAK]
Puan: 10
--------->
- Hafta 04, Konu : turetilmis veri yapilari

	a. [45, ___, ___]
	b. Yığın - Stack
	c. 
		(1). push(69) - [45, 69, ___]
		(2). pop() [hangi sayı diziden çıkar = 69]
		(3). pop() [hangi sayı diziden çıkar = 45]
		(4). pop() [hangi sayı diziden çıkar = hiçbir sayı çıkmaz]
		(6). push(90) - [90, ___, ___]
		(7). push(31) - [90, 31, ___]
		(8). push(86) - [90, 31, 86]
	d. Queue
	e. 
		(1). dequeue() [hangi sayı diziden çıkar = 45]
		(2). dequeue() [hangi sayı diziden çıkar = hiçbir sayı çıkmaz]
		(3). enqueue(85) - [___, 85, ___]
		(4). enqueue(64) - [___, 85, 64]
		(5). enqueue(16) - [16, 85, 64]
		(6). dequeue() [hangi sayı diziden çıkar = 85]



***Soru Nu[4]
Ogrenci[15-K-9508-SAMIL CAKMAK]
Puan: 60
--------->
Soru seceneklerinde belirtilen fonksiyonlara bakarak her bir secenegi cozun. Konu : Siralama algoritmalari

Asagidaki seceneklerde belirtilen f1, f2, f3, f4 fonksiyonlara gore sorulari cozunuz.
	a. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[9 11 44 38 52 94 82] dizisini siralayiniz.

void f1_sort_insertion(int arr[], int n)  
{  
    int i, j, k;  
    for (i=1;i<n;i++) 
    {  
        k = arr[i];  
        for(j=i-1;j>=0 && arr[j]>k;j--) 
            arr[j+1]=arr[j];  
        arr[j+1]=k;
    }
}

Baslangic D=[9 11 44 38 52 94 82]
_____-----------------------------------______________
  i | 00 | 01 | 02 | 03 | 04 | 05 | 06 |  k |ekle|D(k)
_____-----------------------------------______________
 01 |  9 |*11*| 44 | 38 | 52 | 94 | 82 |  0 | 11 |  9 
_____-----------------------------------______________
 02 |  9 | 11 |*44*| 38 | 52 | 94 | 82 |  1 | 44 | 11 
_____-----------------------------------______________
 03 |  9 | 11 |*44*|*44*| 52 | 94 | 82 |  2 | 38 | 44 
_____-----------------------------------______________
 03 |  9 | 11 |*38*| 44 | 52 | 94 | 82 |  1 | 38 | 11 
_____-----------------------------------______________
 04 |  9 | 11 | 38 | 44 |*52*| 94 | 82 |  3 | 52 | 44 
_____-----------------------------------______________
 05 |  9 | 11 | 38 | 44 | 52 |*94*| 82 |  4 | 94 | 52 
_____-----------------------------------______________
 06 |  9 | 11 | 38 | 44 | 52 |*94*|*94*|  5 | 82 | 94 
_____-----------------------------------______________
 06 |  9 | 11 | 38 | 44 | 52 |*82*| 94 |  4 | 82 | 52 
_____-----------------------------------______________
8 islem yapildi.
Son D=[9 11 38 44 52 82 94]

	b. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[20 4 34 64 84 14 91 95 42 43] dizisini siralayiniz.

void f2_sort_selection(int arr[], int n)  
{  
    int i, j, k, m;
    for (i=0;i<n-1;i++) 
    {
        k=n-1;
        m=arr[k];
        for(j=i;j<n-1;j++)
            if(arr[j]<m)
            {
                k=j;
                m=arr[j];
            }
        arr[k]=arr[i];
        arr[i]=m;
    }
}

Baslangic D=[20 4 34 64 84 14 91 95 42 43]
_____--------------------------------------------------
  i | 00 | 01 | 02 | 03 | 04 | 05 | 06 | 07 | 08 | 09 |
_____--------------------------------------------------
 00 |* 4*|*20*| 34 | 64 | 84 | 14 | 91 | 95 | 42 | 43 |
_____--------------------------------------------------
 01 |  4 |*14*| 34 | 64 | 84 |*20*| 91 | 95 | 42 | 43 |
_____--------------------------------------------------
 02 |  4 | 14 |*20*| 64 | 84 |*34*| 91 | 95 | 42 | 43 |
_____--------------------------------------------------
 03 |  4 | 14 | 20 |*34*| 84 |*64*| 91 | 95 | 42 | 43 |
_____--------------------------------------------------
 04 |  4 | 14 | 20 | 34 |*42*| 64 | 91 | 95 |*84*| 43 |
_____--------------------------------------------------
 05 |  4 | 14 | 20 | 34 | 42 |*43*| 91 | 95 | 84 |*64*|
_____--------------------------------------------------
 06 |  4 | 14 | 20 | 34 | 42 | 43 |*64*| 95 | 84 |*91*|
_____--------------------------------------------------
 07 |  4 | 14 | 20 | 34 | 42 | 43 | 64 |*84*|*95*| 91 |
_____--------------------------------------------------
 08 |  4 | 14 | 20 | 34 | 42 | 43 | 64 | 84 |*91*|*95*|
_____--------------------------------------------------
9 islem yapildi.
Son D=[4 14 20 34 42 43 64 84 91 95]

	c. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[3 50 91 33 64 82 31 85] dizisini siralayiniz.

void f3_sort_bubble(int arr[], int n)  
{  
    int i, j;
    for (i=0;i<n-1;i++) 
        for(j=0;j<n-1-i;j++) 
            if(arr[j]>arr[j+1]) 
	        swap(arr,j,j+1);
}

Baslangic D=[3 50 91 33 64 82 31 85]
----------------------------------------_________
 00 | 01 | 02 | 03 | 04 | 05 | 06 | 07 | mov|  k 
----------------------------------------_________
  3 | 50 |*33*|*91*| 64 | 82 | 31 | 85 |  0 |  2 
----------------------------------------_________
  3 | 50 | 33 |*64*|*91*| 82 | 31 | 85 |  0 |  3 
----------------------------------------_________
  3 | 50 | 33 | 64 |*82*|*91*| 31 | 85 |  0 |  4 
----------------------------------------_________
  3 | 50 | 33 | 64 | 82 |*31*|*91*| 85 |  0 |  5 
----------------------------------------_________
  3 | 50 | 33 | 64 | 82 | 31 |*85*|*91*|  0 |  6 
----------------------------------------_________
  3 |*33*|*50*| 64 | 82 | 31 | 85 | 91 |  1 |  1 
----------------------------------------_________
  3 | 33 | 50 | 64 |*31*|*82*| 85 | 91 |  1 |  4 
----------------------------------------_________
  3 | 33 | 50 |*31*|*64*| 82 | 85 | 91 |  2 |  3 
----------------------------------------_________
  3 | 33 |*31*|*50*| 64 | 82 | 85 | 91 |  3 |  2 
----------------------------------------_________
  3 |*31*|*33*| 50 | 64 | 82 | 85 | 91 |  4 |  1 
----------------------------------------_________
10 islem yapildi.
Son D=[3 31 33 50 64 82 85 91]

	d. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[8 13 14 10 12 9 15 11] dizisini siralayiniz.

void f4_sort_merge(int arr[], int i, int n)  
{  
    if(n>1) 
    {
        j=n/2;
        ms(arr,i,j);
        ms(arr,j,n-j);
        m(D,i,j,n);
    }
}

8 9 10 11 12 13 14 15



