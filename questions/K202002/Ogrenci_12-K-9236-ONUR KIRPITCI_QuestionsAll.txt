Sinav 5 soru ve toplam 100 puandan olusmakta olup, puan dagilimi su sekildedir:
[SoruNu-Puan]-->[0-10],[1-10],[2-10],[3-10],[4-60]

***Soru Nu[0]
Ogrenci[12-K-9236-ONUR KIRPITCI]
Puan: 10
--------->
Asagidaki sorularin cevaplarini yanlarina yaziniz. Hafta 04, Konu : Degiskenlerin tanimli oldugu araliklar.

Eger integer bilgisayarda 7 bit ile ifade edilseydi
  a. signed integer'in alabilecegi minimum deger ne olurdu?
  b. signed integer'in alabilecegi maximum deger ne olurdu?
  c. unsigned integer'in alabilecegi minimum deger ne olurdu?
  d. unsigned integer'in alabilecegi maximum deger ne olurdu?
  e. signed integer'in alabilecegi minimum degeri 2 bitlik duzende yaziniz:
    _,_,_,_,_,_,_
  f. signed integer'in alabilecegi maximum degeri 2 bitlik duzende yaziniz:
    _,_,_,_,_,_,_
  g. unsigned integer'in alabilecegi minimum degeri 2 bitlik duzende yaziniz:
    _,_,_,_,_,_,_
  h. unsigned integer'in alabilecegi maximum degeri 2 bitlik duzende yaziniz:
    _,_,_,_,_,_,_



***Soru Nu[1]
Ogrenci[12-K-9236-ONUR KIRPITCI]
Puan: 10
--------->
Asagidaki programin hafizada kapladigi boyutu hesaplayiniz. Hafta 03, Konu : Veri yapilari ve hafiza.

Eger bir bilgisayarda veri tiplerinin boyutlari asagidaki gibiyse asagidaki fonksiyonlar hafizada kac bit yer kaplar?
integer-2 byte, long-4 byte, float 2 byte.

    void main()
    {
      int A[] = {177,294,91,284,91,41,388,466};
      char B[]="Kertenkele";
      float C[] = {39.006,137.77,106.96,138.61,182.03,29.736,4.9841,127.94,77.266,118.15};
      long long D[19];
    }



***Soru Nu[2]
Ogrenci[12-K-9236-ONUR KIRPITCI]
Puan: 10
--------->
 Konu : Genel Programlama Bilgisi

Aşağıda yer alan for döngüsünde belirtilen blokların hangi sırayla çalışacağını anlatınız. Bir örnek ile gösteriniz. 
	BLOK_A
	for (BLOK_B;BLOK_C;BLOK_D)
	{
		BLOK_E;
	}
	BLOK_F;



***Soru Nu[3]
Ogrenci[12-K-9236-ONUR KIRPITCI]
Puan: 10
--------->
- Hafta 04, Konu : turetilmis veri yapilari

Integer tipinde, adı Xarr olan 3 elemanlı bir dizi tanımlayın. 
	a. Dizinin birinci elemanını 68 olarak doldurun. Diziyi kutucuklarla gösterin.
	b. Bu diziye aşağıdaki push ve pop işlemleri uygulandığına göre bu dizi hangi türetilmiş veri yapısına sahiptir?
	c. Bu diziye sırayla aşağıdaki işlemleri uygulayın ve her işlemden sonra dizinin kutularının şeklini çizin.
		(1). push(91)
		(2). pop() [hangi sayı diziden çıkar = ___]
		(3). pop() [hangi sayı diziden çıkar = ___]
		(4). pop() [hangi sayı diziden çıkar = ___]
		(6). push(63)
		(7). push(88)
		(8). push(12)
	d. Bu diziye aşağıdaki enqueue ve dequeue işlemleri uygulansaydı bu dizi hangi türetilmiş veri yapısına sahip olurdu?
	e. Bu diziye "a." maddesindeki tek elemanlı halinden başlayarak sırayla aşağıdaki işlemleri uygulayın ve her işlemden sonra dizinin kutularının şeklini çizin.
		(1). dequeue() [hangi sayı diziden çıkar = ___]
		(2). dequeue() [hangi sayı diziden çıkar = ___]
		(3). enqueue(97)
		(4). enqueue(84)
		(5). enqueue(51)
		(6). dequeue() [hangi sayı diziden çıkar = ___]
		



***Soru Nu[4]
Ogrenci[12-K-9236-ONUR KIRPITCI]
Puan: 60
--------->
Soru seceneklerinde belirtilen fonksiyonlara bakarak her bir secenegi cozun. Konu : Siralama algoritmalari

Asagidaki seceneklerde belirtilen f1, f2, f3, f4 fonksiyonlara gore sorulari cozunuz.
	a. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[7 4 6 3 5 8 2 9] dizisini siralayiniz.

void f1(int arr[], int i, int n)  
{  
    if(n>1) 
    {
        j=n/2;
        ms(arr,i,j);
        ms(arr,j,n-j);
        m(D,i,j,n);
    }
}

	b. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[18 31 58 69 32 27 20 26 74 17] dizisini siralayiniz.

void f2(int arr[], int n)  
{  
    int i, j, k, m;
    for (i=0;i<n-1;i++) 
    {
        k=n-1;
        m=arr[k];
        for(j=i;j<n-1;j++)
            if(arr[j]<m)
            {
                k=j;
                m=arr[j];
            }
        arr[k]=arr[i];
        arr[i]=m;
    }
}

	c. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[10 23 27 9 66 88 83] dizisini siralayiniz.

void f3(int arr[], int n)  
{  
    int i, j, k;  
    for (i=1;i<n;i++) 
    {  
        k = arr[i];  
        for(j=i-1;j>=0 && arr[j]>k;j--) 
            arr[j+1]=arr[j];  
        arr[j+1]=k;
    }
}

	d. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[61 22 4 6 28 93 55 77] dizisini siralayiniz.

void f4(int arr[], int n)  
{  
    int i, j;
    for (i=0;i<n-1;i++) 
        for(j=0;j<n-1-i;j++) 
            if(arr[j]>arr[j+1]) 
	        swap(arr,j,j+1);
}




