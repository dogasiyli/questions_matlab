Sinav 5 soru ve toplam 100 puandan olusmakta olup, puan dagilimi su sekildedir:
[SoruNu-Puan]-->[0-10],[1-10],[2-10],[3-10],[4-60]

***Soru Nu[0]
Ogrenci[19-K-9710-TUGRUL AYYILDIZ]
Puan: 10
--------->
Asagidaki sorularin cevaplarini yanlarina yaziniz. Hafta 04, Konu : Degiskenlerin tanimli oldugu araliklar.
  Eger integer bilgisayarda 6 bit ile ifade edilseydi
    a. -pow(2, 5)=-32
    b. pow(2, 5)-1=31
    c. 0
    d. pow(2, 6)=64
    e. [1 0 0 0 0 0]
    f. [0 1 1 1 1 1]
    g. [0 0 0 0 0 0]
    h. [1 1 1 1 1 1]



***Soru Nu[1]
Ogrenci[19-K-9710-TUGRUL AYYILDIZ]
Puan: 10
--------->
Asagidaki programin hafizada kapladigi boyutu hesaplayiniz. Hafta 03, Konu : Veri yapilari ve hafiza.
      9 integers --> 9*2 bytes = 144 bits.
      6 chars --> 5 + 1 bytes = 48 bits.
      6 float --> 6*2 bytes = 96 bits.
      12 long --> 12*4 bytes = 384 bits.
      TOPLAM --> 84 bytes = 672 bits.



***Soru Nu[2]
Ogrenci[19-K-9710-TUGRUL AYYILDIZ]
Puan: 10
--------->
 Konu : Genel Programlama Bilgisi

	BLOK_A - for öncesinde çalışır.
	BLOK_B - for bloğunun ilklendirici komutudur. Her zaman çalışır. Boş bırakılabilir.
	BLOK_C - for bloğunun sonlanacağı zamanı belirten koşul komutudur. Burasının ilk değeri "yanlış" ise döngü başlamaz. Boş bırakılması uygun değildir. Boş ise döngü içinde mutlaka break veya exit bulunmalıdır.
	BLOK_D - for bloğunun iterasyon komutudur. 
	BLOK_E - for bloğunun uygulama bloğudur. BLOK_E işlendikten sonra önce bu işlem gerçekleştirilir ardından BLOK_C'ye tekrar bakılır.
	BLOK_F - for sonrasında çalışır.
	
	sıralama - A,B,[C,E,D],F --> sırasıyla çalışır. D sonrası C'ye geri döner.



***Soru Nu[3]
Ogrenci[19-K-9710-TUGRUL AYYILDIZ]
Puan: 10
--------->
- Hafta 04, Konu : turetilmis veri yapilari

	a. [44, ___, ___]
	b. Yığın - Stack
	c. 
		(1). push(81) - [44, 81, ___]
		(2). pop() [hangi sayı diziden çıkar = 81]
		(3). pop() [hangi sayı diziden çıkar = 44]
		(4). pop() [hangi sayı diziden çıkar = hiçbir sayı çıkmaz]
		(6). push(12) - [12, ___, ___]
		(7). push(43) - [12, 43, ___]
		(8). push(60) - [12, 43, 60]
	d. Queue
	e. 
		(1). dequeue() [hangi sayı diziden çıkar = 44]
		(2). dequeue() [hangi sayı diziden çıkar = hiçbir sayı çıkmaz]
		(3). enqueue(38) - [___, 38, ___]
		(4). enqueue(16) - [___, 38, 16]
		(5). enqueue(96) - [96, 38, 16]
		(6). dequeue() [hangi sayı diziden çıkar = 38]



***Soru Nu[4]
Ogrenci[19-K-9710-TUGRUL AYYILDIZ]
Puan: 60
--------->
Soru seceneklerinde belirtilen fonksiyonlara bakarak her bir secenegi cozun. Konu : Siralama algoritmalari

Asagidaki seceneklerde belirtilen f1, f2, f3, f4 fonksiyonlara gore sorulari cozunuz.
	a. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[8 5 4 7 6 2 3 9] dizisini siralayiniz.

void f1_sort_merge(int arr[], int i, int n)  
{  
    if(n>1) 
    {
        j=n/2;
        ms(arr,i,j);
        ms(arr,j,n-j);
        m(D,i,j,n);
    }
}

2 3 4 5 6 7 8 9
	b. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[60 2 9 79 31 62 35 78 41 14] dizisini siralayiniz.

void f2_sort_selection(int arr[], int n)  
{  
    int i, j, k, m;
    for (i=0;i<n-1;i++) 
    {
        k=n-1;
        m=arr[k];
        for(j=i;j<n-1;j++)
            if(arr[j]<m)
            {
                k=j;
                m=arr[j];
            }
        arr[k]=arr[i];
        arr[i]=m;
    }
}

Baslangic D=[60 2 9 79 31 62 35 78 41 14]
_____--------------------------------------------------
  i | 00 | 01 | 02 | 03 | 04 | 05 | 06 | 07 | 08 | 09 |
_____--------------------------------------------------
 00 |* 2*|*60*|  9 | 79 | 31 | 62 | 35 | 78 | 41 | 14 |
_____--------------------------------------------------
 01 |  2 |* 9*|*60*| 79 | 31 | 62 | 35 | 78 | 41 | 14 |
_____--------------------------------------------------
 02 |  2 |  9 |*14*| 79 | 31 | 62 | 35 | 78 | 41 |*60*|
_____--------------------------------------------------
 03 |  2 |  9 | 14 |*31*|*79*| 62 | 35 | 78 | 41 | 60 |
_____--------------------------------------------------
 04 |  2 |  9 | 14 | 31 |*35*| 62 |*79*| 78 | 41 | 60 |
_____--------------------------------------------------
 05 |  2 |  9 | 14 | 31 | 35 |*41*| 79 | 78 |*62*| 60 |
_____--------------------------------------------------
 06 |  2 |  9 | 14 | 31 | 35 | 41 |*60*| 78 | 62 |*79*|
_____--------------------------------------------------
 07 |  2 |  9 | 14 | 31 | 35 | 41 | 60 |*62*|*78*| 79 |
_____--------------------------------------------------
 08 |  2 |  9 | 14 | 31 | 35 | 41 | 60 | 62 |*78*| 79 |
_____--------------------------------------------------
9 islem yapildi.
Son D=[2 9 14 31 35 41 60 62 78 79]

	c. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[5 9 16 72 30 97 68] dizisini siralayiniz.

void f3_sort_insertion(int arr[], int n)  
{  
    int i, j, k;  
    for (i=1;i<n;i++) 
    {  
        k = arr[i];  
        for(j=i-1;j>=0 && arr[j]>k;j--) 
            arr[j+1]=arr[j];  
        arr[j+1]=k;
    }
}

Baslangic D=[5 9 16 72 30 97 68]
_____-----------------------------------______________
  i | 00 | 01 | 02 | 03 | 04 | 05 | 06 |  k |ekle|D(k)
_____-----------------------------------______________
 01 |  5 |* 9*| 16 | 72 | 30 | 97 | 68 |  0 |  9 |  5 
_____-----------------------------------______________
 02 |  5 |  9 |*16*| 72 | 30 | 97 | 68 |  1 | 16 |  9 
_____-----------------------------------______________
 03 |  5 |  9 | 16 |*72*| 30 | 97 | 68 |  2 | 72 | 16 
_____-----------------------------------______________
 04 |  5 |  9 | 16 |*72*|*72*| 97 | 68 |  3 | 30 | 72 
_____-----------------------------------______________
 04 |  5 |  9 | 16 |*30*| 72 | 97 | 68 |  2 | 30 | 16 
_____-----------------------------------______________
 05 |  5 |  9 | 16 | 30 | 72 |*97*| 68 |  4 | 97 | 72 
_____-----------------------------------______________
 06 |  5 |  9 | 16 | 30 | 72 |*97*|*97*|  5 | 68 | 97 
_____-----------------------------------______________
 06 |  5 |  9 | 16 | 30 |*72*|*72*| 97 |  4 | 68 | 72 
_____-----------------------------------______________
 06 |  5 |  9 | 16 | 30 |*68*| 72 | 97 |  3 | 68 | 30 
_____-----------------------------------______________
9 islem yapildi.
Son D=[5 9 16 30 68 72 97]

	d. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[5 65 53 27 39 52 55] dizisini siralayiniz.

void f4_sort_bubble(int arr[], int n)  
{  
    int i, j;
    for (i=0;i<n-1;i++) 
        for(j=0;j<n-1-i;j++) 
            if(arr[j]>arr[j+1]) 
	        swap(arr,j,j+1);
}

Baslangic D=[5 65 53 27 39 52 55]
-----------------------------------_________
 00 | 01 | 02 | 03 | 04 | 05 | 06 | mov|  k 
-----------------------------------_________
  5 |*53*|*65*| 27 | 39 | 52 | 55 |  0 |  1 
-----------------------------------_________
  5 | 53 |*27*|*65*| 39 | 52 | 55 |  0 |  2 
-----------------------------------_________
  5 | 53 | 27 |*39*|*65*| 52 | 55 |  0 |  3 
-----------------------------------_________
  5 | 53 | 27 | 39 |*52*|*65*| 55 |  0 |  4 
-----------------------------------_________
  5 | 53 | 27 | 39 | 52 |*55*|*65*|  0 |  5 
-----------------------------------_________
  5 |*27*|*53*| 39 | 52 | 55 | 65 |  1 |  1 
-----------------------------------_________
  5 | 27 |*39*|*53*| 52 | 55 | 65 |  1 |  2 
-----------------------------------_________
  5 | 27 | 39 |*52*|*53*| 55 | 65 |  1 |  3 
-----------------------------------_________
8 islem yapildi.
Son D=[5 27 39 52 53 55 65]




