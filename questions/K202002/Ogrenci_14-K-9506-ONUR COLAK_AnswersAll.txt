Sinav 5 soru ve toplam 100 puandan olusmakta olup, puan dagilimi su sekildedir:
[SoruNu-Puan]-->[0-10],[1-10],[2-10],[3-10],[4-60]

***Soru Nu[0]
Ogrenci[14-K-9506-ONUR COLAK]
Puan: 10
--------->
Asagidaki sorularin cevaplarini yanlarina yaziniz. Hafta 04, Konu : Degiskenlerin tanimli oldugu araliklar.
  Eger integer bilgisayarda 5 bit ile ifade edilseydi
    a. -pow(2, 4)=-16
    b. pow(2, 4)-1=15
    c. 0
    d. pow(2, 5)=32
    e. [1 0 0 0 0]
    f. [0 1 1 1 1]
    g. [0 0 0 0 0]
    h. [1 1 1 1 1]



***Soru Nu[1]
Ogrenci[14-K-9506-ONUR COLAK]
Puan: 10
--------->
Asagidaki programin hafizada kapladigi boyutu hesaplayiniz. Hafta 03, Konu : Veri yapilari ve hafiza.
      10 integers --> 10*2 bytes = 160 bits.
      11 chars --> 10 + 1 bytes = 88 bits.
      15 float --> 15*2 bytes = 240 bits.
      100 long --> 100*4 bytes = 3200 bits.
      TOPLAM --> 461 bytes = 3688 bits.



***Soru Nu[2]
Ogrenci[14-K-9506-ONUR COLAK]
Puan: 10
--------->
 Konu : Genel Programlama Bilgisi

	BLOK_A - for öncesinde çalışır.
	BLOK_B - for bloğunun ilklendirici komutudur. Her zaman çalışır. Boş bırakılabilir.
	BLOK_C - for bloğunun sonlanacağı zamanı belirten koşul komutudur. Burasının ilk değeri "yanlış" ise döngü başlamaz. Boş bırakılması uygun değildir. Boş ise döngü içinde mutlaka break veya exit bulunmalıdır.
	BLOK_D - for bloğunun iterasyon komutudur. 
	BLOK_E - for bloğunun uygulama bloğudur. BLOK_E işlendikten sonra önce bu işlem gerçekleştirilir ardından BLOK_C'ye tekrar bakılır.
	BLOK_F - for sonrasında çalışır.
	
	sıralama - A,B,[C,E,D],F --> sırasıyla çalışır. D sonrası C'ye geri döner.



***Soru Nu[3]
Ogrenci[14-K-9506-ONUR COLAK]
Puan: 10
--------->
- Hafta 04, Konu : turetilmis veri yapilari

	a. [50, ___, ___]
	b. Yığın - Stack
	c. 
		(1). push(35) - [50, 35, ___]
		(2). pop() [hangi sayı diziden çıkar = 35]
		(3). pop() [hangi sayı diziden çıkar = 50]
		(4). pop() [hangi sayı diziden çıkar = hiçbir sayı çıkmaz]
		(6). push(87) - [87, ___, ___]
		(7). push(82) - [87, 82, ___]
		(8). push(84) - [87, 82, 84]
	d. Queue
	e. 
		(1). dequeue() [hangi sayı diziden çıkar = 50]
		(2). dequeue() [hangi sayı diziden çıkar = hiçbir sayı çıkmaz]
		(3). enqueue(92) - [___, 92, ___]
		(4). enqueue(22) - [___, 92, 22]
		(5). enqueue(69) - [69, 92, 22]
		(6). dequeue() [hangi sayı diziden çıkar = 92]



***Soru Nu[4]
Ogrenci[14-K-9506-ONUR COLAK]
Puan: 60
--------->
Soru seceneklerinde belirtilen fonksiyonlara bakarak her bir secenegi cozun. Konu : Siralama algoritmalari

Asagidaki seceneklerde belirtilen f1, f2, f3, f4 fonksiyonlara gore sorulari cozunuz.
	a. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[13 15 16 37 35 34 91] dizisini siralayiniz.

void f1_sort_insertion(int arr[], int n)  
{  
    int i, j, k;  
    for (i=1;i<n;i++) 
    {  
        k = arr[i];  
        for(j=i-1;j>=0 && arr[j]>k;j--) 
            arr[j+1]=arr[j];  
        arr[j+1]=k;
    }
}

Baslangic D=[13 15 16 37 35 34 91]
_____-----------------------------------______________
  i | 00 | 01 | 02 | 03 | 04 | 05 | 06 |  k |ekle|D(k)
_____-----------------------------------______________
 01 | 13 |*15*| 16 | 37 | 35 | 34 | 91 |  0 | 15 | 13 
_____-----------------------------------______________
 02 | 13 | 15 |*16*| 37 | 35 | 34 | 91 |  1 | 16 | 15 
_____-----------------------------------______________
 03 | 13 | 15 | 16 |*37*| 35 | 34 | 91 |  2 | 37 | 16 
_____-----------------------------------______________
 04 | 13 | 15 | 16 |*37*|*37*| 34 | 91 |  3 | 35 | 37 
_____-----------------------------------______________
 04 | 13 | 15 | 16 |*35*| 37 | 34 | 91 |  2 | 35 | 16 
_____-----------------------------------______________
 05 | 13 | 15 | 16 | 35 |*37*|*37*| 91 |  4 | 34 | 37 
_____-----------------------------------______________
 05 | 13 | 15 | 16 |*35*|*35*| 37 | 91 |  3 | 34 | 35 
_____-----------------------------------______________
 05 | 13 | 15 | 16 |*34*| 35 | 37 | 91 |  2 | 34 | 16 
_____-----------------------------------______________
 06 | 13 | 15 | 16 | 34 | 35 | 37 |*91*|  5 | 91 | 37 
_____-----------------------------------______________
9 islem yapildi.
Son D=[13 15 16 34 35 37 91]

	b. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[6 74 50 56 37 15 20 47 55 80] dizisini siralayiniz.

void f2_sort_selection(int arr[], int n)  
{  
    int i, j, k, m;
    for (i=0;i<n-1;i++) 
    {
        k=n-1;
        m=arr[k];
        for(j=i;j<n-1;j++)
            if(arr[j]<m)
            {
                k=j;
                m=arr[j];
            }
        arr[k]=arr[i];
        arr[i]=m;
    }
}

Baslangic D=[6 74 50 56 37 15 20 47 55 80]
_____--------------------------------------------------
  i | 00 | 01 | 02 | 03 | 04 | 05 | 06 | 07 | 08 | 09 |
_____--------------------------------------------------
 00 |* 6*| 74 | 50 | 56 | 37 | 15 | 20 | 47 | 55 | 80 |
_____--------------------------------------------------
 01 |  6 |*15*| 50 | 56 | 37 |*74*| 20 | 47 | 55 | 80 |
_____--------------------------------------------------
 02 |  6 | 15 |*20*| 56 | 37 | 74 |*50*| 47 | 55 | 80 |
_____--------------------------------------------------
 03 |  6 | 15 | 20 |*37*|*56*| 74 | 50 | 47 | 55 | 80 |
_____--------------------------------------------------
 04 |  6 | 15 | 20 | 37 |*47*| 74 | 50 |*56*| 55 | 80 |
_____--------------------------------------------------
 05 |  6 | 15 | 20 | 37 | 47 |*50*|*74*| 56 | 55 | 80 |
_____--------------------------------------------------
 06 |  6 | 15 | 20 | 37 | 47 | 50 |*55*| 56 |*74*| 80 |
_____--------------------------------------------------
 07 |  6 | 15 | 20 | 37 | 47 | 50 | 55 |*56*| 74 | 80 |
_____--------------------------------------------------
 08 |  6 | 15 | 20 | 37 | 47 | 50 | 55 | 56 |*74*| 80 |
_____--------------------------------------------------
9 islem yapildi.
Son D=[6 15 20 37 47 50 55 56 74 80]

	c. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[70 3 22 33 12 56 78 47 98] dizisini siralayiniz.

void f3_sort_bubble(int arr[], int n)  
{  
    int i, j;
    for (i=0;i<n-1;i++) 
        for(j=0;j<n-1-i;j++) 
            if(arr[j]>arr[j+1]) 
	        swap(arr,j,j+1);
}

Baslangic D=[70 3 22 33 12 56 78 47 98]
---------------------------------------------_________
 00 | 01 | 02 | 03 | 04 | 05 | 06 | 07 | 08 | mov|  k 
---------------------------------------------_________
* 3*|*70*| 22 | 33 | 12 | 56 | 78 | 47 | 98 |  0 |  0 
---------------------------------------------_________
  3 |*22*|*70*| 33 | 12 | 56 | 78 | 47 | 98 |  0 |  1 
---------------------------------------------_________
  3 | 22 |*33*|*70*| 12 | 56 | 78 | 47 | 98 |  0 |  2 
---------------------------------------------_________
  3 | 22 | 33 |*12*|*70*| 56 | 78 | 47 | 98 |  0 |  3 
---------------------------------------------_________
  3 | 22 | 33 | 12 |*56*|*70*| 78 | 47 | 98 |  0 |  4 
---------------------------------------------_________
  3 | 22 | 33 | 12 | 56 | 70 |*47*|*78*| 98 |  0 |  6 
---------------------------------------------_________
  3 | 22 |*12*|*33*| 56 | 70 | 47 | 78 | 98 |  1 |  2 
---------------------------------------------_________
  3 | 22 | 12 | 33 | 56 |*47*|*70*| 78 | 98 |  1 |  5 
---------------------------------------------_________
  3 |*12*|*22*| 33 | 56 | 47 | 70 | 78 | 98 |  2 |  1 
---------------------------------------------_________
  3 | 12 | 22 | 33 |*47*|*56*| 70 | 78 | 98 |  2 |  4 
---------------------------------------------_________
10 islem yapildi.
Son D=[3 12 22 33 47 56 70 78 98]

	d. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[15 13 18 12 16 17 14 11] dizisini siralayiniz.

void f4_sort_merge(int arr[], int i, int n)  
{  
    if(n>1) 
    {
        j=n/2;
        ms(arr,i,j);
        ms(arr,j,n-j);
        m(D,i,j,n);
    }
}

11 12 13 14 15 16 17 18



