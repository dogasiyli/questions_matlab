Sinav 5 soru ve toplam 100 puandan olusmakta olup, puan dagilimi su sekildedir:
[SoruNu-Puan]-->[0-10],[1-10],[2-10],[3-10],[4-60]

***Soru Nu[0]
Ogrenci[3-K-9008-MERT KAAN GOKTAS]
Puan: 10
--------->
Asagidaki sorularin cevaplarini yanlarina yaziniz. Hafta 04, Konu : Degiskenlerin tanimli oldugu araliklar.

Eger integer bilgisayarda 9 bit ile ifade edilseydi
  a. signed integer'in alabilecegi minimum deger ne olurdu?
  b. signed integer'in alabilecegi maximum deger ne olurdu?
  c. unsigned integer'in alabilecegi minimum deger ne olurdu?
  d. unsigned integer'in alabilecegi maximum deger ne olurdu?
  e. signed integer'in alabilecegi minimum degeri 2 bitlik duzende yaziniz:
    _,_,_,_,_,_,_,_,_
  f. signed integer'in alabilecegi maximum degeri 2 bitlik duzende yaziniz:
    _,_,_,_,_,_,_,_,_
  g. unsigned integer'in alabilecegi minimum degeri 2 bitlik duzende yaziniz:
    _,_,_,_,_,_,_,_,_
  h. unsigned integer'in alabilecegi maximum degeri 2 bitlik duzende yaziniz:
    _,_,_,_,_,_,_,_,_



***Soru Nu[1]
Ogrenci[3-K-9008-MERT KAAN GOKTAS]
Puan: 10
--------->
Asagidaki programin hafizada kapladigi boyutu hesaplayiniz. Hafta 03, Konu : Veri yapilari ve hafiza.

Eger bir bilgisayarda veri tiplerinin boyutlari asagidaki gibiyse asagidaki fonksiyonlar hafizada kac bit yer kaplar?
integer-2 byte, long-4 byte, float 2 byte.

    void main()
    {
      int A[] = {461,148,470,458,486,426,273,51,233};
      char B[]="Sekonder";
      float C[] = {37.747,36.89,117.57,6.6827,137.34,52.101,48.713,186.43,164.57,76.117,131.22,148.79,118.2,63.369};
      long long D[36];
    }



***Soru Nu[2]
Ogrenci[3-K-9008-MERT KAAN GOKTAS]
Puan: 10
--------->
 Konu : Genel Programlama Bilgisi

Aşağıda yer alan for döngüsünde belirtilen blokların hangi sırayla çalışacağını anlatınız. Bir örnek ile gösteriniz. 
	BLOK_A
	for (BLOK_B;BLOK_C;BLOK_D)
	{
		BLOK_E;
	}
	BLOK_F;



***Soru Nu[3]
Ogrenci[3-K-9008-MERT KAAN GOKTAS]
Puan: 10
--------->
- Hafta 04, Konu : turetilmis veri yapilari

Integer tipinde, adı Xarr olan 3 elemanlı bir dizi tanımlayın. 
	a. Dizinin birinci elemanını 81 olarak doldurun. Diziyi kutucuklarla gösterin.
	b. Bu diziye aşağıdaki push ve pop işlemleri uygulandığına göre bu dizi hangi türetilmiş veri yapısına sahiptir?
	c. Bu diziye sırayla aşağıdaki işlemleri uygulayın ve her işlemden sonra dizinin kutularının şeklini çizin.
		(1). push(21)
		(2). pop() [hangi sayı diziden çıkar = ___]
		(3). pop() [hangi sayı diziden çıkar = ___]
		(4). pop() [hangi sayı diziden çıkar = ___]
		(6). push(53)
		(7). push(64)
		(8). push(65)
	d. Bu diziye aşağıdaki enqueue ve dequeue işlemleri uygulansaydı bu dizi hangi türetilmiş veri yapısına sahip olurdu?
	e. Bu diziye "a." maddesindeki tek elemanlı halinden başlayarak sırayla aşağıdaki işlemleri uygulayın ve her işlemden sonra dizinin kutularının şeklini çizin.
		(1). dequeue() [hangi sayı diziden çıkar = ___]
		(2). dequeue() [hangi sayı diziden çıkar = ___]
		(3). enqueue(39)
		(4). enqueue(28)
		(5). enqueue(80)
		(6). dequeue() [hangi sayı diziden çıkar = ___]
		



***Soru Nu[4]
Ogrenci[3-K-9008-MERT KAAN GOKTAS]
Puan: 60
--------->
Soru seceneklerinde belirtilen fonksiyonlara bakarak her bir secenegi cozun. Konu : Siralama algoritmalari

Asagidaki seceneklerde belirtilen f1, f2, f3, f4 fonksiyonlara gore sorulari cozunuz.
	a. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[24 11 56 36 53 97 6 96 71 93] dizisini siralayiniz.

void f1(int arr[], int n)  
{  
    int i, j, k, m;
    for (i=0;i<n-1;i++) 
    {
        k=n-1;
        m=arr[k];
        for(j=i;j<n-1;j++)
            if(arr[j]<m)
            {
                k=j;
                m=arr[j];
            }
        arr[k]=arr[i];
        arr[i]=m;
    }
}

	b. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[8 13 10 15 14 12 11 9] dizisini siralayiniz.

void f2(int arr[], int i, int n)  
{  
    if(n>1) 
    {
        j=n/2;
        ms(arr,i,j);
        ms(arr,j,n-j);
        m(D,i,j,n);
    }
}

	c. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[80 6 38 72 61 65 64] dizisini siralayiniz.

void f3(int arr[], int n)  
{  
    int i, j;
    for (i=0;i<n-1;i++) 
        for(j=0;j<n-1-i;j++) 
            if(arr[j]>arr[j+1]) 
	        swap(arr,j,j+1);
}

	d. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[53 62 64 7 70 98 82] dizisini siralayiniz.

void f4(int arr[], int n)  
{  
    int i, j, k;  
    for (i=1;i<n;i++) 
    {  
        k = arr[i];  
        for(j=i-1;j>=0 && arr[j]>k;j--) 
            arr[j+1]=arr[j];  
        arr[j+1]=k;
    }
}




