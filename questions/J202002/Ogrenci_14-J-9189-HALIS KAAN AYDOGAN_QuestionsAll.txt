Sinav 5 soru ve toplam 100 puandan olusmakta olup, puan dagilimi su sekildedir:
[SoruNu-Puan]-->[0-10],[1-10],[2-10],[3-10],[4-60]

***Soru Nu[0]
Ogrenci[14-J-9189-HALIS KAAN AYDOGAN]
Puan: 10
--------->
Asagidaki sorularin cevaplarini yanlarina yaziniz. Hafta 04, Konu : Degiskenlerin tanimli oldugu araliklar.

Eger integer bilgisayarda 5 bit ile ifade edilseydi
  a. signed integer'in alabilecegi minimum deger ne olurdu?
  b. signed integer'in alabilecegi maximum deger ne olurdu?
  c. unsigned integer'in alabilecegi minimum deger ne olurdu?
  d. unsigned integer'in alabilecegi maximum deger ne olurdu?
  e. signed integer'in alabilecegi minimum degeri 2 bitlik duzende yaziniz:
    _,_,_,_,_
  f. signed integer'in alabilecegi maximum degeri 2 bitlik duzende yaziniz:
    _,_,_,_,_
  g. unsigned integer'in alabilecegi minimum degeri 2 bitlik duzende yaziniz:
    _,_,_,_,_
  h. unsigned integer'in alabilecegi maximum degeri 2 bitlik duzende yaziniz:
    _,_,_,_,_



***Soru Nu[1]
Ogrenci[14-J-9189-HALIS KAAN AYDOGAN]
Puan: 10
--------->
Asagidaki programin hafizada kapladigi boyutu hesaplayiniz. Hafta 03, Konu : Veri yapilari ve hafiza.

Eger bir bilgisayarda veri tiplerinin boyutlari asagidaki gibiyse asagidaki fonksiyonlar hafizada kac bit yer kaplar?
integer-2 byte, long-4 byte, float 2 byte.

    void main()
    {
      int A[] = {44,66,190,388,54,35,173,325};
      char B[]="Bilgisayar";
      float C[] = {65.868,143.69,90.442,183.33,62.836,81.268,141.53};
      long long D[89];
    }



***Soru Nu[2]
Ogrenci[14-J-9189-HALIS KAAN AYDOGAN]
Puan: 10
--------->
 Konu : Genel Programlama Bilgisi

Aşağıda yer alan for döngüsünde belirtilen blokların hangi sırayla çalışacağını anlatınız. Bir örnek ile gösteriniz. 
	BLOK_A
	for (BLOK_B;BLOK_C;BLOK_D)
	{
		BLOK_E;
	}
	BLOK_F;



***Soru Nu[3]
Ogrenci[14-J-9189-HALIS KAAN AYDOGAN]
Puan: 10
--------->
- Hafta 04, Konu : turetilmis veri yapilari

Integer tipinde, adı Xarr olan 3 elemanlı bir dizi tanımlayın. 
	a. Dizinin birinci elemanını 90 olarak doldurun. Diziyi kutucuklarla gösterin.
	b. Bu diziye aşağıdaki push ve pop işlemleri uygulandığına göre bu dizi hangi türetilmiş veri yapısına sahiptir?
	c. Bu diziye sırayla aşağıdaki işlemleri uygulayın ve her işlemden sonra dizinin kutularının şeklini çizin.
		(1). push(74)
		(2). pop() [hangi sayı diziden çıkar = ___]
		(3). pop() [hangi sayı diziden çıkar = ___]
		(4). pop() [hangi sayı diziden çıkar = ___]
		(6). push(26)
		(7). push(81)
		(8). push(16)
	d. Bu diziye aşağıdaki enqueue ve dequeue işlemleri uygulansaydı bu dizi hangi türetilmiş veri yapısına sahip olurdu?
	e. Bu diziye "a." maddesindeki tek elemanlı halinden başlayarak sırayla aşağıdaki işlemleri uygulayın ve her işlemden sonra dizinin kutularının şeklini çizin.
		(1). dequeue() [hangi sayı diziden çıkar = ___]
		(2). dequeue() [hangi sayı diziden çıkar = ___]
		(3). enqueue(73)
		(4). enqueue(36)
		(5). enqueue(51)
		(6). dequeue() [hangi sayı diziden çıkar = ___]
		



***Soru Nu[4]
Ogrenci[14-J-9189-HALIS KAAN AYDOGAN]
Puan: 60
--------->
Soru seceneklerinde belirtilen fonksiyonlara bakarak her bir secenegi cozun. Konu : Siralama algoritmalari

Asagidaki seceneklerde belirtilen f1, f2, f3, f4 fonksiyonlara gore sorulari cozunuz.
	a. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[9 12 8 7 11 5 6 10] dizisini siralayiniz.

void f1(int arr[], int i, int n)  
{  
    if(n>1) 
    {
        j=n/2;
        ms(arr,i,j);
        ms(arr,j,n-j);
        m(D,i,j,n);
    }
}

	b. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[17 68 56 61 78 74 75] dizisini siralayiniz.

void f2(int arr[], int n)  
{  
    int i, j, k;  
    for (i=1;i<n;i++) 
    {  
        k = arr[i];  
        for(j=i-1;j>=0 && arr[j]>k;j--) 
            arr[j+1]=arr[j];  
        arr[j+1]=k;
    }
}

	c. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[9 51 34 21 39 98 23 70 41 76] dizisini siralayiniz.

void f3(int arr[], int n)  
{  
    int i, j, k, m;
    for (i=0;i<n-1;i++) 
    {
        k=n-1;
        m=arr[k];
        for(j=i;j<n-1;j++)
            if(arr[j]<m)
            {
                k=j;
                m=arr[j];
            }
        arr[k]=arr[i];
        arr[i]=m;
    }
}

	d. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[94 32 45 70 27 54] dizisini siralayiniz.

void f4(int arr[], int n)  
{  
    int i, j;
    for (i=0;i<n-1;i++) 
        for(j=0;j<n-1-i;j++) 
            if(arr[j]>arr[j+1]) 
	        swap(arr,j,j+1);
}




