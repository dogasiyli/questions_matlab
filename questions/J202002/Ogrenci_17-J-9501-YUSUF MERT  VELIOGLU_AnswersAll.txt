Sinav 5 soru ve toplam 100 puandan olusmakta olup, puan dagilimi su sekildedir:
[SoruNu-Puan]-->[0-10],[1-10],[2-10],[3-10],[4-60]

***Soru Nu[0]
Ogrenci[17-J-9501-YUSUF MERT  VELIOGLU]
Puan: 10
--------->
Asagidaki sorularin cevaplarini yanlarina yaziniz. Hafta 04, Konu : Degiskenlerin tanimli oldugu araliklar.
  Eger integer bilgisayarda 7 bit ile ifade edilseydi
    a. -pow(2, 6)=-64
    b. pow(2, 6)-1=63
    c. 0
    d. pow(2, 7)=128
    e. [1 0 0 0 0 0 0]
    f. [0 1 1 1 1 1 1]
    g. [0 0 0 0 0 0 0]
    h. [1 1 1 1 1 1 1]



***Soru Nu[1]
Ogrenci[17-J-9501-YUSUF MERT  VELIOGLU]
Puan: 10
--------->
Asagidaki programin hafizada kapladigi boyutu hesaplayiniz. Hafta 03, Konu : Veri yapilari ve hafiza.
      7 integers --> 7*2 bytes = 112 bits.
      6 chars --> 5 + 1 bytes = 48 bits.
      15 float --> 15*2 bytes = 240 bits.
      96 long --> 96*4 bytes = 3072 bits.
      TOPLAM --> 434 bytes = 3472 bits.



***Soru Nu[2]
Ogrenci[17-J-9501-YUSUF MERT  VELIOGLU]
Puan: 10
--------->
 Konu : Genel Programlama Bilgisi

	BLOK_A - for öncesinde çalışır.
	BLOK_B - for bloğunun ilklendirici komutudur. Her zaman çalışır. Boş bırakılabilir.
	BLOK_C - for bloğunun sonlanacağı zamanı belirten koşul komutudur. Burasının ilk değeri "yanlış" ise döngü başlamaz. Boş bırakılması uygun değildir. Boş ise döngü içinde mutlaka break veya exit bulunmalıdır.
	BLOK_D - for bloğunun iterasyon komutudur. 
	BLOK_E - for bloğunun uygulama bloğudur. BLOK_E işlendikten sonra önce bu işlem gerçekleştirilir ardından BLOK_C'ye tekrar bakılır.
	BLOK_F - for sonrasında çalışır.
	
	sıralama - A,B,[C,E,D],F --> sırasıyla çalışır. D sonrası C'ye geri döner.



***Soru Nu[3]
Ogrenci[17-J-9501-YUSUF MERT  VELIOGLU]
Puan: 10
--------->
- Hafta 04, Konu : turetilmis veri yapilari

	a. [56, ___, ___]
	b. Yığın - Stack
	c. 
		(1). push(47) - [56, 47, ___]
		(2). pop() [hangi sayı diziden çıkar = 47]
		(3). pop() [hangi sayı diziden çıkar = 56]
		(4). pop() [hangi sayı diziden çıkar = hiçbir sayı çıkmaz]
		(6). push(81) - [81, ___, ___]
		(7). push(64) - [81, 64, ___]
		(8). push(85) - [81, 64, 85]
	d. Queue
	e. 
		(1). dequeue() [hangi sayı diziden çıkar = 56]
		(2). dequeue() [hangi sayı diziden çıkar = hiçbir sayı çıkmaz]
		(3). enqueue(52) - [___, 52, ___]
		(4). enqueue(41) - [___, 52, 41]
		(5). enqueue(33) - [33, 52, 41]
		(6). dequeue() [hangi sayı diziden çıkar = 52]



***Soru Nu[4]
Ogrenci[17-J-9501-YUSUF MERT  VELIOGLU]
Puan: 60
--------->
Soru seceneklerinde belirtilen fonksiyonlara bakarak her bir secenegi cozun. Konu : Siralama algoritmalari

Asagidaki seceneklerde belirtilen f1, f2, f3, f4 fonksiyonlara gore sorulari cozunuz.
	a. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[13 14 15 10 11 12 9 8] dizisini siralayiniz.

void f1_sort_merge(int arr[], int i, int n)  
{  
    if(n>1) 
    {
        j=n/2;
        ms(arr,i,j);
        ms(arr,j,n-j);
        m(D,i,j,n);
    }
}

8 9 10 11 12 13 14 15
	b. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[4 26 92 64 19 6 67 99 20 59] dizisini siralayiniz.

void f2_sort_selection(int arr[], int n)  
{  
    int i, j, k, m;
    for (i=0;i<n-1;i++) 
    {
        k=n-1;
        m=arr[k];
        for(j=i;j<n-1;j++)
            if(arr[j]<m)
            {
                k=j;
                m=arr[j];
            }
        arr[k]=arr[i];
        arr[i]=m;
    }
}

Baslangic D=[4 26 92 64 19 6 67 99 20 59]
_____--------------------------------------------------
  i | 00 | 01 | 02 | 03 | 04 | 05 | 06 | 07 | 08 | 09 |
_____--------------------------------------------------
 00 |* 4*| 26 | 92 | 64 | 19 |  6 | 67 | 99 | 20 | 59 |
_____--------------------------------------------------
 01 |  4 |* 6*| 92 | 64 | 19 |*26*| 67 | 99 | 20 | 59 |
_____--------------------------------------------------
 02 |  4 |  6 |*19*| 64 |*92*| 26 | 67 | 99 | 20 | 59 |
_____--------------------------------------------------
 03 |  4 |  6 | 19 |*20*| 92 | 26 | 67 | 99 |*64*| 59 |
_____--------------------------------------------------
 04 |  4 |  6 | 19 | 20 |*26*|*92*| 67 | 99 | 64 | 59 |
_____--------------------------------------------------
 05 |  4 |  6 | 19 | 20 | 26 |*59*| 67 | 99 | 64 |*92*|
_____--------------------------------------------------
 06 |  4 |  6 | 19 | 20 | 26 | 59 |*64*| 99 |*67*| 92 |
_____--------------------------------------------------
 07 |  4 |  6 | 19 | 20 | 26 | 59 | 64 |*67*|*99*| 92 |
_____--------------------------------------------------
 08 |  4 |  6 | 19 | 20 | 26 | 59 | 64 | 67 |*92*|*99*|
_____--------------------------------------------------
9 islem yapildi.
Son D=[4 6 19 20 26 59 64 67 92 99]

	c. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[4 61 51 73 27 83 92] dizisini siralayiniz.

void f3_sort_insertion(int arr[], int n)  
{  
    int i, j, k;  
    for (i=1;i<n;i++) 
    {  
        k = arr[i];  
        for(j=i-1;j>=0 && arr[j]>k;j--) 
            arr[j+1]=arr[j];  
        arr[j+1]=k;
    }
}

Baslangic D=[4 61 51 73 27 83 92]
_____-----------------------------------______________
  i | 00 | 01 | 02 | 03 | 04 | 05 | 06 |  k |ekle|D(k)
_____-----------------------------------______________
 01 |  4 |*61*| 51 | 73 | 27 | 83 | 92 |  0 | 61 |  4 
_____-----------------------------------______________
 02 |  4 |*61*|*61*| 73 | 27 | 83 | 92 |  1 | 51 | 61 
_____-----------------------------------______________
 02 |  4 |*51*| 61 | 73 | 27 | 83 | 92 |  0 | 51 |  4 
_____-----------------------------------______________
 03 |  4 | 51 | 61 |*73*| 27 | 83 | 92 |  2 | 73 | 61 
_____-----------------------------------______________
 04 |  4 | 51 | 61 |*73*|*73*| 83 | 92 |  3 | 27 | 73 
_____-----------------------------------______________
 04 |  4 | 51 |*61*|*61*| 73 | 83 | 92 |  2 | 27 | 61 
_____-----------------------------------______________
 04 |  4 |*51*|*51*| 61 | 73 | 83 | 92 |  1 | 27 | 51 
_____-----------------------------------______________
 04 |  4 |*27*| 51 | 61 | 73 | 83 | 92 |  0 | 27 |  4 
_____-----------------------------------______________
 05 |  4 | 27 | 51 | 61 | 73 |*83*| 92 |  4 | 83 | 73 
_____-----------------------------------______________
 06 |  4 | 27 | 51 | 61 | 73 | 83 |*92*|  5 | 92 | 83 
_____-----------------------------------______________
10 islem yapildi.
Son D=[4 27 51 61 73 83 92]

	d. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[64 62 3 4 31 67 99 68 87 97] dizisini siralayiniz.

void f4_sort_bubble(int arr[], int n)  
{  
    int i, j;
    for (i=0;i<n-1;i++) 
        for(j=0;j<n-1-i;j++) 
            if(arr[j]>arr[j+1]) 
	        swap(arr,j,j+1);
}

Baslangic D=[64 62 3 4 31 67 99 68 87 97]
--------------------------------------------------_________
 00 | 01 | 02 | 03 | 04 | 05 | 06 | 07 | 08 | 09 | mov|  k 
--------------------------------------------------_________
*62*|*64*|  3 |  4 | 31 | 67 | 99 | 68 | 87 | 97 |  0 |  0 
--------------------------------------------------_________
 62 |* 3*|*64*|  4 | 31 | 67 | 99 | 68 | 87 | 97 |  0 |  1 
--------------------------------------------------_________
 62 |  3 |* 4*|*64*| 31 | 67 | 99 | 68 | 87 | 97 |  0 |  2 
--------------------------------------------------_________
 62 |  3 |  4 |*31*|*64*| 67 | 99 | 68 | 87 | 97 |  0 |  3 
--------------------------------------------------_________
 62 |  3 |  4 | 31 | 64 | 67 |*68*|*99*| 87 | 97 |  0 |  6 
--------------------------------------------------_________
 62 |  3 |  4 | 31 | 64 | 67 | 68 |*87*|*99*| 97 |  0 |  7 
--------------------------------------------------_________
 62 |  3 |  4 | 31 | 64 | 67 | 68 | 87 |*97*|*99*|  0 |  8 
--------------------------------------------------_________
* 3*|*62*|  4 | 31 | 64 | 67 | 68 | 87 | 97 | 99 |  1 |  0 
--------------------------------------------------_________
  3 |* 4*|*62*| 31 | 64 | 67 | 68 | 87 | 97 | 99 |  1 |  1 
--------------------------------------------------_________
  3 |  4 |*31*|*62*| 64 | 67 | 68 | 87 | 97 | 99 |  1 |  2 
--------------------------------------------------_________
10 islem yapildi.
Son D=[3 4 31 62 64 67 68 87 97 99]




