Sinav 5 soru ve toplam 100 puandan olusmakta olup, puan dagilimi su sekildedir:
[SoruNu-Puan]-->[0-10],[1-10],[2-10],[3-10],[4-60]

***Soru Nu[0]
Ogrenci[5-J-9080-MUSA YIGCI]
Puan: 10
--------->
Asagidaki sorularin cevaplarini yanlarina yaziniz. Hafta 04, Konu : Degiskenlerin tanimli oldugu araliklar.
  Eger integer bilgisayarda 5 bit ile ifade edilseydi
    a. -pow(2, 4)=-16
    b. pow(2, 4)-1=15
    c. 0
    d. pow(2, 5)=32
    e. [1 0 0 0 0]
    f. [0 1 1 1 1]
    g. [0 0 0 0 0]
    h. [1 1 1 1 1]



***Soru Nu[1]
Ogrenci[5-J-9080-MUSA YIGCI]
Puan: 10
--------->
Asagidaki programin hafizada kapladigi boyutu hesaplayiniz. Hafta 03, Konu : Veri yapilari ve hafiza.
      2 integers --> 2*2 bytes = 32 bits.
      8 chars --> 7 + 1 bytes = 64 bits.
      4 float --> 4*2 bytes = 64 bits.
      86 long --> 86*4 bytes = 2752 bits.
      TOPLAM --> 364 bytes = 2912 bits.



***Soru Nu[2]
Ogrenci[5-J-9080-MUSA YIGCI]
Puan: 10
--------->
 Konu : Genel Programlama Bilgisi

	BLOK_A - for öncesinde çalışır.
	BLOK_B - for bloğunun ilklendirici komutudur. Her zaman çalışır. Boş bırakılabilir.
	BLOK_C - for bloğunun sonlanacağı zamanı belirten koşul komutudur. Burasının ilk değeri "yanlış" ise döngü başlamaz. Boş bırakılması uygun değildir. Boş ise döngü içinde mutlaka break veya exit bulunmalıdır.
	BLOK_D - for bloğunun iterasyon komutudur. 
	BLOK_E - for bloğunun uygulama bloğudur. BLOK_E işlendikten sonra önce bu işlem gerçekleştirilir ardından BLOK_C'ye tekrar bakılır.
	BLOK_F - for sonrasında çalışır.
	
	sıralama - A,B,[C,E,D],F --> sırasıyla çalışır. D sonrası C'ye geri döner.



***Soru Nu[3]
Ogrenci[5-J-9080-MUSA YIGCI]
Puan: 10
--------->
- Hafta 04, Konu : turetilmis veri yapilari

	a. [47, ___, ___]
	b. Yığın - Stack
	c. 
		(1). push(13) - [47, 13, ___]
		(2). pop() [hangi sayı diziden çıkar = 13]
		(3). pop() [hangi sayı diziden çıkar = 47]
		(4). pop() [hangi sayı diziden çıkar = hiçbir sayı çıkmaz]
		(6). push(80) - [80, ___, ___]
		(7). push(98) - [80, 98, ___]
		(8). push(57) - [80, 98, 57]
	d. Queue
	e. 
		(1). dequeue() [hangi sayı diziden çıkar = 47]
		(2). dequeue() [hangi sayı diziden çıkar = hiçbir sayı çıkmaz]
		(3). enqueue(55) - [___, 55, ___]
		(4). enqueue(93) - [___, 55, 93]
		(5). enqueue(96) - [96, 55, 93]
		(6). dequeue() [hangi sayı diziden çıkar = 55]



***Soru Nu[4]
Ogrenci[5-J-9080-MUSA YIGCI]
Puan: 60
--------->
Soru seceneklerinde belirtilen fonksiyonlara bakarak her bir secenegi cozun. Konu : Siralama algoritmalari

Asagidaki seceneklerde belirtilen f1, f2, f3, f4 fonksiyonlara gore sorulari cozunuz.
	a. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[60 34 65 62 52 31 69 7 26 70] dizisini siralayiniz.

void f1_sort_selection(int arr[], int n)  
{  
    int i, j, k, m;
    for (i=0;i<n-1;i++) 
    {
        k=n-1;
        m=arr[k];
        for(j=i;j<n-1;j++)
            if(arr[j]<m)
            {
                k=j;
                m=arr[j];
            }
        arr[k]=arr[i];
        arr[i]=m;
    }
}

Baslangic D=[60 34 65 62 52 31 69 7 26 70]
_____--------------------------------------------------
  i | 00 | 01 | 02 | 03 | 04 | 05 | 06 | 07 | 08 | 09 |
_____--------------------------------------------------
 00 |* 7*| 34 | 65 | 62 | 52 | 31 | 69 |*60*| 26 | 70 |
_____--------------------------------------------------
 01 |  7 |*26*| 65 | 62 | 52 | 31 | 69 | 60 |*34*| 70 |
_____--------------------------------------------------
 02 |  7 | 26 |*31*| 62 | 52 |*65*| 69 | 60 | 34 | 70 |
_____--------------------------------------------------
 03 |  7 | 26 | 31 |*34*| 52 | 65 | 69 | 60 |*62*| 70 |
_____--------------------------------------------------
 04 |  7 | 26 | 31 | 34 |*52*| 65 | 69 | 60 | 62 | 70 |
_____--------------------------------------------------
 05 |  7 | 26 | 31 | 34 | 52 |*60*| 69 |*65*| 62 | 70 |
_____--------------------------------------------------
 06 |  7 | 26 | 31 | 34 | 52 | 60 |*62*| 65 |*69*| 70 |
_____--------------------------------------------------
 07 |  7 | 26 | 31 | 34 | 52 | 60 | 62 |*65*| 69 | 70 |
_____--------------------------------------------------
 08 |  7 | 26 | 31 | 34 | 52 | 60 | 62 | 65 |*69*| 70 |
_____--------------------------------------------------
9 islem yapildi.
Son D=[7 26 31 34 52 60 62 65 69 70]

	b. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[64 98 76 70 61 81 92 95] dizisini siralayiniz.

void f2_sort_bubble(int arr[], int n)  
{  
    int i, j;
    for (i=0;i<n-1;i++) 
        for(j=0;j<n-1-i;j++) 
            if(arr[j]>arr[j+1]) 
	        swap(arr,j,j+1);
}

Baslangic D=[64 98 76 70 61 81 92 95]
----------------------------------------_________
 00 | 01 | 02 | 03 | 04 | 05 | 06 | 07 | mov|  k 
----------------------------------------_________
 64 |*76*|*98*| 70 | 61 | 81 | 92 | 95 |  0 |  1 
----------------------------------------_________
 64 | 76 |*70*|*98*| 61 | 81 | 92 | 95 |  0 |  2 
----------------------------------------_________
 64 | 76 | 70 |*61*|*98*| 81 | 92 | 95 |  0 |  3 
----------------------------------------_________
 64 | 76 | 70 | 61 |*81*|*98*| 92 | 95 |  0 |  4 
----------------------------------------_________
 64 | 76 | 70 | 61 | 81 |*92*|*98*| 95 |  0 |  5 
----------------------------------------_________
 64 | 76 | 70 | 61 | 81 | 92 |*95*|*98*|  0 |  6 
----------------------------------------_________
 64 |*70*|*76*| 61 | 81 | 92 | 95 | 98 |  1 |  1 
----------------------------------------_________
 64 | 70 |*61*|*76*| 81 | 92 | 95 | 98 |  1 |  2 
----------------------------------------_________
 64 |*61*|*70*| 76 | 81 | 92 | 95 | 98 |  2 |  1 
----------------------------------------_________
*61*|*64*| 70 | 76 | 81 | 92 | 95 | 98 |  3 |  0 
----------------------------------------_________
10 islem yapildi.
Son D=[61 64 70 76 81 92 95 98]

	c. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[73 9 23 82 80 99] dizisini siralayiniz.

void f3_sort_insertion(int arr[], int n)  
{  
    int i, j, k;  
    for (i=1;i<n;i++) 
    {  
        k = arr[i];  
        for(j=i-1;j>=0 && arr[j]>k;j--) 
            arr[j+1]=arr[j];  
        arr[j+1]=k;
    }
}

Baslangic D=[73 9 23 82 80 99]
_____------------------------------______________
  i | 00 | 01 | 02 | 03 | 04 | 05 |  k |ekle|D(k)
_____------------------------------______________
 01 |*73*|*73*| 23 | 82 | 80 | 99 |  0 |  9 | 73 
_____------------------------------______________
 01 |* 9*| 73 | 23 | 82 | 80 | 99 | -1 |  9 | NaN 
_____------------------------------______________
 02 |  9 |*73*|*73*| 82 | 80 | 99 |  1 | 23 | 73 
_____------------------------------______________
 02 |  9 |*23*| 73 | 82 | 80 | 99 |  0 | 23 |  9 
_____------------------------------______________
 03 |  9 | 23 | 73 |*82*| 80 | 99 |  2 | 82 | 73 
_____------------------------------______________
 04 |  9 | 23 | 73 |*82*|*82*| 99 |  3 | 80 | 82 
_____------------------------------______________
 04 |  9 | 23 | 73 |*80*| 82 | 99 |  2 | 80 | 73 
_____------------------------------______________
 05 |  9 | 23 | 73 | 80 | 82 |*99*|  4 | 99 | 82 
_____------------------------------______________
8 islem yapildi.
Son D=[9 23 73 80 82 99]

	d. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[13 12 16 15 11 10 14 9] dizisini siralayiniz.

void f4_sort_merge(int arr[], int i, int n)  
{  
    if(n>1) 
    {
        j=n/2;
        ms(arr,i,j);
        ms(arr,j,n-j);
        m(D,i,j,n);
    }
}

9 10 11 12 13 14 15 16



