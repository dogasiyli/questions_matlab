Sinav 5 soru ve toplam 100 puandan olusmakta olup, puan dagilimi su sekildedir:
[SoruNu-Puan]-->[0-10],[1-10],[2-10],[3-10],[4-60]

***Soru Nu[0]
Ogrenci[19-J-9515-MUSTAFA INCEMAN]
Puan: 10
--------->
Asagidaki sorularin cevaplarini yanlarina yaziniz. Hafta 04, Konu : Degiskenlerin tanimli oldugu araliklar.
  Eger integer bilgisayarda 9 bit ile ifade edilseydi
    a. -pow(2, 8)=-256
    b. pow(2, 8)-1=255
    c. 0
    d. pow(2, 9)=512
    e. [1 0 0 0 0 0 0 0 0]
    f. [0 1 1 1 1 1 1 1 1]
    g. [0 0 0 0 0 0 0 0 0]
    h. [1 1 1 1 1 1 1 1 1]



***Soru Nu[1]
Ogrenci[19-J-9515-MUSTAFA INCEMAN]
Puan: 10
--------->
Asagidaki programin hafizada kapladigi boyutu hesaplayiniz. Hafta 03, Konu : Veri yapilari ve hafiza.
      5 integers --> 5*2 bytes = 80 bits.
      5 chars --> 4 + 1 bytes = 40 bits.
      8 float --> 8*2 bytes = 128 bits.
      76 long --> 76*4 bytes = 2432 bits.
      TOPLAM --> 335 bytes = 2680 bits.



***Soru Nu[2]
Ogrenci[19-J-9515-MUSTAFA INCEMAN]
Puan: 10
--------->
 Konu : Genel Programlama Bilgisi

	BLOK_A - for öncesinde çalışır.
	BLOK_B - for bloğunun ilklendirici komutudur. Her zaman çalışır. Boş bırakılabilir.
	BLOK_C - for bloğunun sonlanacağı zamanı belirten koşul komutudur. Burasının ilk değeri "yanlış" ise döngü başlamaz. Boş bırakılması uygun değildir. Boş ise döngü içinde mutlaka break veya exit bulunmalıdır.
	BLOK_D - for bloğunun iterasyon komutudur. 
	BLOK_E - for bloğunun uygulama bloğudur. BLOK_E işlendikten sonra önce bu işlem gerçekleştirilir ardından BLOK_C'ye tekrar bakılır.
	BLOK_F - for sonrasında çalışır.
	
	sıralama - A,B,[C,E,D],F --> sırasıyla çalışır. D sonrası C'ye geri döner.



***Soru Nu[3]
Ogrenci[19-J-9515-MUSTAFA INCEMAN]
Puan: 10
--------->
- Hafta 04, Konu : turetilmis veri yapilari

	a. [58, ___, ___]
	b. Yığın - Stack
	c. 
		(1). push(73) - [58, 73, ___]
		(2). pop() [hangi sayı diziden çıkar = 73]
		(3). pop() [hangi sayı diziden çıkar = 58]
		(4). pop() [hangi sayı diziden çıkar = hiçbir sayı çıkmaz]
		(6). push(33) - [33, ___, ___]
		(7). push(85) - [33, 85, ___]
		(8). push(97) - [33, 85, 97]
	d. Queue
	e. 
		(1). dequeue() [hangi sayı diziden çıkar = 58]
		(2). dequeue() [hangi sayı diziden çıkar = hiçbir sayı çıkmaz]
		(3). enqueue(36) - [___, 36, ___]
		(4). enqueue(68) - [___, 36, 68]
		(5). enqueue(22) - [22, 36, 68]
		(6). dequeue() [hangi sayı diziden çıkar = 36]



***Soru Nu[4]
Ogrenci[19-J-9515-MUSTAFA INCEMAN]
Puan: 60
--------->
Soru seceneklerinde belirtilen fonksiyonlara bakarak her bir secenegi cozun. Konu : Siralama algoritmalari

Asagidaki seceneklerde belirtilen f1, f2, f3, f4 fonksiyonlara gore sorulari cozunuz.
	a. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[13 18 49 12 87 92 32 74] dizisini siralayiniz.

void f1_sort_bubble(int arr[], int n)  
{  
    int i, j;
    for (i=0;i<n-1;i++) 
        for(j=0;j<n-1-i;j++) 
            if(arr[j]>arr[j+1]) 
	        swap(arr,j,j+1);
}

Baslangic D=[13 18 49 12 87 92 32 74]
----------------------------------------_________
 00 | 01 | 02 | 03 | 04 | 05 | 06 | 07 | mov|  k 
----------------------------------------_________
 13 | 18 |*12*|*49*| 87 | 92 | 32 | 74 |  0 |  2 
----------------------------------------_________
 13 | 18 | 12 | 49 | 87 |*32*|*92*| 74 |  0 |  5 
----------------------------------------_________
 13 | 18 | 12 | 49 | 87 | 32 |*74*|*92*|  0 |  6 
----------------------------------------_________
 13 |*12*|*18*| 49 | 87 | 32 | 74 | 92 |  1 |  1 
----------------------------------------_________
 13 | 12 | 18 | 49 |*32*|*87*| 74 | 92 |  1 |  4 
----------------------------------------_________
 13 | 12 | 18 | 49 | 32 |*74*|*87*| 92 |  1 |  5 
----------------------------------------_________
*12*|*13*| 18 | 49 | 32 | 74 | 87 | 92 |  2 |  0 
----------------------------------------_________
 12 | 13 | 18 |*32*|*49*| 74 | 87 | 92 |  2 |  3 
----------------------------------------_________
8 islem yapildi.
Son D=[12 13 18 32 49 74 87 92]

	b. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[12 14 9 11 8 15 13 10] dizisini siralayiniz.

void f2_sort_merge(int arr[], int i, int n)  
{  
    if(n>1) 
    {
        j=n/2;
        ms(arr,i,j);
        ms(arr,j,n-j);
        m(D,i,j,n);
    }
}

8 9 10 11 12 13 14 15
	c. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[18 72 12 80 25 91 4 84 50 64] dizisini siralayiniz.

void f3_sort_selection(int arr[], int n)  
{  
    int i, j, k, m;
    for (i=0;i<n-1;i++) 
    {
        k=n-1;
        m=arr[k];
        for(j=i;j<n-1;j++)
            if(arr[j]<m)
            {
                k=j;
                m=arr[j];
            }
        arr[k]=arr[i];
        arr[i]=m;
    }
}

Baslangic D=[18 72 12 80 25 91 4 84 50 64]
_____--------------------------------------------------
  i | 00 | 01 | 02 | 03 | 04 | 05 | 06 | 07 | 08 | 09 |
_____--------------------------------------------------
 00 |* 4*| 72 | 12 | 80 | 25 | 91 |*18*| 84 | 50 | 64 |
_____--------------------------------------------------
 01 |  4 |*12*|*72*| 80 | 25 | 91 | 18 | 84 | 50 | 64 |
_____--------------------------------------------------
 02 |  4 | 12 |*18*| 80 | 25 | 91 |*72*| 84 | 50 | 64 |
_____--------------------------------------------------
 03 |  4 | 12 | 18 |*25*|*80*| 91 | 72 | 84 | 50 | 64 |
_____--------------------------------------------------
 04 |  4 | 12 | 18 | 25 |*50*| 91 | 72 | 84 |*80*| 64 |
_____--------------------------------------------------
 05 |  4 | 12 | 18 | 25 | 50 |*64*| 72 | 84 | 80 |*91*|
_____--------------------------------------------------
 06 |  4 | 12 | 18 | 25 | 50 | 64 |*72*| 84 | 80 | 91 |
_____--------------------------------------------------
 07 |  4 | 12 | 18 | 25 | 50 | 64 | 72 |*80*|*84*| 91 |
_____--------------------------------------------------
 08 |  4 | 12 | 18 | 25 | 50 | 64 | 72 | 80 |*84*| 91 |
_____--------------------------------------------------
9 islem yapildi.
Son D=[4 12 18 25 50 64 72 80 84 91]

	d. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[70 1 37 51 69 96 97] dizisini siralayiniz.

void f4_sort_insertion(int arr[], int n)  
{  
    int i, j, k;  
    for (i=1;i<n;i++) 
    {  
        k = arr[i];  
        for(j=i-1;j>=0 && arr[j]>k;j--) 
            arr[j+1]=arr[j];  
        arr[j+1]=k;
    }
}

Baslangic D=[70 1 37 51 69 96 97]
_____-----------------------------------______________
  i | 00 | 01 | 02 | 03 | 04 | 05 | 06 |  k |ekle|D(k)
_____-----------------------------------______________
 01 |*70*|*70*| 37 | 51 | 69 | 96 | 97 |  0 |  1 | 70 
_____-----------------------------------______________
 01 |* 1*| 70 | 37 | 51 | 69 | 96 | 97 | -1 |  1 | NaN 
_____-----------------------------------______________
 02 |  1 |*70*|*70*| 51 | 69 | 96 | 97 |  1 | 37 | 70 
_____-----------------------------------______________
 02 |  1 |*37*| 70 | 51 | 69 | 96 | 97 |  0 | 37 |  1 
_____-----------------------------------______________
 03 |  1 | 37 |*70*|*70*| 69 | 96 | 97 |  2 | 51 | 70 
_____-----------------------------------______________
 03 |  1 | 37 |*51*| 70 | 69 | 96 | 97 |  1 | 51 | 37 
_____-----------------------------------______________
 04 |  1 | 37 | 51 |*70*|*70*| 96 | 97 |  3 | 69 | 70 
_____-----------------------------------______________
 04 |  1 | 37 | 51 |*69*| 70 | 96 | 97 |  2 | 69 | 51 
_____-----------------------------------______________
 05 |  1 | 37 | 51 | 69 | 70 |*96*| 97 |  4 | 96 | 70 
_____-----------------------------------______________
 06 |  1 | 37 | 51 | 69 | 70 | 96 |*97*|  5 | 97 | 96 
_____-----------------------------------______________
10 islem yapildi.
Son D=[1 37 51 69 70 96 97]




