Sinav 5 soru ve toplam 100 puandan olusmakta olup, puan dagilimi su sekildedir:
[SoruNu-Puan]-->[0-10],[1-10],[2-10],[3-10],[4-60]

***Soru Nu[0]
Ogrenci[20-J-9516-EREN SARIARSLAN]
Puan: 10
--------->
Asagidaki sorularin cevaplarini yanlarina yaziniz. Hafta 04, Konu : Degiskenlerin tanimli oldugu araliklar.

Eger integer bilgisayarda 7 bit ile ifade edilseydi
  a. signed integer'in alabilecegi minimum deger ne olurdu?
  b. signed integer'in alabilecegi maximum deger ne olurdu?
  c. unsigned integer'in alabilecegi minimum deger ne olurdu?
  d. unsigned integer'in alabilecegi maximum deger ne olurdu?
  e. signed integer'in alabilecegi minimum degeri 2 bitlik duzende yaziniz:
    _,_,_,_,_,_,_
  f. signed integer'in alabilecegi maximum degeri 2 bitlik duzende yaziniz:
    _,_,_,_,_,_,_
  g. unsigned integer'in alabilecegi minimum degeri 2 bitlik duzende yaziniz:
    _,_,_,_,_,_,_
  h. unsigned integer'in alabilecegi maximum degeri 2 bitlik duzende yaziniz:
    _,_,_,_,_,_,_



***Soru Nu[1]
Ogrenci[20-J-9516-EREN SARIARSLAN]
Puan: 10
--------->
Asagidaki programin hafizada kapladigi boyutu hesaplayiniz. Hafta 03, Konu : Veri yapilari ve hafiza.

Eger bir bilgisayarda veri tiplerinin boyutlari asagidaki gibiyse asagidaki fonksiyonlar hafizada kac bit yer kaplar?
integer-2 byte, long-4 byte, float 2 byte.

    void main()
    {
      int A[] = {429,385,280,25,263};
      char B[]="Fare";
      float C[] = {184.59,38.291,125.91,183.1,66.271,79.481,118.78,139.95,132.25,11.651};
      long long D[22];
    }



***Soru Nu[2]
Ogrenci[20-J-9516-EREN SARIARSLAN]
Puan: 10
--------->
 Konu : Genel Programlama Bilgisi

Aşağıda yer alan for döngüsünde belirtilen blokların hangi sırayla çalışacağını anlatınız. Bir örnek ile gösteriniz. 
	BLOK_A
	for (BLOK_B;BLOK_C;BLOK_D)
	{
		BLOK_E;
	}
	BLOK_F;



***Soru Nu[3]
Ogrenci[20-J-9516-EREN SARIARSLAN]
Puan: 10
--------->
- Hafta 04, Konu : turetilmis veri yapilari

Integer tipinde, adı Xarr olan 3 elemanlı bir dizi tanımlayın. 
	a. Dizinin birinci elemanını 88 olarak doldurun. Diziyi kutucuklarla gösterin.
	b. Bu diziye aşağıdaki push ve pop işlemleri uygulandığına göre bu dizi hangi türetilmiş veri yapısına sahiptir?
	c. Bu diziye sırayla aşağıdaki işlemleri uygulayın ve her işlemden sonra dizinin kutularının şeklini çizin.
		(1). push(62)
		(2). pop() [hangi sayı diziden çıkar = ___]
		(3). pop() [hangi sayı diziden çıkar = ___]
		(4). pop() [hangi sayı diziden çıkar = ___]
		(6). push(21)
		(7). push(93)
		(8). push(82)
	d. Bu diziye aşağıdaki enqueue ve dequeue işlemleri uygulansaydı bu dizi hangi türetilmiş veri yapısına sahip olurdu?
	e. Bu diziye "a." maddesindeki tek elemanlı halinden başlayarak sırayla aşağıdaki işlemleri uygulayın ve her işlemden sonra dizinin kutularının şeklini çizin.
		(1). dequeue() [hangi sayı diziden çıkar = ___]
		(2). dequeue() [hangi sayı diziden çıkar = ___]
		(3). enqueue(64)
		(4). enqueue(19)
		(5). enqueue(26)
		(6). dequeue() [hangi sayı diziden çıkar = ___]
		



***Soru Nu[4]
Ogrenci[20-J-9516-EREN SARIARSLAN]
Puan: 60
--------->
Soru seceneklerinde belirtilen fonksiyonlara bakarak her bir secenegi cozun. Konu : Siralama algoritmalari

Asagidaki seceneklerde belirtilen f1, f2, f3, f4 fonksiyonlara gore sorulari cozunuz.
	a. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[40 94 55 66 16 69 39 51 57 13] dizisini siralayiniz.

void f1(int arr[], int n)  
{  
    int i, j, k, m;
    for (i=0;i<n-1;i++) 
    {
        k=n-1;
        m=arr[k];
        for(j=i;j<n-1;j++)
            if(arr[j]<m)
            {
                k=j;
                m=arr[j];
            }
        arr[k]=arr[i];
        arr[i]=m;
    }
}

	b. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[8 6 3 5 4 7 9 2] dizisini siralayiniz.

void f2(int arr[], int i, int n)  
{  
    if(n>1) 
    {
        j=n/2;
        ms(arr,i,j);
        ms(arr,j,n-j);
        m(D,i,j,n);
    }
}

	c. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[6 21 1 98 43 18 26] dizisini siralayiniz.

void f3(int arr[], int n)  
{  
    int i, j;
    for (i=0;i<n-1;i++) 
        for(j=0;j<n-1-i;j++) 
            if(arr[j]>arr[j+1]) 
	        swap(arr,j,j+1);
}

	d. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[36 9 83 8 97 87] dizisini siralayiniz.

void f4(int arr[], int n)  
{  
    int i, j, k;  
    for (i=1;i<n;i++) 
    {  
        k = arr[i];  
        for(j=i-1;j>=0 && arr[j]>k;j--) 
            arr[j+1]=arr[j];  
        arr[j+1]=k;
    }
}




