Sinav 5 soru ve toplam 100 puandan olusmakta olup, puan dagilimi su sekildedir:
[SoruNu-Puan]-->[0-10],[1-10],[2-10],[3-10],[4-60]

***Soru Nu[0]
Ogrenci[14-J-9189-HALIS KAAN AYDOGAN]
Puan: 10
--------->
Asagidaki sorularin cevaplarini yanlarina yaziniz. Hafta 04, Konu : Degiskenlerin tanimli oldugu araliklar.
  Eger integer bilgisayarda 5 bit ile ifade edilseydi
    a. -pow(2, 4)=-16
    b. pow(2, 4)-1=15
    c. 0
    d. pow(2, 5)=32
    e. [1 0 0 0 0]
    f. [0 1 1 1 1]
    g. [0 0 0 0 0]
    h. [1 1 1 1 1]



***Soru Nu[1]
Ogrenci[14-J-9189-HALIS KAAN AYDOGAN]
Puan: 10
--------->
Asagidaki programin hafizada kapladigi boyutu hesaplayiniz. Hafta 03, Konu : Veri yapilari ve hafiza.
      8 integers --> 8*2 bytes = 128 bits.
      11 chars --> 10 + 1 bytes = 88 bits.
      7 float --> 7*2 bytes = 112 bits.
      89 long --> 89*4 bytes = 2848 bits.
      TOPLAM --> 397 bytes = 3176 bits.



***Soru Nu[2]
Ogrenci[14-J-9189-HALIS KAAN AYDOGAN]
Puan: 10
--------->
 Konu : Genel Programlama Bilgisi

	BLOK_A - for öncesinde çalışır.
	BLOK_B - for bloğunun ilklendirici komutudur. Her zaman çalışır. Boş bırakılabilir.
	BLOK_C - for bloğunun sonlanacağı zamanı belirten koşul komutudur. Burasının ilk değeri "yanlış" ise döngü başlamaz. Boş bırakılması uygun değildir. Boş ise döngü içinde mutlaka break veya exit bulunmalıdır.
	BLOK_D - for bloğunun iterasyon komutudur. 
	BLOK_E - for bloğunun uygulama bloğudur. BLOK_E işlendikten sonra önce bu işlem gerçekleştirilir ardından BLOK_C'ye tekrar bakılır.
	BLOK_F - for sonrasında çalışır.
	
	sıralama - A,B,[C,E,D],F --> sırasıyla çalışır. D sonrası C'ye geri döner.



***Soru Nu[3]
Ogrenci[14-J-9189-HALIS KAAN AYDOGAN]
Puan: 10
--------->
- Hafta 04, Konu : turetilmis veri yapilari

	a. [90, ___, ___]
	b. Yığın - Stack
	c. 
		(1). push(74) - [90, 74, ___]
		(2). pop() [hangi sayı diziden çıkar = 74]
		(3). pop() [hangi sayı diziden çıkar = 90]
		(4). pop() [hangi sayı diziden çıkar = hiçbir sayı çıkmaz]
		(6). push(26) - [26, ___, ___]
		(7). push(81) - [26, 81, ___]
		(8). push(16) - [26, 81, 16]
	d. Queue
	e. 
		(1). dequeue() [hangi sayı diziden çıkar = 90]
		(2). dequeue() [hangi sayı diziden çıkar = hiçbir sayı çıkmaz]
		(3). enqueue(73) - [___, 73, ___]
		(4). enqueue(36) - [___, 73, 36]
		(5). enqueue(51) - [51, 73, 36]
		(6). dequeue() [hangi sayı diziden çıkar = 73]



***Soru Nu[4]
Ogrenci[14-J-9189-HALIS KAAN AYDOGAN]
Puan: 60
--------->
Soru seceneklerinde belirtilen fonksiyonlara bakarak her bir secenegi cozun. Konu : Siralama algoritmalari

Asagidaki seceneklerde belirtilen f1, f2, f3, f4 fonksiyonlara gore sorulari cozunuz.
	a. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[9 12 8 7 11 5 6 10] dizisini siralayiniz.

void f1_sort_merge(int arr[], int i, int n)  
{  
    if(n>1) 
    {
        j=n/2;
        ms(arr,i,j);
        ms(arr,j,n-j);
        m(D,i,j,n);
    }
}

5 6 7 8 9 10 11 12
	b. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[17 68 56 61 78 74 75] dizisini siralayiniz.

void f2_sort_insertion(int arr[], int n)  
{  
    int i, j, k;  
    for (i=1;i<n;i++) 
    {  
        k = arr[i];  
        for(j=i-1;j>=0 && arr[j]>k;j--) 
            arr[j+1]=arr[j];  
        arr[j+1]=k;
    }
}

Baslangic D=[17 68 56 61 78 74 75]
_____-----------------------------------______________
  i | 00 | 01 | 02 | 03 | 04 | 05 | 06 |  k |ekle|D(k)
_____-----------------------------------______________
 01 | 17 |*68*| 56 | 61 | 78 | 74 | 75 |  0 | 68 | 17 
_____-----------------------------------______________
 02 | 17 |*68*|*68*| 61 | 78 | 74 | 75 |  1 | 56 | 68 
_____-----------------------------------______________
 02 | 17 |*56*| 68 | 61 | 78 | 74 | 75 |  0 | 56 | 17 
_____-----------------------------------______________
 03 | 17 | 56 |*68*|*68*| 78 | 74 | 75 |  2 | 61 | 68 
_____-----------------------------------______________
 03 | 17 | 56 |*61*| 68 | 78 | 74 | 75 |  1 | 61 | 56 
_____-----------------------------------______________
 04 | 17 | 56 | 61 | 68 |*78*| 74 | 75 |  3 | 78 | 68 
_____-----------------------------------______________
 05 | 17 | 56 | 61 | 68 |*78*|*78*| 75 |  4 | 74 | 78 
_____-----------------------------------______________
 05 | 17 | 56 | 61 | 68 |*74*| 78 | 75 |  3 | 74 | 68 
_____-----------------------------------______________
 06 | 17 | 56 | 61 | 68 | 74 |*78*|*78*|  5 | 75 | 78 
_____-----------------------------------______________
 06 | 17 | 56 | 61 | 68 | 74 |*75*| 78 |  4 | 75 | 74 
_____-----------------------------------______________
10 islem yapildi.
Son D=[17 56 61 68 74 75 78]

	c. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[9 51 34 21 39 98 23 70 41 76] dizisini siralayiniz.

void f3_sort_selection(int arr[], int n)  
{  
    int i, j, k, m;
    for (i=0;i<n-1;i++) 
    {
        k=n-1;
        m=arr[k];
        for(j=i;j<n-1;j++)
            if(arr[j]<m)
            {
                k=j;
                m=arr[j];
            }
        arr[k]=arr[i];
        arr[i]=m;
    }
}

Baslangic D=[9 51 34 21 39 98 23 70 41 76]
_____--------------------------------------------------
  i | 00 | 01 | 02 | 03 | 04 | 05 | 06 | 07 | 08 | 09 |
_____--------------------------------------------------
 00 |* 9*| 51 | 34 | 21 | 39 | 98 | 23 | 70 | 41 | 76 |
_____--------------------------------------------------
 01 |  9 |*21*| 34 |*51*| 39 | 98 | 23 | 70 | 41 | 76 |
_____--------------------------------------------------
 02 |  9 | 21 |*23*| 51 | 39 | 98 |*34*| 70 | 41 | 76 |
_____--------------------------------------------------
 03 |  9 | 21 | 23 |*34*| 39 | 98 |*51*| 70 | 41 | 76 |
_____--------------------------------------------------
 04 |  9 | 21 | 23 | 34 |*39*| 98 | 51 | 70 | 41 | 76 |
_____--------------------------------------------------
 05 |  9 | 21 | 23 | 34 | 39 |*41*| 51 | 70 |*98*| 76 |
_____--------------------------------------------------
 06 |  9 | 21 | 23 | 34 | 39 | 41 |*51*| 70 | 98 | 76 |
_____--------------------------------------------------
 07 |  9 | 21 | 23 | 34 | 39 | 41 | 51 |*70*| 98 | 76 |
_____--------------------------------------------------
 08 |  9 | 21 | 23 | 34 | 39 | 41 | 51 | 70 |*76*|*98*|
_____--------------------------------------------------
9 islem yapildi.
Son D=[9 21 23 34 39 41 51 70 76 98]

	d. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[94 32 45 70 27 54] dizisini siralayiniz.

void f4_sort_bubble(int arr[], int n)  
{  
    int i, j;
    for (i=0;i<n-1;i++) 
        for(j=0;j<n-1-i;j++) 
            if(arr[j]>arr[j+1]) 
	        swap(arr,j,j+1);
}

Baslangic D=[94 32 45 70 27 54]
------------------------------_________
 00 | 01 | 02 | 03 | 04 | 05 | mov|  k 
------------------------------_________
*32*|*94*| 45 | 70 | 27 | 54 |  0 |  0 
------------------------------_________
 32 |*45*|*94*| 70 | 27 | 54 |  0 |  1 
------------------------------_________
 32 | 45 |*70*|*94*| 27 | 54 |  0 |  2 
------------------------------_________
 32 | 45 | 70 |*27*|*94*| 54 |  0 |  3 
------------------------------_________
 32 | 45 | 70 | 27 |*54*|*94*|  0 |  4 
------------------------------_________
 32 | 45 |*27*|*70*| 54 | 94 |  1 |  2 
------------------------------_________
 32 | 45 | 27 |*54*|*70*| 94 |  1 |  3 
------------------------------_________
 32 |*27*|*45*| 54 | 70 | 94 |  2 |  1 
------------------------------_________
*27*|*32*| 45 | 54 | 70 | 94 |  3 |  0 
------------------------------_________
9 islem yapildi.
Son D=[27 32 45 54 70 94]




