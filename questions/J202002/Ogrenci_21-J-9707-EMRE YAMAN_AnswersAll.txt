Sinav 5 soru ve toplam 100 puandan olusmakta olup, puan dagilimi su sekildedir:
[SoruNu-Puan]-->[0-10],[1-10],[2-10],[3-10],[4-60]

***Soru Nu[0]
Ogrenci[21-J-9707-EMRE YAMAN]
Puan: 10
--------->
Asagidaki sorularin cevaplarini yanlarina yaziniz. Hafta 04, Konu : Degiskenlerin tanimli oldugu araliklar.
  Eger integer bilgisayarda 7 bit ile ifade edilseydi
    a. -pow(2, 6)=-64
    b. pow(2, 6)-1=63
    c. 0
    d. pow(2, 7)=128
    e. [1 0 0 0 0 0 0]
    f. [0 1 1 1 1 1 1]
    g. [0 0 0 0 0 0 0]
    h. [1 1 1 1 1 1 1]



***Soru Nu[1]
Ogrenci[21-J-9707-EMRE YAMAN]
Puan: 10
--------->
Asagidaki programin hafizada kapladigi boyutu hesaplayiniz. Hafta 03, Konu : Veri yapilari ve hafiza.
      4 integers --> 4*2 bytes = 64 bits.
      11 chars --> 10 + 1 bytes = 88 bits.
      5 float --> 5*2 bytes = 80 bits.
      21 long --> 21*4 bytes = 672 bits.
      TOPLAM --> 113 bytes = 904 bits.



***Soru Nu[2]
Ogrenci[21-J-9707-EMRE YAMAN]
Puan: 10
--------->
 Konu : Genel Programlama Bilgisi

	BLOK_A - for öncesinde çalışır.
	BLOK_B - for bloğunun ilklendirici komutudur. Her zaman çalışır. Boş bırakılabilir.
	BLOK_C - for bloğunun sonlanacağı zamanı belirten koşul komutudur. Burasının ilk değeri "yanlış" ise döngü başlamaz. Boş bırakılması uygun değildir. Boş ise döngü içinde mutlaka break veya exit bulunmalıdır.
	BLOK_D - for bloğunun iterasyon komutudur. 
	BLOK_E - for bloğunun uygulama bloğudur. BLOK_E işlendikten sonra önce bu işlem gerçekleştirilir ardından BLOK_C'ye tekrar bakılır.
	BLOK_F - for sonrasında çalışır.
	
	sıralama - A,B,[C,E,D],F --> sırasıyla çalışır. D sonrası C'ye geri döner.



***Soru Nu[3]
Ogrenci[21-J-9707-EMRE YAMAN]
Puan: 10
--------->
- Hafta 04, Konu : turetilmis veri yapilari

	a. [86, ___, ___]
	b. Yığın - Stack
	c. 
		(1). push(16) - [86, 16, ___]
		(2). pop() [hangi sayı diziden çıkar = 16]
		(3). pop() [hangi sayı diziden çıkar = 86]
		(4). pop() [hangi sayı diziden çıkar = hiçbir sayı çıkmaz]
		(6). push(23) - [23, ___, ___]
		(7). push(89) - [23, 89, ___]
		(8). push(73) - [23, 89, 73]
	d. Queue
	e. 
		(1). dequeue() [hangi sayı diziden çıkar = 86]
		(2). dequeue() [hangi sayı diziden çıkar = hiçbir sayı çıkmaz]
		(3). enqueue(12) - [___, 12, ___]
		(4). enqueue(20) - [___, 12, 20]
		(5). enqueue(87) - [87, 12, 20]
		(6). dequeue() [hangi sayı diziden çıkar = 12]



***Soru Nu[4]
Ogrenci[21-J-9707-EMRE YAMAN]
Puan: 60
--------->
Soru seceneklerinde belirtilen fonksiyonlara bakarak her bir secenegi cozun. Konu : Siralama algoritmalari

Asagidaki seceneklerde belirtilen f1, f2, f3, f4 fonksiyonlara gore sorulari cozunuz.
	a. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[5 53 25 41 56 80 54] dizisini siralayiniz.

void f1_sort_insertion(int arr[], int n)  
{  
    int i, j, k;  
    for (i=1;i<n;i++) 
    {  
        k = arr[i];  
        for(j=i-1;j>=0 && arr[j]>k;j--) 
            arr[j+1]=arr[j];  
        arr[j+1]=k;
    }
}

Baslangic D=[5 53 25 41 56 80 54]
_____-----------------------------------______________
  i | 00 | 01 | 02 | 03 | 04 | 05 | 06 |  k |ekle|D(k)
_____-----------------------------------______________
 01 |  5 |*53*| 25 | 41 | 56 | 80 | 54 |  0 | 53 |  5 
_____-----------------------------------______________
 02 |  5 |*53*|*53*| 41 | 56 | 80 | 54 |  1 | 25 | 53 
_____-----------------------------------______________
 02 |  5 |*25*| 53 | 41 | 56 | 80 | 54 |  0 | 25 |  5 
_____-----------------------------------______________
 03 |  5 | 25 |*53*|*53*| 56 | 80 | 54 |  2 | 41 | 53 
_____-----------------------------------______________
 03 |  5 | 25 |*41*| 53 | 56 | 80 | 54 |  1 | 41 | 25 
_____-----------------------------------______________
 04 |  5 | 25 | 41 | 53 |*56*| 80 | 54 |  3 | 56 | 53 
_____-----------------------------------______________
 05 |  5 | 25 | 41 | 53 | 56 |*80*| 54 |  4 | 80 | 56 
_____-----------------------------------______________
 06 |  5 | 25 | 41 | 53 | 56 |*80*|*80*|  5 | 54 | 80 
_____-----------------------------------______________
 06 |  5 | 25 | 41 | 53 |*56*|*56*| 80 |  4 | 54 | 56 
_____-----------------------------------______________
 06 |  5 | 25 | 41 | 53 |*54*| 56 | 80 |  3 | 54 | 53 
_____-----------------------------------______________
10 islem yapildi.
Son D=[5 25 41 53 54 56 80]

	b. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[10 7 52 49 83 15 59 58 77 92] dizisini siralayiniz.

void f2_sort_bubble(int arr[], int n)  
{  
    int i, j;
    for (i=0;i<n-1;i++) 
        for(j=0;j<n-1-i;j++) 
            if(arr[j]>arr[j+1]) 
	        swap(arr,j,j+1);
}

Baslangic D=[10 7 52 49 83 15 59 58 77 92]
--------------------------------------------------_________
 00 | 01 | 02 | 03 | 04 | 05 | 06 | 07 | 08 | 09 | mov|  k 
--------------------------------------------------_________
* 7*|*10*| 52 | 49 | 83 | 15 | 59 | 58 | 77 | 92 |  0 |  0 
--------------------------------------------------_________
  7 | 10 |*49*|*52*| 83 | 15 | 59 | 58 | 77 | 92 |  0 |  2 
--------------------------------------------------_________
  7 | 10 | 49 | 52 |*15*|*83*| 59 | 58 | 77 | 92 |  0 |  4 
--------------------------------------------------_________
  7 | 10 | 49 | 52 | 15 |*59*|*83*| 58 | 77 | 92 |  0 |  5 
--------------------------------------------------_________
  7 | 10 | 49 | 52 | 15 | 59 |*58*|*83*| 77 | 92 |  0 |  6 
--------------------------------------------------_________
  7 | 10 | 49 | 52 | 15 | 59 | 58 |*77*|*83*| 92 |  0 |  7 
--------------------------------------------------_________
  7 | 10 | 49 |*15*|*52*| 59 | 58 | 77 | 83 | 92 |  1 |  3 
--------------------------------------------------_________
  7 | 10 | 49 | 15 | 52 |*58*|*59*| 77 | 83 | 92 |  1 |  5 
--------------------------------------------------_________
  7 | 10 |*15*|*49*| 52 | 58 | 59 | 77 | 83 | 92 |  2 |  2 
--------------------------------------------------_________
9 islem yapildi.
Son D=[7 10 15 49 52 58 59 77 83 92]

	c. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[41 20 46 80 48 7 92 21 33 88] dizisini siralayiniz.

void f3_sort_selection(int arr[], int n)  
{  
    int i, j, k, m;
    for (i=0;i<n-1;i++) 
    {
        k=n-1;
        m=arr[k];
        for(j=i;j<n-1;j++)
            if(arr[j]<m)
            {
                k=j;
                m=arr[j];
            }
        arr[k]=arr[i];
        arr[i]=m;
    }
}

Baslangic D=[41 20 46 80 48 7 92 21 33 88]
_____--------------------------------------------------
  i | 00 | 01 | 02 | 03 | 04 | 05 | 06 | 07 | 08 | 09 |
_____--------------------------------------------------
 00 |* 7*| 20 | 46 | 80 | 48 |*41*| 92 | 21 | 33 | 88 |
_____--------------------------------------------------
 01 |  7 |*20*| 46 | 80 | 48 | 41 | 92 | 21 | 33 | 88 |
_____--------------------------------------------------
 02 |  7 | 20 |*21*| 80 | 48 | 41 | 92 |*46*| 33 | 88 |
_____--------------------------------------------------
 03 |  7 | 20 | 21 |*33*| 48 | 41 | 92 | 46 |*80*| 88 |
_____--------------------------------------------------
 04 |  7 | 20 | 21 | 33 |*41*|*48*| 92 | 46 | 80 | 88 |
_____--------------------------------------------------
 05 |  7 | 20 | 21 | 33 | 41 |*46*| 92 |*48*| 80 | 88 |
_____--------------------------------------------------
 06 |  7 | 20 | 21 | 33 | 41 | 46 |*48*|*92*| 80 | 88 |
_____--------------------------------------------------
 07 |  7 | 20 | 21 | 33 | 41 | 46 | 48 |*80*|*92*| 88 |
_____--------------------------------------------------
 08 |  7 | 20 | 21 | 33 | 41 | 46 | 48 | 80 |*88*|*92*|
_____--------------------------------------------------
9 islem yapildi.
Son D=[7 20 21 33 41 46 48 80 88 92]

	d. Asagida verilen fonksiyon hangi siralama algoritmasinin kodudur? Bu fonksiyona gore D=[10 14 15 12 13 9 8 11] dizisini siralayiniz.

void f4_sort_merge(int arr[], int i, int n)  
{  
    if(n>1) 
    {
        j=n/2;
        ms(arr,i,j);
        ms(arr,j,n-j);
        m(D,i,j,n);
    }
}

8 9 10 11 12 13 14 15



