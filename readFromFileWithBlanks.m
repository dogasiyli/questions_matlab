function strFile = readFromFileWithBlanks(fileName)
    strFile = '';
    fid = fopen(fileName,'r','n','UTF-8');
    tline = fgetl(fid);
    while ischar(tline)
        strFile = [strFile newLineFunc() tline];
        tline = fgetl(fid);
    end    
    strFile = [strFile newLineFunc()];
    fclose(fid);  
end