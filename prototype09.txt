<x> degişkeninin her bulundugu satirda {g,l,s} harflerinden hangisi/hangileri dogru ise yuvarlak içine aliniz. Örnegin global olan bir degişken için g harfini, static ve lokal olan bir degişken için ise hem s hem de l harflerini yuvarlak içine aliniz.
#include <stdio.h>
#include <stdlib.h>

//g - global
//l - lokal
//s - static

int x = X01;//  g  -  l  -  s
void f01(int x, int y){//  g  -  l  -  s
	printf("f01(%d)/n",M01*x);//  g  -  l  -  s
	x=y;//  g  -  l  -  s
}
void f02(int y){
	static int x=X02;//  g  -  l  -  s
	printf("f02_s(%d)/n",M02*x);//  g  -  l  -  s
	x=y;//  g  -  l  -  s
}
void f03(int y){
	int x=y;//  g  -  l  -  s
	printf("f03(%d)/n",M03*x);//  g  -  l  -  s
}
void f04(int y){
	printf("f04(%d)/n",M04*x);//  g  -  l  -  s
	x=y;//  g  -  l  -  s
}
void f05(int y){
	printf("f05_g(%d)/n",M05*x);//  g  -  l  -  s
	x=y;//  g  -  l  -  s
	int x = M06*y;//  g  -  l  -  s
	printf("f05_l(%d)/n",M07*x);//  g  -  l  -  s
}
int main(int argc, char *argv[]) {
	F01_CALL_01
	F_RAND_CALL_01_01
	F_RAND_CALL_01_02
	F_RAND_CALL_01_03

	printf("main_g(%d)/n",x);//  g  -  l  -  s

	int x = X03;//  g  -  l  -  s

	F01_CALL_02

	printf("main_l(%d)/n",x);//  g  -  l  -  s

	F_RAND_CALL_02_01
	F_RAND_CALL_02_02
	F_RAND_CALL_02_03

	F05_Call
	return 0;
}
