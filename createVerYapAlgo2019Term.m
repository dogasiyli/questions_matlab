numOfQuestionIDs = 18;

nameNumberCells = [];
load('namesOfStudents_2019_J.mat')

numOfSamples = zeros(1,numOfQuestionIDs);
soruPuan = zeros(1,numOfQuestionIDs);

numOfSamples([1 4 6 11 17 18]) = 1;
soruPuan([1 4 6 11 17 18]) = [10 20 15 15 20 20];
questionOrder = [11 6 4 17 18 1];


createPapers( nameNumberCells, soruPuan, numOfSamples, questionOrder);