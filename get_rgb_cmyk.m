function [rgb_vec, cmyk_vec, color_name] = get_rgb_cmyk(selection_idx)
    if selection_idx==1
        rgb_vec = [0 0 0];
        cmyk_vec = [0 0 0 1];
        color_name = 'Siyah';
    elseif selection_idx==2
        rgb_vec = [255 255 255];
        cmyk_vec = [0 0 0 0];
        color_name = 'Beyaz';
    elseif selection_idx==3
        rgb_vec = [255 0 0];
        cmyk_vec = [0 1 1 0];
        color_name = 'Kırmızı';
    elseif selection_idx==4
        rgb_vec = [0 255 0];
        cmyk_vec = [1 0 1 0];
        color_name = 'Yeşil';
    elseif selection_idx==5
        rgb_vec = [0 0 255];
        cmyk_vec = [1 1 0 0];
        color_name = 'Mavi';
    elseif selection_idx==6
        rgb_vec = [255 255 0];
        cmyk_vec = [0 0 1 0];
        color_name = 'Sarı';
    elseif selection_idx==7
        rgb_vec = [0 255 255];
        cmyk_vec = [1 0 0 0];
        color_name = 'Camgöbeği';
    elseif selection_idx==8
        rgb_vec = [255 0 255];
        cmyk_vec = [0 1 0 0];
        color_name = 'Macenta';
    else
        error('wrong choice')
    end
end