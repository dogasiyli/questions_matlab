function i_m = sub2ind_mine(L,M,a,b,c)
    i_m = a*M*L + b*M + c;
end
