function [funcCells] = createLoop( loopTypes, i_lastValue, incVal, useExpVec, tabCnt)
    [expCells, expCount] = createExpressionCells(incVal);
    loopCount = length(loopTypes);
    funcCells = cell(1,loopCount);
    biglesschar = '>';%decrease normally
    biglesschar_r = '<';%decrease normally
    if incVal>0%if increasing
        biglesschar = '<';
        biglesschar_r = '>';%decrease normally
    end
    expCells = expCells(useExpVec,:);
    for i=1:loopCount
        switch loopTypes(i)
            case 0 %recursion
                funcName = 'foo_recursion';
                firstLine = getStringWithTab(['if (i' biglesschar_r '=' num2str(i_lastValue) ')'], tabCnt+1);
                secondLine = getStringWithTab('return;', tabCnt+2);
                lastLine = '';
                ifGroupString = createIfLoop(expCells, tabCnt+1, incVal, 'foo_recursion');
            case 1 %do-while
                funcName = 'foo_dowhile';
                firstLine = getStringWithTab('do', tabCnt+1);
                secondLine = getStringWithTab('{', tabCnt+1);
                lastLine = getStringWithTab(['}while(i' biglesschar num2str(i_lastValue) ');'], tabCnt+1);
                ifGroupString = createIfLoop(expCells, tabCnt+2, incVal, '');
            case 2 %while do
                funcName = 'foo_whiledo';
                firstLine = getStringWithTab(['while(i' biglesschar num2str(i_lastValue) ')'], tabCnt+1);
                secondLine = getStringWithTab('{', tabCnt+1);
                lastLine = getStringWithTab('}', tabCnt+1);
                ifGroupString = createIfLoop(expCells, tabCnt+2, incVal, '');
            otherwise %for-loop
                funcName = 'foo_forloop';
                firstLine = getStringWithTab(['for(;i' biglesschar num2str(i_lastValue) ';)'], tabCnt+1);
                secondLine = getStringWithTab('{', tabCnt+1);
                lastLine = getStringWithTab('}', tabCnt+1);
                ifGroupString = createIfLoop(expCells, tabCnt+2, incVal, '');
        end
        s = getStringWithTab(['void ' funcName '(int i)'], tabCnt);
        s = [s newLineFunc() getStringWithTab('{', tabCnt)];
        s = [s newLineFunc() firstLine];
        s = [s newLineFunc() secondLine];
        s = [s newLineFunc() ifGroupString];
        s = [s newLineFunc() lastLine];
        s = [s newLineFunc() getStringWithTab('}', tabCnt)];
        funcCells{i} = s;
        disp(['function for <' funcName '> : ']);
        fprintf(s);
    end
end

function s = createIfLoop(expCells, tabCnt, incVal, recursionFuncName)
    expCount = size(expCells, 1);
    s = '';
    for i=1:expCount
        if i==1
            s = [s newLineFunc() getStringWithTab(['if (' expCells{i,1} ') '], tabCnt)];
        else
            s = [s newLineFunc() getStringWithTab(['else if (' expCells{i,1} ') '], tabCnt)];
        end
        s = [s newLineFunc() getStringWithTab('{',tabCnt)];
        if (~strcmp(expCells{i,2},''))
            s = [s newLineFunc() getStringWithTab(expCells{i,2}, tabCnt+1)];
        end
        if (~strcmp(expCells{i,3},''))
            if ~strcmp(recursionFuncName,'')
                %this is recursion
                s = [s newLineFunc() getStringWithTab([recursionFuncName '(' expCells{i,3} ');'], tabCnt+1)];
            else
                %this is not recursion
                s = [s newLineFunc() getStringWithTab(['i=' expCells{i,3} ';'], tabCnt+1)];
            end
        end
        s = [s newLineFunc() getStringWithTab('}',tabCnt)];
    end
    s = [s newLineFunc() getStringWithTab('else', tabCnt)];
    s = [s newLineFunc() getStringWithTab('{',tabCnt)];
    plusMinusChar = '+';%+ not shown normally
    if incVal<0%if increasing
        plusMinusChar = '';%- already shown
    end
    if ~strcmp(recursionFuncName,'')
        %this is recursion
        s = [s newLineFunc() getStringWithTab([recursionFuncName '(i' plusMinusChar num2str(incVal) ');'], tabCnt+1)];
    else
        %this is not recursion
        s = [s newLineFunc() getStringWithTab(['i=i' plusMinusChar num2str(incVal) ';'], tabCnt+1)];
    end    
    s = [s newLineFunc() getStringWithTab('}',tabCnt)];    
end

function [expCells, expCount] = createExpressionCells(incVal)
    vals = [2 3 5 7 11 13 17];
    expCount = length(vals);
    expCells = cell(expCount,3);
    for i=1:expCount
        if incVal<0
            expCells{i,1} = ['i%%' num2str(vals(i)) '==0'];
            expCells{i,2} = 'cout << i << endl;';
            expCells{i,3} = ['i/' num2str(vals(i))];
        else
            expCells{i,1} = ['i%%' num2str(vals(expCount-i+1)) '==0'];
            expCells{i,2} = 'cout << i << endl;';
            if i<expCount
                expCells{i,3} = ['i*' num2str(vals(expCount-i))];
            else
                expCells{i,3} = 'i+1';
            end
        end        
    end
end

% recursion
% void fooRecursive(int i)
% {
% 	if (i==0)
% 		return;
% 	if (i%2==0)
% 	{
% 		cout << i << endl;
% 		fooRecursive(i/2);
% 	}
% 	else if (i%3==0)
% 	{
% 		cout << i << endl;
% 		fooRecursive(i/3);
% 	}
% 	else
% 	{
% 		fooRecursive(i-1);
% 	}
% % }
% 
%   a. for
%     void fooRecursive(int i)
%     {
%       for(;i>0;)
%       {
%         if(i%2==0)
%         {
%           cout << i << endl;
%           i = i/2;
%         }
%         else if(i%3==0)
%         {
%           cout << i << endl;
%           i = i/3;
%         }
%         else
%           i--;
%       }
%     }
% 
% 
% 
%   b. while-do
%     void fooRecursive(int i)
%     {
%       while(i>0)
%       {
%         if(i%2==0)
%         {
%           cout << i << endl;
%           i = i/2;
%         }
%         else if(i%3==0)
%         {
%           cout << i << endl;
%           i = i/3;
%         }
%         else
%           i--;
%       }
%     }
% 
% 
% 
%   c. do-while
%     void fooRecursive(int i)
%     {
%       do
%       {
%         if(i%2==0)
%         {
%           cout << i << endl;
%           i = i/2;
%         }
%         else if(i%3==0)
%         {
%           cout << i << endl;
%           i = i/3;
%         }
%         else
%           i--;
%       }while(i>0);
%     }
% 
% 
