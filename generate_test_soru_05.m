function [soru_05, correct_selection_id] = generate_test_soru_05(base_soru, ogrenci_str, selection_idx, verbose)
% % SoruTip005Begin
% \section{Soru-X:}
% Konu: Renk Uzayları \\
% Puan: SoruTip005Puan \\
% Soru:$[R,G,B]$ rengi CMYK uzayında hangi renge eş gelir?
% \begin{enumerate}[label=\alph*)]
%     \item Select-A
%     \item Select-B
%     \item Select-C
%     \item Select-D
%     \item Select-E
% \end{enumerate}
% % SoruTip005End
%   base_soru = soru_base{5}
    soru_id = 5;
    rng(selection_idx)
    [rgb_vec, cmyk_vec, color_name] = get_rgb_cmyk(selection_idx);
    % Soru:$[X_1,Y_1]$ noktasından $[X_2,Y_2]$ noktasına çizgi çizildiğinde
    %  $X ve Y$ düzlemlerinde kullanılacak artış miktarları hangi seçenekle sırayla doğru olarak verilmiştir
    soru_05 = strrep(base_soru, '{Soru-X:}',['{' ogrenci_str '-Soru-' num2str(soru_id, '%d')  '-Seed(' num2str(selection_idx)  '):}']);
    soru_05 = strrep(soru_05, '$[R,G,B]$', ['$[R=' num2str(rgb_vec(1),'%d') ',G=' num2str(rgb_vec(2),'%d') ',B=' num2str(rgb_vec(3),'%d') ']$ (' color_name ')']); 
    
    [answers_cell, correct_selection_id] = latex_test_generate_answer_05(cmyk_vec, randi(50));
    
    for i=1:5
        old_str = ['Select-' char(('A')+i-1)];
        new_str = answers_cell{i};
        soru_05 = strrep(soru_05, old_str, new_str);
    end 
    if verbose>0
        disp(['soru_05 : \n' soru_05])
        disp(['cevap : ' char(('A')+correct_selection_id-1)])
        disp(['cevap : ' color_name ', rgb_vec(' mat2str(rgb_vec) '), cmyk_vec(' mat2str(cmyk_vec) ')' ])
    end
end

function [answers_cell, correct_selection_id] = latex_test_generate_answer_05(cmyk_vec, seed_val)
    rng(seed_val)
    answers_cell = cell(1,5);
    
    correct_answer = 1+bin2dec(strrep(int2str(cmyk_vec), ' ', ''));
    possible_vec = 1:16;
    possible_vec(correct_answer) = [];
    possible_vec = possible_vec(randperm(15));
    possible_vec = [correct_answer possible_vec(1:4)];
    possible_vec = possible_vec(randperm(5));  
    correct_selection_id = find(possible_vec==correct_answer,1);

    for i=1:5
        ans_ca = dec2bin(possible_vec(i)-1);
        while length(ans_ca)<4
            ans_ca = ['0' ans_ca];
        end
        answers_cell{i} = ['C(' ans_ca(1) '),M(' ans_ca(2) '),Y(' ans_ca(3) '),K(' ans_ca(4) ')'];
    end 

    assert(sum(possible_vec(correct_selection_id)==correct_answer)~=0, 'problem here')
end

