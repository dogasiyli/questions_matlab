function [soru_02, correct_selection_id] = generate_test_soru_02(base_soru, ogrenci_str, seed_val, verbose)
%   base_soru = soru_base{2}
    soru_id = 2;
    rng(seed_val)
    minDif = 4; maxDif = 8; minPoint=5; 
    ret_struct = generate_line(minDif, maxDif, minPoint, randi(50), verbose);
    % Soru:$[X_1,Y_1]$ noktasından $[X_2,Y_2]$ noktasına çizgi çizildiğinde
    % hangi noktadan noktasından geçmez?
    soru_02 = strrep(base_soru, '{Soru-X:}',['{' ogrenci_str '-Soru-' num2str(soru_id, '%d')  '-Seed(' num2str(seed_val)  '):}']);
    soru_02 = strrep(soru_02, '$[X_1,Y_1]$', ['$[X=' num2str(ret_struct.beg(1),'%d') ',Y=' num2str(ret_struct.beg(2),'%d') ']$']); 
    soru_02 = strrep(soru_02, '$[X_2,Y_2]$', ['$[X=' num2str(ret_struct.end(1),'%d') ',Y=' num2str(ret_struct.end(2),'%d') ']$']); 
    
    [ answers_cell, correct_selection_id ] = latex_test_generate_answer_02( ret_struct, randi(50), verbose);
    
    for i=1:5
        old_str = ['Select-' char(('A')+i-1)];
        new_str = answers_cell{i};
        soru_02 = strrep(soru_02, old_str, new_str);
    end 
    if verbose>0
        disp(['soru_02 : \n' soru_02])
        disp(['cevap : ' char(('A')+correct_selection_id-1)])
    end
end

function [ answers_cell, correct_selection_id ] = latex_test_generate_answer_02(ret_struct, seed_val, verbose)
%latex_test_generate_answers Correct answer is an integer
%   according to dif inc this function will generate 5 possible answers
%   to which one answer is correct
    rng(seed_val)
    possible_couple_count = length(ret_struct.xVec)-2; %remove first and last
    select_correct_inds = 1 + randperm(possible_couple_count);
    correct_selection_id = randi(5);    
    answers_cell = cell(1,5);
    for i = 1:5
        si = select_correct_inds(i);
        xi = ret_struct.xVec(si);
        yi = ret_struct.yVec(si);
        if i==correct_selection_id
            answers_cell{i} = ['X=' num2str(xi-1) ',Y=' num2str(yi+1)];
        else
            answers_cell{i} = ['X=' num2str(xi) ',Y=' num2str(yi)];
        end
    end    
       
    if verbose>0
        disp(['xy_couples : ' newLineFunc() mat2str([ret_struct.xVec;ret_struct.yVec])])
        disp(['answers_vec : ' answers_cell])
        disp(['ans_correct : ' mat2str(answers_cell{correct_selection_id})])
        disp(['correct_selection_id : ' num2str(correct_selection_id)])
    end
    
    all_poss_answers = 100*ret_struct.xVec + ret_struct.yVec;
    s_slct = select_correct_inds(correct_selection_id);
    not_to_be_found = 100*ret_struct.xVec(s_slct) + ret_struct.yVec(s_slct);
    assert(sum(all_poss_answers==not_to_be_found)~=0, 'problem here')

end

