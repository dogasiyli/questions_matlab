function [soru_03, correct_selection_id] = generate_test_soru_03(base_soru, ogrenci_str, seed_val, verbose)
% % SoruTip003Begin
% \section{Soru-X:}
% Konu: Çizgi Çizmek \\
% Puan: SoruTip003Puan \\
% Soru:$[X_1,Y_1]$ noktasından $[X_2,Y_2]$ noktasına çizgi çizildiğinde
% $X ve Y$ düzlemlerinde kullanılacak artış miktarları hangi seçenekle sırayla doğru olarak verilmiştir?
% \begin{enumerate}[label=\alph*)]
%     \item Select-A
%     \item Select-B
%     \item Select-C
%     \item Select-D
%     \item Select-E
% \end{enumerate}
% % SoruTip003End
%   base_soru = soru_base{3}
    soru_id = 3;
    minDif = 4; maxDif = 8; minPoint=5; 
    rng(seed_val)
    ret_struct = generate_line(minDif, maxDif, minPoint, randi(50), verbose);
    % Soru:$[X_1,Y_1]$ noktasından $[X_2,Y_2]$ noktasına çizgi çizildiğinde
    %  $X ve Y$ düzlemlerinde kullanılacak artış miktarları hangi seçenekle sırayla doğru olarak verilmiştir
    soru_03 = strrep(base_soru, '{Soru-X:}',['{' ogrenci_str '-Soru-' num2str(soru_id, '%d')  '-Seed(' num2str(seed_val)  '):}']);
    soru_03 = strrep(soru_03, '$[X_1,Y_1]$', ['$[X=' num2str(ret_struct.beg(1),'%d') ',Y=' num2str(ret_struct.beg(2),'%d') ']$']); 
    soru_03 = strrep(soru_03, '$[X_2,Y_2]$', ['$[X=' num2str(ret_struct.end(1),'%d') ',Y=' num2str(ret_struct.end(2),'%d') ']$']); 

    [ answers_cell, correct_selection_id ] = latex_test_generate_answer_03( ret_struct, randi(50), verbose);
    
    for i=1:5
        old_str = ['Select-' char(('A')+i-1)];
        new_str = answers_cell{i};
        soru_03 = strrep(soru_03, old_str, new_str);
    end 
    if verbose>0
        disp(['soru_03 : \n' soru_03])
        disp(['cevap : ' char(('A')+correct_selection_id-1)])
    end
end

function [ answers_cell, correct_selection_id ] = latex_test_generate_answer_03(ret_struct, seed_val, verbose)
    rng(seed_val)
    answers_cell = cell(1,5);
    answers_cell{1} = ['xInc=' num2str(ret_struct.inc(1),'%4.2f') ',yInc=' num2str(ret_struct.inc(2),'%4.2f')];
    answers_cell{2} = ['xInc=' num2str(ret_struct.inc(2),'%4.2f') ',yInc=' num2str(ret_struct.inc(1),'%4.2f')];
    xInc_wrong = (ret_struct.dif(1)-1)/(ret_struct.longer_dif-1);
    yInc_wrong = (ret_struct.dif(2)-1)/(ret_struct.longer_dif-1);
    answers_cell{3} = ['xInc=' num2str(xInc_wrong,'%4.2f') ',yInc=' num2str(yInc_wrong,'%4.2f')];
    answers_cell{4} = ['xInc=' num2str(yInc_wrong,'%4.2f') ',yInc=' num2str(xInc_wrong,'%4.2f')];
    answers_cell{5} = ['xInc=' num2str(ret_struct.dif(1),'%4.2f') ',yInc=' num2str(ret_struct.dif(2),'%4.2f')];
    shuffle_vec = randperm(5);
    answers_cell = answers_cell(shuffle_vec);
    correct_selection_id = find(shuffle_vec==1,1);
    if verbose>0
        disp(['xInc=' num2str(ret_struct.inc(1),'%4.2f') ',yInc=' num2str(ret_struct.inc(2),'%4.2f')])
    end
    assert(sum(shuffle_vec(correct_selection_id)==1)~=0, 'problem here')
end

