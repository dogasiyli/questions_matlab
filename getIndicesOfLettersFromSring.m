function X = getIndicesOfLettersFromSring(strToDisp, letters)
    stringLentgh = length(strToDisp);
    X = NaN(1,stringLentgh);
    for i=1:stringLentgh
        X(i) = find(strToDisp(i)==letters);
    end
end

