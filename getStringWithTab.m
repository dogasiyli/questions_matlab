function str = getStringWithTab(str, tabCnt)
    str = [repmat('  ', 1, tabCnt) str];
end