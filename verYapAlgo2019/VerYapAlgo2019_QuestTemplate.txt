Veri Yapıları ve Algoritmalar Ara Sınavı - 11 Nisan 2019


Soru 1 : [15 puan]
- Bir struct yarat sorusu

Soru 2 : [15 puan]
[Prototype-5, soruID-5]
- Recursive fonksiyon okut.

Soru 3 : [20 puan] 
[Prototype-3, soruID-3]
- Bitlere göre üst alt limit sorusu

Soru 4 : [20 puan] 
- Pointer'lı karışık kod yaz ve okut
  - Memory'de verinin tutulmasını da düşünerek işlem yaptır
  
Soru 5 : [20 puan]
- Push, pop, enqueue, dequeue
  - kod yaz çıktı sor

Soru 6 : [10 puan]
- döngüler ve çıkış komutları [soruID-0]

