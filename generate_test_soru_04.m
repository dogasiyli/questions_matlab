function [soru_04, correct_selection_id] = generate_test_soru_04(base_soru, ogrenci_str, seed_val, verbose)
% % SoruTip004Begin
% \section{Soru-X:}
% Konu: Çizgi Çizmek \\
% Puan: SoruTip004Puan \\
% Soru:$[X_1,Y_1]$ noktasından $[X_2,Y_2]$ noktasına çizgi çizildiğinde
% %X=5 nostası Y düzleminde veya Y=3 noktası X düzleminde
% $UZUNKENARVER$ noktası $DIGERDUZLEMADI$ düzleminde 
% hangi noktadan geçer?
% \begin{enumerate}[label=\alph*)]
%     \item Select-A
%     \item Select-B
%     \item Select-C
%     \item Select-D
%     \item Select-E
% \end{enumerate}
% % SoruTip004End
%   base_soru = soru_base{4}
    soru_id = 4;
    minDif = 4; maxDif = 8; minPoint=5; 
    rng(seed_val)
    ret_struct = generate_line(minDif, maxDif, minPoint, randi(50), verbose);
    % Soru:$[X_1,Y_1]$ noktasından $[X_2,Y_2]$ noktasına çizgi çizildiğinde
    %  $X ve Y$ düzlemlerinde kullanılacak artış miktarları hangi seçenekle sırayla doğru olarak verilmiştir
    soru_04 = strrep(base_soru, '{Soru-X:}',['{' ogrenci_str '-Soru-' num2str(soru_id, '%d')  '-Seed(' num2str(seed_val)  '):}']);
    soru_04 = strrep(soru_04, '$[X_1,Y_1]$', ['$[X=' num2str(ret_struct.beg(1),'%d') ',Y=' num2str(ret_struct.beg(2),'%d') ']$']); 
    soru_04 = strrep(soru_04, '$[X_2,Y_2]$', ['$[X=' num2str(ret_struct.end(1),'%d') ',Y=' num2str(ret_struct.end(2),'%d') ']$']); 
    
    [answers_cell, correct_selection_id, uzunkenarver_str_new, diger_str_new] = latex_test_generate_answer_04( ret_struct, randi(50), verbose);
    soru_04 = strrep(soru_04, 'UZUNKENARVER', uzunkenarver_str_new);
    soru_04 = strrep(soru_04, 'DIGERDUZLEMADI', diger_str_new);
    
    for i=1:5
        old_str = ['Select-' char(('A')+i-1)];
        new_str = answers_cell{i};
        soru_04 = strrep(soru_04, old_str, new_str);
    end 
    if verbose>0
        disp(['soru_04 : \n' soru_04])
        disp(['cevap : ' char(('A')+correct_selection_id-1)])
    end
end

function [answers_cell, correct_selection_id, uzunkenarver_str_new, diger_str_new] = latex_test_generate_answer_04(ret_struct, seed_val, verbose)
    rng(seed_val)
    answers_cell = cell(1,5);
    
    possible_couple_count = length(ret_struct.xVec)-2; %remove first and last
    selected_inds = 1 + randi(possible_couple_count-2);
    %şimdi selected_indsteki couple'dan soru soracağım
        
    if ret_struct.longer_edge=='X'
        uzunkenarver_str_new = ['X=' num2str(ret_struct.xVec(selected_inds))];
        diger_str_new = 'Y';
        correct_answer = ret_struct.yVec(selected_inds);
        possible_answers = ret_struct.beg(2):ret_struct.end(2);
        ans_start = max(1, find(possible_answers==correct_answer, 1) - randi(2)); %start other selections 0 1 or 2 before
        ans_start = min(ans_start, length(possible_answers)-4);
        possible_answers = possible_answers(ans_start:ans_start+4);
    else
        uzunkenarver_str_new = ['Y=' num2str(ret_struct.yVec(selected_inds))];
        diger_str_new = 'X';
        correct_answer = ret_struct.xVec(selected_inds);
        possible_answers = ret_struct.beg(1):ret_struct.end(1);
        ans_start = max(1, find(possible_answers==correct_answer, 1) - randi(2)); %start other selections 0 1 or 2 before
        ans_start = min(ans_start, length(possible_answers)-4);
        possible_answers = possible_answers(ans_start:ans_start+4);
    end    
    
    possible_answers = possible_answers(randperm(length(possible_answers)));
    correct_selection_id = find(possible_answers==correct_answer,1);
    if isempty(correct_selection_id)
        correct_selection_id = randi(5);
        possible_answers(correct_selection_id) = correct_answer;
    end
    
    for i=1:5
        answers_cell{i} = num2str(possible_answers(i));
    end 

    assert(sum(possible_answers(correct_selection_id)==correct_answer)~=0, 'problem here')
    assert(length(unique(possible_answers))==5, 'problem here')
end

