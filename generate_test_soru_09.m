function [soru_09, correct_selection_id] = generate_test_soru_09(base_soru, ogrenci_str, seed_val, verbose)
% % SoruTip009Begin
% \section{Soru-X:}
% Konu: Renk Uzayları \\
% Puan: SoruTip009Puan \\
% Soru:$[R,G,B]$ renginde her renk XBITX bit ile ifade edilseydi kaç farklı tonda renk üretilebilirdi?
% \begin{enumerate}[label=\alph*)]
%     \item Select-A
%     \item Select-B
%     \item Select-C
%     \item Select-D
%     \item Select-E
% \end{enumerate}
% % SoruTip009End
%   base_soru = soru_base{9}
    soru_id = 9;
    rng(seed_val)
    XBITX = 3 + randi(3);
    soru_09 = strrep(base_soru, '{Soru-X:}',['{' ogrenci_str '-Soru-' num2str(soru_id, '%d')  '-Seed(' num2str(seed_val)  '):}']);
    soru_09 = strrep(soru_09, 'XBITX', num2str(XBITX)); 
    
    [answers_cell, correct_selection_id] = latex_test_generate_answer_09(XBITX, randi(50));
    
    for i=1:5
        old_str = ['Select-' char(('A')+i-1)];
        new_str = answers_cell{i};
        soru_09 = strrep(soru_09, old_str, new_str);
    end 
    if verbose>0
        disp(['soru_09 : \n' soru_09])
        disp(['cevap : ' char(('A')+correct_selection_id-1)])
        disp(['cevap : ' num2str(XBITX) ' bit ile ' answers_cell{correct_selection_id} ' tane ton elde edilir'])
    end
end

function [answers_cell, correct_selection_id] = latex_test_generate_answer_09(XBITX, seed_val)
    rng(seed_val)
    answers_cell = cell(1,5);
    
    values_vec = [power(2, XBITX)*3 power(3, XBITX)*2 3*XBITX*2 XBITX*2 power(2, XBITX)*2];
    for i=1:5
        answers_cell{i} = num2str(values_vec(i));
    end
    shuffle_vec = randperm(5);
    answers_cell = answers_cell(shuffle_vec);
    correct_selection_id = find(shuffle_vec==1,1);
    
    assert(sum(shuffle_vec(correct_selection_id)==1)~=0, 'problem here')
    assert(length(unique(values_vec))==5, 'problem here')
end
