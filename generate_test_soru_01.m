function [soru_01, correct_selection_id] = generate_test_soru_01(base_soru, ogrenci_str, seed_val, verbose)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
%   base_soru = soru_base{1}
    soru_id = 1;
    minDif = 4; maxDif = 8; minPoint=5; 
    rng(seed_val)
    ret_struct = generate_line(minDif, maxDif, minPoint, randi(50), verbose);
    % Soru:$[X_1,Y_1]$ noktasından $[X_2,Y_2]$ noktasına çizgi çizildiğinde
    soru_01 = strrep(base_soru, '{Soru-X:}',['{' ogrenci_str '-Soru-' num2str(soru_id, '%d')  '-Seed(' num2str(seed_val)  '):}']);
    soru_01 = strrep(soru_01, '$[X_1,Y_1]$', ['$[X=' num2str(ret_struct.beg(1),'%d') ',Y=' num2str(ret_struct.beg(2),'%d') ']$']); 
    soru_01 = strrep(soru_01, '$[X_2,Y_2]$', ['$[X=' num2str(ret_struct.end(1),'%d') ',Y=' num2str(ret_struct.end(2),'%d') ']$']); 
    ans_correct = ret_struct.longer_dif + 1;
    must_have_as_false = unique([min(ret_struct.dif) min(ret_struct.beg)]);
    ans_min = min([must_have_as_false ret_struct.beg])-1;
    ans_max = max([ret_struct.end]); 
    [ answers_vec, correct_selection_id ] = latex_test_generate_answer_01( ans_correct, must_have_as_false, ans_min, ans_max, randi(50), verbose);
    for i=1:5
        old_str = ['Select-' char(('A')+i-1)];
        new_str = num2str(answers_vec(i),'%d');
        soru_01 = strrep(soru_01, old_str, new_str);
    end 
    if verbose>0
        disp(['soru_01 : \n' soru_01])
        disp(['cevap : ' char(('A')+correct_selection_id-1)])
    end
end

function [ answers_vec, correct_selection_id ] = latex_test_generate_answer_01( ans_correct, must_have_as_false, ans_min, ans_max, seed_val, verbose)
%latex_test_generate_answers Correct answer is an integer
%   according to dif inc this function will generate 5 possible answers
%   to which one answer is correct
    
    rng(seed_val)
    generate_answer_count = 4;
    %start the answer pool
    others_vec = ans_min:ans_max;
    %remove correct answer
    others_vec(others_vec==ans_correct) = [];
    %remove must_have_as_false answers
    for i = 1:length(must_have_as_false)
        have_ans = must_have_as_false(i);
        others_vec(others_vec==have_ans) = [];
        generate_answer_count = generate_answer_count - 1;
    end
    %shuffle answers
    others_vec = others_vec(randperm(length(others_vec)));
    %pick wrong answers
    others_vec = others_vec (1:generate_answer_count);
    
    correct_selection_id = randi(5);
    
    answers_vec = zeros(1,5);
    for i = 1:length(must_have_as_false)
        have_ans = must_have_as_false(i);
        answers_vec(i) = have_ans;
    end    
    
    answers_vec(length(must_have_as_false)+1:4) = others_vec;
    answers_vec(5) = ans_correct;
    
    t = answers_vec(correct_selection_id);
    answers_vec(correct_selection_id) = ans_correct;
    answers_vec(5) = t;
    
    if verbose>0
        disp(['answers_vec : ' mat2str(answers_vec)])
        disp(['ans_correct : ' num2str(ans_correct)])
        disp(['correct_selection_id : ' num2str(correct_selection_id)])
    end
    
    assert(answers_vec(correct_selection_id)==ans_correct, 'problem here')

end

