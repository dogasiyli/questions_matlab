function [soru_08, correct_selection_id] = generate_test_soru_08(base_soru, ogrenci_str, seed_val, verbose)
% % SoruTip008Begin
% \section{Soru-X:}
% Konu: Renk Uzayları \\
% Puan: SoruTip008Puan \\
% Soru:$[R,G,B]$ renginin 24 bitlik düzendeki yazılışı hangisidir?
% \begin{enumerate}[label=\alph*)]
%     \item Select-A
%     \item Select-B
%     \item Select-C
%     \item Select-D
%     \item Select-E
% \end{enumerate}
% % SoruTip008End
%   base_soru = soru_base{8}
    soru_id = 8;
    rng(seed_val)
    selection_idx = randi(8);
    [~, ~, color_name] = get_rgb_cmyk(selection_idx);
    soru_08 = strrep(base_soru, '{Soru-X:}',['{' ogrenci_str '-Soru-' num2str(soru_id, '%d')  '-Seed(' num2str(seed_val)  '):}']);
    soru_08 = strrep(soru_08, '$[R,G,B]$', color_name); 
    
    [answers_cell, correct_selection_id] = latex_test_generate_answer_08(selection_idx, randi(50));
    
    for i=1:5
        old_str = ['Select-' char(('A')+i-1)];
        new_str = answers_cell{i};
        soru_08 = strrep(soru_08, old_str, new_str);
    end 
    if verbose>0
        disp(['soru_08 : \n' soru_08])
        disp(['cevap : ' char(('A')+correct_selection_id-1)])
        disp(['cevap : ' color_name ', binary(' answers_cell{correct_selection_id} ')'])
    end
end

function [answers_cell, correct_selection_id] = latex_test_generate_answer_08(selection_idx, seed_val)
    rng(seed_val)
    answers_cell = cell(1,5);

    correct_answer = selection_idx;
    possible_vec = 1:8;
    possible_vec(correct_answer) = [];
    possible_vec = possible_vec(randperm(4));
    possible_vec = [correct_answer possible_vec(1:4)];
    possible_vec = possible_vec(randperm(5));  
    correct_selection_id = find(possible_vec==correct_answer,1);
    
    [rgb_vec, ~, ~] = get_rgb_cmyk(correct_answer);
    correct_answer = get_binary_from_rgb(rgb_vec);
    for i=1:5
        %take color id
        color_id = possible_vec(i);
        [rgb_vec, ~, ~] = get_rgb_cmyk(color_id);
        binary_char_array = get_binary_from_rgb(rgb_vec);
        answers_cell{i} = binary_char_array;
    end 
    assert(strcmpi(answers_cell{correct_selection_id},correct_answer), 'problem here')
end

function binary_char_array = get_binary_from_rgb(rgb_vec)
    r_c = sprintf('%08s', dec2bin(rgb_vec(1)));
    g_c = sprintf('%08s', dec2bin(rgb_vec(2)));
    b_c = sprintf('%08s', dec2bin(rgb_vec(3)));    
    binary_char_array = [r_c g_c b_c];
end