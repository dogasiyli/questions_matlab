function testIndCalcFuncs( K,L,M )
    for i=0:K*L*M-1
        [a,b,c] = ind2sub_mine(L,M,i);
        i_m = sub2ind_mine(L,M,a,b,c);
        assert(i==i_m);
        disp([num2str(a) ',' num2str(b) ',' num2str(c) ' --> i(' num2str(i) '),i_m(' num2str(i_m) ')']);
    end
end





