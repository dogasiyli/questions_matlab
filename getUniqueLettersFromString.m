function letters = getUniqueLettersFromString(strToDisp, letterCnt)
    letters = unique(strToDisp);
    assert(length(letters)<=letterCnt,'no way');
    if length(letters)<letterCnt
        %add letters to it
        unUsedLetterIDs = 65:90;
        unUsedLetterIDs(letters-'A'+1)=[];
        unUsedLetters = char(unUsedLetterIDs);
        %shuffle characters
        unUsedLetters = unUsedLetters(randperm(length(unUsedLetters)));
        letters = [letters unUsedLetters(1:(letterCnt-length(letters)))];
    end
    %shuffle letters
    letters = letters(randperm(length(letters)));
end

