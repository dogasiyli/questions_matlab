function createPapers( nameNumberCells, soruPuan, numOfSamples, questionOrder)
    %load('namesOfStudents_sample2.mat')
    %createPapers( nameNumberCells, [21], [1], [19]);
    lastQuestionID = max(questionOrder);
    if ~exist('questionOrder','var') || isempty(questionOrder)
        questionOrder = 1:lastQuestionID;
    end
    if ~exist('numOfSamples','var') || isempty(numOfSamples)
        numOfSamples = zeros(1,lastQuestionID);
        numOfSamples(questionOrder) = 1;
    elseif length(numOfSamples)~=length(questionOrder)
        nosTemp = zeros(1,lastQuestionID);
        nosTemp(questionOrder) = ones(1,length(questionOrder));
        numOfSamples = nosTemp;
    elseif length(numOfSamples)==length(questionOrder)
        nosTemp = zeros(1,lastQuestionID);
        nosTemp(questionOrder) = numOfSamples;
        numOfSamples = nosTemp;
    end
    if ~exist('soruPuan','var') || isempty(soruPuan)
        soruPuan = zeros(1,lastQuestionID);
        soruPuan(questionOrder) = 1;
%     soruPuan = [1 1 1 1 1 ... 1-5
%                 1 1 1 1 1 ... 6-10
%                 1 1 1 1 1 ... 11-15
%                 1]; %16
%     soruPuan(11) = 10;
%     soruPuan(12) = 20;
%     soruPuan(13) = 30;
%     soruPuan(14) = 20;
%     soruPuan(15) = 10;
%     soruPuan(16) = 10;
    else
        spTemp = zeros(1,lastQuestionID);
        spTemp(questionOrder) = soruPuan;
        soruPuan = spTemp;        
    end

    questSheetCombined = '';
    answerSheetCombined = '';
    for i=1:size(nameNumberCells,1)%3%
        inputStruct = struct;
        inputStruct.Q03_stringsToDisp = q3GetSringsFromPool(numOfSamples(3));
        inputStruct.Q04_bitCount = 4 + randi(5,[1,numOfSamples(4)]);%sth between 5 and 9
        inputStruct.repeatCounts = numOfSamples;   
        inputStruct.student_i = i-1;
        inputStruct.studentName = nameNumberCells{i,1};
        inputStruct.studentNumber = nameNumberCells{i,2};
        %if i>1
        %    inputStruct.questionOrder = randperm(7);
        %else
            inputStruct.questionOrder = questionOrder;
        %end
        inputStruct.questPuanVec = soruPuan;
        [questSheetAdd, answerSheetAdd] = createQuestions(inputStruct);
        if i>1
            questSheetCombined = [questSheetCombined 'SINAV***SORU***' inputStruct.studentName '***SORU***SINAV' newLineFunc() questSheetAdd]; %#ok<AGROW>
            answerSheetCombined = [answerSheetCombined 'SINAV***CEVAP***' inputStruct.studentName '***CEVAP***SINAV' newLineFunc() answerSheetAdd];%#ok<AGROW>
        end
    end
    fileName_question = ['questions' filesep 'QuestionsAll.txt'];
    fileName_answer = ['questions' filesep 'AnswersAll.txt'];
    if exist(fileName_question,'file')
        delete(fileName_question);
    end
    if exist(fileName_answer,'file')
        delete(fileName_answer);
    end
    
    fid = fopen(fileName_question,'w','n','UTF-8');
    fprintf(fid, questSheetCombined);
    fclose(fid);
    
    fid = fopen(fileName_answer,'w','n','UTF-8');
    fprintf(fid, answerSheetCombined);
    fclose(fid);    
end

function strCell = q3GetSringsFromPool(stringCount)
    %Q03_stringsToDisp = {'KEDI','ARABA','MEYVE','SEPET','TEPELEME','SEKERLEME'};%[];%
    Q03_stringsToDisp = {'KAPICI','BACAK','KABACA','TARAMALI','KEMENCE','KABASAKAL','SERSEM','SERSERI','SEVECEN','MUTLULUK','TARANTULA','TERLEME','KERPETEN','SENTEZ'};%[];%
    stringsToSelect = randperm(length(Q03_stringsToDisp));
    stringsToSelect = stringsToSelect(1:stringCount);
    if stringCount>1
        strCell = Q03_stringsToDisp(stringsToSelect);
    else
        strCell = {Q03_stringsToDisp(stringsToSelect)};
    end
end

