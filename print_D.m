function out_str = print_D(D, ii, pel, sort_name, changedIndiceVec, sort_vals)
    out_str1 = print_hor_line(D, false, sort_name);
    if ~strcmpi(sort_name,'bubsort')
        out_str1 = [out_str1 ' ' num2str(ii,'%02d') ' |'];
    end
    if pel
        fprintf('%s',out_str1)
    end
    out_str2 = print_changed_D(D, changedIndiceVec, 0, pel);
    
    if sort_name=='inssort'
        k = sort_vals(1);
        ekle = sort_vals(2);
        if k>0
            D_k = D(k);
        else
            D_k = NaN;
        end
        out_str3 = sprintf(' %2d | %2d | %2d \n',k-1, ekle, D_k);
    elseif sort_name=='bubsort'
        mov = sort_vals(1);
        k = sort_vals(2);
        out_str3 = sprintf(' %2d | %2d \n',mov, k);
    elseif sort_name=='selsort'
        %do nothing
        out_str3 = newline;
    else
        error(['unknown sort_name(' sort_name ')']);
    end
    
    if pel
        fprintf('%s',out_str3)
    end
    out_str = [out_str1 out_str2 out_str3];
end