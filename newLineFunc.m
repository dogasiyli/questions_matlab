function s = newLineFunc()
    try
        s = newline;
    catch
        s = sprintf('%s','\r\n');
    end
end